﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApp3.Programação.Cliente;
using WindowsFormsApp3.Programação.Endereço;

namespace sigma
{
    public partial class Cadastro_de_Funcionario : Form
    {
        public Cadastro_de_Funcionario()
        {
            InitializeComponent();
        }


        private void button1_Click_1(object sender, EventArgs e)
        {


            EndereçoDTO endedto = new EndereçoDTO();

            endedto.rua = txtRua.Text;
            endedto.bairro = txtBairro.Text;
            endedto.cidade = txtCidade.Text;
            endedto.cep = txtCep.Text;
            endedto.estado = cboEstado.Text;
            endedto.referencia = txtReferencia.Text;
            endedto.complemento = txtComplemento.Text;
            endedto.numerocasa = txtNumero.Text;
            EndereçoBusiness ebusiness = new EndereçoBusiness();
            int a = ebusiness.Salvar(endedto);

            FuncionarioDTO funcionariodto = new FuncionarioDTO();
            funcionariodto.nome = txtNome.Text;
            funcionariodto.cpf = txtCPF.Text;
            funcionariodto.rg = txtRg.Text;
            funcionariodto.telefone = txtTelefone.Text;
            funcionariodto.celular = txtCelular.Text;
            funcionariodto.email = txtEmail.Text;
            funcionariodto.idendereco = a;
            

            FuncionarioBusiness business = new FuncionarioBusiness();
            business.Salvar(funcionariodto);



        }

        private void button2_Click(object sender, EventArgs e)
        {
            Menu tela = new Menu();
            tela.Show();
            this.Hide();
        }
    }
}
