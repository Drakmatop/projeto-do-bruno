﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace sigma
{
    public partial class Menu : Form
    {
        public Menu()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            Cadastrar_Cliente tela = new Cadastrar_Cliente();
            tela.Show();
            this.Hide();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Cadastro_de_Funcionario tela = new Cadastro_de_Funcionario();
            tela.Show();
            this.Hide();

        }

        private void button3_Click(object sender, EventArgs e)
        {
            Cadastro_de_Veiculo tela = new Cadastro_de_Veiculo();
            tela.Show();
            this.Hide();

        }

        private void button4_Click(object sender, EventArgs e)
        {
            LocalizarVeiculo tela = new LocalizarVeiculo();
            tela.Show();
            this.Hide();

        }
    }
}
