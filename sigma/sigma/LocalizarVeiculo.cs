﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApp3.Programação.CadastrarVeiculo;

namespace sigma
{
    public partial class LocalizarVeiculo : Form
    {
        public LocalizarVeiculo()
        {
            InitializeComponent();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            VeiculoBusiness business = new VeiculoBusiness();
            List<VeiculoDTO> a = business.Consultar(txtBusca.Text);
            dgvVeiculo.AutoGenerateColumns = false;
            dgvVeiculo.DataSource = a;

        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            Menu tela = new Menu();
            tela.Show();
            this.Hide();
        }
    }
}
