﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApp3.Programação.Cliente;
using WindowsFormsApp3.Programação.Endereço;

namespace sigma
{
    public partial class Cadastrar_Cliente : Form
    {
        public Cadastrar_Cliente()
        {
            InitializeComponent();
            rdnPessoaFisica.Checked = true;

        }

      

        private void button1_Click(object sender, EventArgs e)
        {
           
            


            ClienteBusiness business = new ClienteBusiness();

             

            EndereçoDTO endedto = new EndereçoDTO();

            endedto.rua = txtRua.Text;
            endedto.bairro = txtBairro.Text;
            endedto.cidade = txtCidade.Text;
            endedto.numerocasa = txtNumero.Text;
            endedto.cep = txtCep.Text;
            endedto.estado = cboEstado.Text;
            endedto.referencia = txtReferencia.Text;
            endedto.complemento = txtComplemento.Text;

            EndereçoBusiness ebusiness = new EndereçoBusiness();
            int a = ebusiness.Salvar(endedto);


            ClienteDTO clientedto = new ClienteDTO();

            clientedto.Nome = txtNome.Text;
            clientedto.cpf = txtCPF.Text;
            clientedto.profissao = txtProfissao.Text;
            clientedto.nascimento = dtpNascimento.Value;
            clientedto.telefone = txtTelefone.Text;
            clientedto.celular = txtCelular.Text;
            clientedto.telcomercial = txtTelComercial.Text;
            clientedto.email = txtEmail.Text;
            clientedto.ramal = txtramal.Text;
            clientedto.clienteObs = txtObservações.Text;
            clientedto.Id_Endereco = a;

            if (txtAtividade.Text != string.Empty)
            {
                clientedto.atividade = txtAtividade.Text;
            }
            else if (txtrazao.Text != string.Empty)
            {
                clientedto.razaosocial = txtrazao.Text;
            }
            else if (txtcnpj.Text != string.Empty)
            {
                clientedto.cnpj = txtcnpj.Text;
            }

            business.Salvar(clientedto);



        }

        private void maskedTextBox5_MaskInputRejected(object sender, MaskInputRejectedEventArgs e)
        {

        }

        private void rdnPessoaFisica_CheckedChanged(object sender, EventArgs e)
        {
            lblCPF.Visible = false;
            lblNome.Visible = false;
            lblProfissao.Visible = false;
            txtrazao.Visible = false;
            txtcnpj.Visible = false;
            txtAtividade.Visible = false;

            label1.Visible = true;
            label2.Visible = true;
            label3.Visible = true;
            txtNome.Visible = true;
            txtCPF.Visible = true;
            txtSexo.Visible = true;

        }

        private void rdnPessoaJuridica_CheckedChanged(object sender, EventArgs e)
        {
            label1.Visible = false;
            label2.Visible = false;
            label3.Visible = false;
            txtNome.Visible = false;
            txtCPF.Visible = false;
            txtSexo.Visible = false;


            lblCPF.Visible = true;
            lblNome.Visible = true;
            lblProfissao.Visible = true;
            txtrazao.Visible = true;
            txtcnpj.Visible = true;
            txtAtividade.Visible = true;

        }

        private void button2_Click(object sender, EventArgs e)
        {
            Menu tela = new Menu();
            tela.Show();
            this.Hide();

        }

        private void txtrazao_TextChanged(object sender, EventArgs e)
        {

        }
    }
    
}
