﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApp3.Programação.CadastrarVeiculo;

namespace sigma
{
    public partial class Cadastro_de_Veiculo : Form
    {
        public Cadastro_de_Veiculo()
        {
            InitializeComponent();
        }


        private void button6_Click(object sender, EventArgs e)
        {
            VeiculoDTO veiculodto = new VeiculoDTO();
            veiculodto.placa = txtPlaca.Text;
            veiculodto.combustivel = txtCombustivel.Text;
            veiculodto.odometro = txtOdometro.Text;
            veiculodto.cor = txtCor.Text;
            veiculodto.ano = txtAno.Text;
            veiculodto.proprietario = txtProprietario.Text;
            veiculodto.modelo = txtModelo.Text;

            VeiculoBusiness business = new VeiculoBusiness();
            business.Salvar(veiculodto);

        }

        private void button1_Click(object sender, EventArgs e)
        {
            Menu tela = new Menu();
            tela.Show();
            this.Hide();
        }
    }
}
