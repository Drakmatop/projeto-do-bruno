﻿namespace sigma
{
    partial class Cadastrar_Cliente
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.txtNome = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.txtProfissao = new System.Windows.Forms.TextBox();
            this.txtRua = new System.Windows.Forms.TextBox();
            this.txtNumero = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.txtComplemento = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.txtCPF = new System.Windows.Forms.MaskedTextBox();
            this.dtpNascimento = new System.Windows.Forms.DateTimePicker();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.rdnPessoaFisica = new System.Windows.Forms.RadioButton();
            this.rdnPessoaJuridica = new System.Windows.Forms.RadioButton();
            this.txtBairro = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.txtCidade = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.txtCep = new System.Windows.Forms.MaskedTextBox();
            this.cboEstado = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.txtTelefone = new System.Windows.Forms.MaskedTextBox();
            this.txtCelular = new System.Windows.Forms.MaskedTextBox();
            this.txtTelComercial = new System.Windows.Forms.MaskedTextBox();
            this.txtEmail = new System.Windows.Forms.TextBox();
            this.txtramal = new System.Windows.Forms.TextBox();
            this.txtcnpj = new System.Windows.Forms.TextBox();
            this.lblCPF = new System.Windows.Forms.Label();
            this.txtAtividade = new System.Windows.Forms.TextBox();
            this.lblProfissao = new System.Windows.Forms.Label();
            this.lblNome = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.txtObservações = new System.Windows.Forms.TextBox();
            this.txtReferencia = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.txtrazao = new System.Windows.Forms.TextBox();
            this.txtSexo = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Location = new System.Drawing.Point(643, 414);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(120, 33);
            this.button1.TabIndex = 0;
            this.button1.Text = "Cadastrar";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(20, 76);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(52, 16);
            this.label1.TabIndex = 1;
            this.label1.Text = "Nome :";
            // 
            // txtNome
            // 
            this.txtNome.Location = new System.Drawing.Point(87, 74);
            this.txtNome.Name = "txtNome";
            this.txtNome.Size = new System.Drawing.Size(252, 22);
            this.txtNome.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(33, 105);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(39, 16);
            this.label2.TabIndex = 3;
            this.label2.Text = "CPF :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(27, 133);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(45, 16);
            this.label3.TabIndex = 4;
            this.label3.Text = "Sexo :";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(394, 76);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(72, 16);
            this.label4.TabIndex = 5;
            this.label4.Text = "Profissão :";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(354, 108);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(139, 16);
            this.label5.TabIndex = 6;
            this.label5.Text = "Data de  nascimento :";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(33, 204);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(39, 16);
            this.label6.TabIndex = 7;
            this.label6.Text = "Rua :";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(15, 317);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(57, 16);
            this.label7.TabIndex = 8;
            this.label7.Text = "Contato";
            // 
            // txtProfissao
            // 
            this.txtProfissao.Location = new System.Drawing.Point(472, 76);
            this.txtProfissao.Name = "txtProfissao";
            this.txtProfissao.Size = new System.Drawing.Size(289, 22);
            this.txtProfissao.TabIndex = 15;
            // 
            // txtRua
            // 
            this.txtRua.Location = new System.Drawing.Point(78, 201);
            this.txtRua.Name = "txtRua";
            this.txtRua.Size = new System.Drawing.Size(271, 22);
            this.txtRua.TabIndex = 17;
            // 
            // txtNumero
            // 
            this.txtNumero.Location = new System.Drawing.Point(410, 229);
            this.txtNumero.Name = "txtNumero";
            this.txtNumero.Size = new System.Drawing.Size(60, 22);
            this.txtNumero.TabIndex = 26;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(338, 232);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(66, 16);
            this.label13.TabIndex = 25;
            this.label13.Text = "Número :";
            // 
            // txtComplemento
            // 
            this.txtComplemento.Location = new System.Drawing.Point(596, 227);
            this.txtComplemento.Name = "txtComplemento";
            this.txtComplemento.Size = new System.Drawing.Size(165, 22);
            this.txtComplemento.TabIndex = 28;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(488, 232);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(102, 16);
            this.label14.TabIndex = 27;
            this.label14.Text = "Complemento :";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(365, 260);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(39, 16);
            this.label15.TabIndex = 29;
            this.label15.Text = "CEP :";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(507, 263);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(57, 16);
            this.label16.TabIndex = 31;
            this.label16.Text = "Estado :";
            // 
            // txtCPF
            // 
            this.txtCPF.Location = new System.Drawing.Point(87, 101);
            this.txtCPF.Mask = "000,000,000-00";
            this.txtCPF.Name = "txtCPF";
            this.txtCPF.Size = new System.Drawing.Size(109, 22);
            this.txtCPF.TabIndex = 38;
            // 
            // dtpNascimento
            // 
            this.dtpNascimento.Location = new System.Drawing.Point(496, 106);
            this.dtpNascimento.Name = "dtpNascimento";
            this.dtpNascimento.Size = new System.Drawing.Size(269, 22);
            this.dtpNascimento.TabIndex = 39;
            this.dtpNascimento.Value = new System.DateTime(2018, 8, 23, 0, 0, 0, 0);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(8, 44);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(51, 16);
            this.label19.TabIndex = 40;
            this.label19.Text = "Cliente";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(5, 176);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(67, 16);
            this.label20.TabIndex = 41;
            this.label20.Text = "Endereço";
            // 
            // rdnPessoaFisica
            // 
            this.rdnPessoaFisica.AutoSize = true;
            this.rdnPessoaFisica.Location = new System.Drawing.Point(524, 53);
            this.rdnPessoaFisica.Name = "rdnPessoaFisica";
            this.rdnPessoaFisica.Size = new System.Drawing.Size(107, 20);
            this.rdnPessoaFisica.TabIndex = 42;
            this.rdnPessoaFisica.TabStop = true;
            this.rdnPessoaFisica.Text = "Pessoa Física";
            this.rdnPessoaFisica.UseVisualStyleBackColor = true;
            this.rdnPessoaFisica.CheckedChanged += new System.EventHandler(this.rdnPessoaFisica_CheckedChanged);
            // 
            // rdnPessoaJuridica
            // 
            this.rdnPessoaJuridica.AutoSize = true;
            this.rdnPessoaJuridica.Location = new System.Drawing.Point(639, 53);
            this.rdnPessoaJuridica.Name = "rdnPessoaJuridica";
            this.rdnPessoaJuridica.Size = new System.Drawing.Size(122, 20);
            this.rdnPessoaJuridica.TabIndex = 43;
            this.rdnPessoaJuridica.TabStop = true;
            this.rdnPessoaJuridica.Text = "Pessoa Juridica";
            this.rdnPessoaJuridica.UseVisualStyleBackColor = true;
            this.rdnPessoaJuridica.CheckedChanged += new System.EventHandler(this.rdnPessoaJuridica_CheckedChanged);
            // 
            // txtBairro
            // 
            this.txtBairro.Location = new System.Drawing.Point(87, 232);
            this.txtBairro.Name = "txtBairro";
            this.txtBairro.Size = new System.Drawing.Size(165, 22);
            this.txtBairro.TabIndex = 45;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(18, 232);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(54, 16);
            this.label21.TabIndex = 44;
            this.label21.Text = "Bairro :";
            // 
            // txtCidade
            // 
            this.txtCidade.Location = new System.Drawing.Point(78, 257);
            this.txtCidade.Name = "txtCidade";
            this.txtCidade.Size = new System.Drawing.Size(165, 22);
            this.txtCidade.TabIndex = 47;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(15, 263);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(57, 16);
            this.label22.TabIndex = 46;
            this.label22.Text = "Cidade :";
            // 
            // txtCep
            // 
            this.txtCep.Location = new System.Drawing.Point(410, 257);
            this.txtCep.Mask = "00,000,000";
            this.txtCep.Name = "txtCep";
            this.txtCep.Size = new System.Drawing.Size(82, 22);
            this.txtCep.TabIndex = 48;
            // 
            // cboEstado
            // 
            this.cboEstado.FormattingEnabled = true;
            this.cboEstado.Items.AddRange(new object[] {
            "RS",
            "SP"});
            this.cboEstado.Location = new System.Drawing.Point(570, 258);
            this.cboEstado.Name = "cboEstado";
            this.cboEstado.Size = new System.Drawing.Size(48, 24);
            this.cboEstado.TabIndex = 49;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(75, 349);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(68, 16);
            this.label8.TabIndex = 50;
            this.label8.Text = "Telefone :";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(84, 371);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(59, 16);
            this.label9.TabIndex = 51;
            this.label9.Text = "Celular :";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(271, 343);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(133, 16);
            this.label10.TabIndex = 52;
            this.label10.Text = "Telefone comercial :";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(354, 371);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(50, 16);
            this.label11.TabIndex = 53;
            this.label11.Text = "Email :";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(554, 346);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(54, 16);
            this.label12.TabIndex = 54;
            this.label12.Text = "Ramal :";
            // 
            // txtTelefone
            // 
            this.txtTelefone.Location = new System.Drawing.Point(149, 343);
            this.txtTelefone.Mask = "(999) 000-0000";
            this.txtTelefone.Name = "txtTelefone";
            this.txtTelefone.Size = new System.Drawing.Size(109, 22);
            this.txtTelefone.TabIndex = 55;
            // 
            // txtCelular
            // 
            this.txtCelular.Location = new System.Drawing.Point(149, 368);
            this.txtCelular.Mask = "(999) 000-0000";
            this.txtCelular.Name = "txtCelular";
            this.txtCelular.Size = new System.Drawing.Size(109, 22);
            this.txtCelular.TabIndex = 56;
            // 
            // txtTelComercial
            // 
            this.txtTelComercial.Location = new System.Drawing.Point(410, 340);
            this.txtTelComercial.Mask = "(999) 000-0000";
            this.txtTelComercial.Name = "txtTelComercial";
            this.txtTelComercial.Size = new System.Drawing.Size(109, 22);
            this.txtTelComercial.TabIndex = 57;
            this.txtTelComercial.MaskInputRejected += new System.Windows.Forms.MaskInputRejectedEventHandler(this.maskedTextBox5_MaskInputRejected);
            // 
            // txtEmail
            // 
            this.txtEmail.Location = new System.Drawing.Point(410, 368);
            this.txtEmail.Name = "txtEmail";
            this.txtEmail.Size = new System.Drawing.Size(271, 22);
            this.txtEmail.TabIndex = 58;
            // 
            // txtramal
            // 
            this.txtramal.Location = new System.Drawing.Point(614, 343);
            this.txtramal.Name = "txtramal";
            this.txtramal.Size = new System.Drawing.Size(67, 22);
            this.txtramal.TabIndex = 59;
            // 
            // txtcnpj
            // 
            this.txtcnpj.Location = new System.Drawing.Point(132, 102);
            this.txtcnpj.Name = "txtcnpj";
            this.txtcnpj.Size = new System.Drawing.Size(100, 22);
            this.txtcnpj.TabIndex = 129;
            // 
            // lblCPF
            // 
            this.lblCPF.AutoSize = true;
            this.lblCPF.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCPF.Location = new System.Drawing.Point(74, 104);
            this.lblCPF.Name = "lblCPF";
            this.lblCPF.Size = new System.Drawing.Size(51, 16);
            this.lblCPF.TabIndex = 128;
            this.lblCPF.Text = "CNPJ:";
            // 
            // txtAtividade
            // 
            this.txtAtividade.Location = new System.Drawing.Point(131, 133);
            this.txtAtividade.Name = "txtAtividade";
            this.txtAtividade.Size = new System.Drawing.Size(100, 22);
            this.txtAtividade.TabIndex = 127;
            // 
            // lblProfissao
            // 
            this.lblProfissao.AutoSize = true;
            this.lblProfissao.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblProfissao.Location = new System.Drawing.Point(47, 133);
            this.lblProfissao.Name = "lblProfissao";
            this.lblProfissao.Size = new System.Drawing.Size(78, 16);
            this.lblProfissao.TabIndex = 126;
            this.lblProfissao.Text = "Atividade:";
            // 
            // lblNome
            // 
            this.lblNome.AutoSize = true;
            this.lblNome.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNome.Location = new System.Drawing.Point(20, 73);
            this.lblNome.Name = "lblNome";
            this.lblNome.Size = new System.Drawing.Size(105, 16);
            this.lblNome.TabIndex = 124;
            this.lblNome.Text = "Razão Social:";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(32, 419);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(94, 16);
            this.label17.TabIndex = 130;
            this.label17.Text = "Observações :";
            // 
            // txtObservações
            // 
            this.txtObservações.Location = new System.Drawing.Point(132, 419);
            this.txtObservações.Name = "txtObservações";
            this.txtObservações.Size = new System.Drawing.Size(387, 22);
            this.txtObservações.TabIndex = 131;
            // 
            // txtReferencia
            // 
            this.txtReferencia.Location = new System.Drawing.Point(489, 201);
            this.txtReferencia.Name = "txtReferencia";
            this.txtReferencia.Size = new System.Drawing.Size(165, 22);
            this.txtReferencia.TabIndex = 133;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(402, 203);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(81, 16);
            this.label18.TabIndex = 132;
            this.label18.Text = "Referência :";
            // 
            // button2
            // 
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.Location = new System.Drawing.Point(11, 8);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(89, 33);
            this.button2.TabIndex = 135;
            this.button2.Text = "Voltar";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // txtrazao
            // 
            this.txtrazao.Location = new System.Drawing.Point(131, 70);
            this.txtrazao.Name = "txtrazao";
            this.txtrazao.Size = new System.Drawing.Size(100, 22);
            this.txtrazao.TabIndex = 136;
            this.txtrazao.TextChanged += new System.EventHandler(this.txtrazao_TextChanged);
            // 
            // txtSexo
            // 
            this.txtSexo.Location = new System.Drawing.Point(87, 130);
            this.txtSexo.Name = "txtSexo";
            this.txtSexo.Size = new System.Drawing.Size(100, 22);
            this.txtSexo.TabIndex = 137;
            // 
            // Cadastrar_Cliente
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.ClientSize = new System.Drawing.Size(777, 465);
            this.Controls.Add(this.txtSexo);
            this.Controls.Add(this.txtrazao);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.txtReferencia);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.txtObservações);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.txtcnpj);
            this.Controls.Add(this.lblCPF);
            this.Controls.Add(this.txtAtividade);
            this.Controls.Add(this.lblProfissao);
            this.Controls.Add(this.lblNome);
            this.Controls.Add(this.txtramal);
            this.Controls.Add(this.txtEmail);
            this.Controls.Add(this.txtTelComercial);
            this.Controls.Add(this.txtCelular);
            this.Controls.Add(this.txtTelefone);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.cboEstado);
            this.Controls.Add(this.txtCep);
            this.Controls.Add(this.txtCidade);
            this.Controls.Add(this.label22);
            this.Controls.Add(this.txtBairro);
            this.Controls.Add(this.label21);
            this.Controls.Add(this.rdnPessoaJuridica);
            this.Controls.Add(this.rdnPessoaFisica);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.dtpNascimento);
            this.Controls.Add(this.txtCPF);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.txtComplemento);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.txtNumero);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.txtRua);
            this.Controls.Add(this.txtProfissao);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtNome);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button1);
            this.Font = new System.Drawing.Font("Georgia", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "Cadastrar_Cliente";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Cadastrar Cliente";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtNome;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtProfissao;
        private System.Windows.Forms.TextBox txtRua;
        private System.Windows.Forms.TextBox txtNumero;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txtComplemento;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.MaskedTextBox txtCPF;
        private System.Windows.Forms.DateTimePicker dtpNascimento;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.RadioButton rdnPessoaFisica;
        private System.Windows.Forms.RadioButton rdnPessoaJuridica;
        private System.Windows.Forms.TextBox txtBairro;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox txtCidade;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.MaskedTextBox txtCep;
        private System.Windows.Forms.ComboBox cboEstado;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.MaskedTextBox txtTelefone;
        private System.Windows.Forms.MaskedTextBox txtCelular;
        private System.Windows.Forms.TextBox txtEmail;
        private System.Windows.Forms.TextBox txtramal;
        private System.Windows.Forms.MaskedTextBox txtTelComercial;
        private System.Windows.Forms.TextBox txtcnpj;
        private System.Windows.Forms.Label lblCPF;
        private System.Windows.Forms.TextBox txtAtividade;
        private System.Windows.Forms.Label lblProfissao;
        private System.Windows.Forms.Label lblNome;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox txtObservações;
        private System.Windows.Forms.TextBox txtReferencia;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.TextBox txtrazao;
        private System.Windows.Forms.TextBox txtSexo;
    }
}