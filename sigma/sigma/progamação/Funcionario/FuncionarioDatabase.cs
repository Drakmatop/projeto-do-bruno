﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WindowsFormsApp3.Programação.Base;

namespace WindowsFormsApp3.Programação.Cliente
{
    public class FuncionarioDatabase
    {
        public int Salvar(FuncionarioDTO funcionario)
        {
            string script =
                @"INSERT INTO Funcionario
                (
                  idFuncionario,
                    Endereco_id_Endereco,
                  nm_funcionario,
                  
                  
                  ds_cpf,
                  ds_rg,
                  dt_nascimento,
                  img_foto,
                  ds_telefone,
                  ds_celular ,
                  ds_email
                )
                VALUES
                (
                  @idFuncionario,
                  @Endereco_id_Endereco,
                  @nm_funcionario,
               
                  
                  @ds_cpf,
                  @ds_rg,
                  @dt_nascimento,
                  @img_foto,
                  @ds_telefone,
                  @ds_celular ,
                  @ds_email)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("idFuncionario", funcionario.idfuncionario));
            parms.Add(new MySqlParameter("Endereco_id_Endereco", funcionario.idendereco));
            parms.Add(new MySqlParameter("nm_funcionario", funcionario.nome));
           parms.Add(new MySqlParameter("ds_cpf", funcionario.cpf));
            parms.Add(new MySqlParameter("ds_rg", funcionario.rg));
            parms.Add(new MySqlParameter("dt_nascimento", funcionario.nascimento));
            parms.Add(new MySqlParameter("img_foto", funcionario.foto));
            parms.Add(new MySqlParameter("ds_telefone", funcionario.telefone));
            parms.Add(new MySqlParameter("ds_celular", funcionario.celular));
            parms.Add(new MySqlParameter("ds_email", funcionario.email));

            Database db = new Database();
            int pk = db.ExecuteInsertScriptWithPk(script, parms);
            return pk;
        }

        public void Alterar(FuncionarioDTO funcionario)
        {
            string script =
            @"UPDATE Funcionario
                 SET idFuncionario = @idFuncionario,
                     Endereco_id_Endereco = @Endereco_id_Endereco,
	                 nm_funcionario = @nm_funcionario,
	                
	                 ds_cpf = @ds_cpf,
	               dt_nascimento = @dt_nascimento,
	                img_foto= @img_foto,
                   ds_telefone= @ ds_telefone,
                    ds_celular= @ds_celular,
                   ds_email= @ds_email
	                 WHERE idFuncionario = @idFuncionario";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("idFuncionario", funcionario.idfuncionario));
            parms.Add(new MySqlParameter("Endereco_id_Endereco", funcionario.idendereco));
            parms.Add(new MySqlParameter("nm_funcionario", funcionario.nome));
            parms.Add(new MySqlParameter("ds_cpf", funcionario.cpf));
            parms.Add(new MySqlParameter("img_foto", funcionario.foto));
            parms.Add(new MySqlParameter("ds_telefone", funcionario.telefone));
            parms.Add(new MySqlParameter("ds_celular", funcionario.celular));
            parms.Add(new MySqlParameter("ds_email", funcionario.email));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }


        public void Remover(int id)
        {
            string script =
            @"DELETE FROM Funcionario WHERE idFuncionario= @idFuncionario";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("idFuncionario", id));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }


        public List<FuncionarioDTO> Listar()
        {
            string script =
                @"SELECT * FROM Funcionario ORDER BY idFuncionario";

            List<MySqlParameter> parms = new List<MySqlParameter>();

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<FuncionarioDTO> funcionarios = new List<FuncionarioDTO>();

            while (reader.Read())
            {
                FuncionarioDTO novoFuncionario= new FuncionarioDTO();
                novoFuncionario.idfuncionario = reader.GetInt32("idFuncionario");
                novoFuncionario.idendereco = reader.GetInt32("Endereco_id_Endereco");
                novoFuncionario.nome = reader.GetString("nm_funcionario");
                novoFuncionario.cpf = reader.GetString("ds_cpf");
                novoFuncionario.rg = reader.GetString("ds_rg");
                novoFuncionario.nascimento = reader.GetDateTime("dt_nascimento");
                novoFuncionario.foto = reader.GetString("img_foto");
                novoFuncionario.telefone = reader.GetString("ds_telefone");
                novoFuncionario.celular = reader.GetString("ds_celular");
                novoFuncionario.email = reader.GetString("ds_email");

           funcionarios.Add(novoFuncionario);
            }
            reader.Close();

            return funcionarios;

        }
    }
}
