﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WindowsFormsApp3.Programação.Base;

namespace WindowsFormsApp3.Programação.Endereço
{
    public class EndereçoDatabase
    {
        public int Salvar(EndereçoDTO endereço)
        {
            string script =
                @"INSERT INTO Endereco
                (
                  id_Endereco,
                  ds_rua ,
                  ds_bairro,
                  ds_cidade,
                  ds_referencia,
                  ds_complemento,
                  ds_estado,
                  ds_ncasa,
                  ds_cep 
                )
                VALUES
                (
                  @id_Endereco,
                  @ds_rua ,
                  @ds_bairro,
                  @ds_cidade,
                  @ds_referencia,
                  @ds_complemento,
                  @ds_estado,
                  @ds_ncasa,
                  @ds_cep )";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_Endereco", endereço.idendereco));
            parms.Add(new MySqlParameter("ds_rua", endereço.rua));
            parms.Add(new MySqlParameter("ds_bairro", endereço.bairro));
            parms.Add(new MySqlParameter("ds_cidade", endereço.cidade));
            parms.Add(new MySqlParameter("ds_referencia", endereço.referencia));
            parms.Add(new MySqlParameter("ds_complemento", endereço.complemento));
            parms.Add(new MySqlParameter("ds_estado", endereço.estado));
            parms.Add(new MySqlParameter("ds_ncasa", endereço.numerocasa));
            parms.Add(new MySqlParameter("ds_cep", endereço.cep));

            Database db = new Database();
            int pk = db.ExecuteInsertScriptWithPk(script, parms);
            return pk;
        }

        public void Alterar(EndereçoDTO endereço)
        {
            string script =
            @"UPDATE Endereco
                 SET id_Endereco  = @id_Endereco,
	                 ds_rua   = @ds_rua,
	                ds_bairro = @ds_bairro,
	                ds_cidade = @ds_cidade,
	               ds_referencia = @ds_referencia,
                    ds_estado = @ds_estado,
                    ds_complemento = @ds_complemento,
                    ds_ncasa = @ds_ncasa,
                   ds_cep = @ds_cep
	               WHERE id_Endereco = @id_Endereco";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_Endereco", endereço.idendereco));
            parms.Add(new MySqlParameter("ds_rua", endereço.rua));
            parms.Add(new MySqlParameter("ds_bairro", endereço.bairro));
            parms.Add(new MySqlParameter("ds_cidade", endereço.cidade));
            parms.Add(new MySqlParameter("ds_referencia", endereço.referencia));
            parms.Add(new MySqlParameter("ds_complemento", endereço.complemento));
            parms.Add(new MySqlParameter("ds_estado", endereço.estado));
            parms.Add(new MySqlParameter("ds_ncasa", endereço.numerocasa));
            parms.Add(new MySqlParameter("ds_cep", endereço.cep));
            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }

        public void Remover(int id)
        {
            string script =
            @"DELETE FROM Endereco WHERE id_Endereco = @id_Endereco";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_Endereco", id));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }


    }
}
