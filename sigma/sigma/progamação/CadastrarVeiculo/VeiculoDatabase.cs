﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WindowsFormsApp3.Programação.Base;

namespace WindowsFormsApp3.Programação.CadastrarVeiculo
{
    public  class VeiculoDatabase
      {
        public int Salvar(VeiculoDTO veiculo)
        {
            string script =
                @"INSERT INTO Veiculo
                (
                   idVeiculo,


                   ds_placa,
                   ds_combustivel,
                   ds_odometro,
                   ds_cor,
                   ds_ano,
                   nm_proprietario,
                   ds_modelo 
                )
                VALUES
                (
                   @idVeiculo,

                   @ds_placa,
                   @ds_combustivel,
                   @ds_odometro,
                   @ds_cor,
                   @ds_ano,
                   @nm_proprietario,
                   @ds_modelo
                )";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("idVeiculo", veiculo.idveiculo));
            parms.Add(new MySqlParameter("ds_placa", veiculo.placa));
            parms.Add(new MySqlParameter("ds_combustivel", veiculo.combustivel));
            parms.Add(new MySqlParameter("ds_odometro", veiculo.odometro));
            parms.Add(new MySqlParameter("ds_cor", veiculo.cor));
            parms.Add(new MySqlParameter("ds_ano", veiculo.ano));
            parms.Add(new MySqlParameter("nm_proprietario", veiculo.proprietario));
            parms.Add(new MySqlParameter("ds_modelo", veiculo.modelo));

            Database db = new Database();
            int pk = db.ExecuteInsertScriptWithPk(script, parms);
            return pk;
        }

        public void Alterar(VeiculoDTO veiculo)
        {
            string script =
            @"UPDATE Veiculo
                 SET idVeiculo  = @idVeiculo,
                  ds_placa   = @ds_placa,
                  ds_combustivel = @ds_combustivel,
                  ds_odometro = @ds_odometro,
                  ds_cor  = @ds_cor,
                  ds_ano = @ds_ano,
                  WHERE idVeiculo = @idVeiculo";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("idVeiculo", veiculo.idveiculo));
            parms.Add(new MySqlParameter("ds_placa", veiculo.placa));
            parms.Add(new MySqlParameter("ds_combustivel", veiculo.combustivel));
            parms.Add(new MySqlParameter("ds_odometro", veiculo.odometro));
            parms.Add(new MySqlParameter("ds_cor", veiculo.cor));
            parms.Add(new MySqlParameter("ds_ano", veiculo.ano));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }


        public void Remover(int id)
        {
            string script =
            @"DELETE FROM Veiculo WHERE idVeiculo = @idVeiculo";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("idVeiculo", id));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }

        public List<VeiculoDTO> Consultar(string modelo)
        {
            string script =
                @"SELECT * FROM veiculo
                  WHERE ds_modelo like @ds_modelo";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("ds_modelo", "%" + modelo + "%"));
            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<VeiculoDTO> veiculos = new List<VeiculoDTO>();
            while (reader.Read())
            {
                VeiculoDTO novoveiculo = new VeiculoDTO();
                novoveiculo.placa = reader.GetString("ds_placa");
                novoveiculo.idveiculo = reader.GetInt32("idVeiculo");
                novoveiculo.combustivel = reader.GetString("ds_combustivel");
                novoveiculo.odometro = reader.GetString("ds_odometro");
                novoveiculo.cor = reader.GetString("ds_cor");
                novoveiculo.ano = reader.GetString("ds_ano");
                novoveiculo.proprietario = reader.GetString("nm_proprietario");
                novoveiculo.modelo = reader.GetString("ds_modelo");

                veiculos.Add(novoveiculo);
            }
            return veiculos;
    }





    //VIEW | 
    //public List<VeiculoDTO> Consultar(//string musica, string genero)
    //{
    //    string script =
    //    @"SELECT * 
    //        FROM tb_Veiculo
    //       WHERE nm_musica like @nm_musica 
    //         AND ds_genero like @ds_genero";

    //    List<MySqlParameter> parms = new List<MySqlParameter>();
    //    parms.Add(new MySqlParameter("nm_musica", "%" + musica + "%"));
    //    parms.Add(new MySqlParameter("ds_genero", "%" + genero + "%"));

    //    Database db = new Database();
    //    MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

    //    List<VeiculoDTO> veiculos = new List<VeiculoDTO>();

    //    while (reader.Read())
    //    {
    //        VeiculoDTO novoVeiculo = new VeiculoDTO();
    //        novoVeiculo.idveiculo = reader.GetInt32("idVeiculo");
    //        novoVeiculo.placa = reader.GetString("ds_placa");
    //        novoVeiculo.combustivel = reader.GetString("ds_combustivel");
    //        novoVeiculo.odometro = reader.GetString("ds_odometro");
    //        novoVeiculo.cor = reader.GetString("ds_cor");
    //        novoVeiculo.ano = reader.GetString("ds_ano");
    //        veiculos.Add(novoVeiculo);
    //    }
    //    reader.Close();
    //    return veiculos;
    //}
}
}
