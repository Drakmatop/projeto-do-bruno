﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace sigma
{
    public partial class CarregamentoURNA : Form
    {
        public CarregamentoURNA()
        {
            InitializeComponent();

            Task.Factory.StartNew(() =>
            {
                // Espera 2 segundos para iniciar o sistema
                System.Threading.Thread.Sleep(6000);

                Invoke(new Action(() =>
                {
                    // Abre a tela Inicial
                    frmMenu frm = new frmMenu();
                    frm.Show();
                    Hide();
                }));
            });
        }
    }
}
