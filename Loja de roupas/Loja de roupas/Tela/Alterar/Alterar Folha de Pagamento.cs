﻿using Loja_de_roupas.DB.Folha_de_Pagamento;
using Loja_de_roupas.DB.Funcionario;
using mecanica.DB.Programação.Folha_de_Pagamento;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Loja_de_roupas.DB.Telas.Alterar
{
    public partial class Alterar_Folha_de_Pagamento : Form
    {
        public Alterar_Folha_de_Pagamento()
        {
            InitializeComponent();
            CarregarCombo();
        }

        void CarregarCombo()
        {
            FuncionarioBusiness business = new FuncionarioBusiness();
            List<FuncionarioDTO> Funcionarios = business.Listar();

            cboFuncionario.ValueMember = nameof(FuncionarioDTO.Id);
            cboFuncionario.DisplayMember = nameof(FuncionarioDTO.Nome);
            cboFuncionario.DataSource = Funcionarios;

        }

        FolhadePagamentoDTO folha1;
        public void LoadScreen(FolhadePagamentoDTO folha)
        {
            this.folha1 = folha;
            cboFuncionario.SelectedItem = folha.IdFuncionario;
            nudHorasExtras.Value = folha.HorasExtras;
            chkVT.Checked = folha.Bt_Valetransporte;
            lblSalarioLiquido.Text = folha.SalarioLiquido.ToString();


        }

        private void btnSalvar_Click(object sender, EventArgs e)
        {
            FuncionarioDTO dto = cboFuncionario.SelectedItem as FuncionarioDTO;

            if (lblSalarioLiquido.Text == folha1.SalarioLiquido.ToString())
            {
                MessageBox.Show("Faça o calculo antes de alterar");
            }
            else
            {
                folha1.Bt_Valetransporte = chkVT.Checked;
                folha1.FGTS = FGTS;
                folha1.HorasExtras = Convert.ToInt32(nudHorasExtras.Value);
                folha1.INSS = INSS;
                folha1.IR = IR;
                folha1.SalarioLiquido = SalarioLiquido;
                folha1.vl_ValeTransporte = VT;
                folha1.IdFuncionario = dto.Id;

                FolhadePagamentoBusiness business = new FolhadePagamentoBusiness();
                business.Alterar(folha1);
                MessageBox.Show("Folha de pagamento alterada com sucesso", "Lottus Store");

            }
        }
        CalculosFolhadePagamento folha = new CalculosFolhadePagamento();
        decimal VT = 0;
        decimal SalarioBruto = 0;
        decimal DSR;
        decimal INSS;
        decimal IR = 0;
        decimal FGTS;
        decimal SalarioLiquido;
        decimal ValordeDesconto = 0;
        decimal valordeatrasos = 0;
        int ht = 220;
        
       

        private void chkHoraExtra_CheckedChanged(object sender, EventArgs e)
        {
            if (chkHoraExtra.Checked == true)
            {
                lblPorcentagem.Visible = true;
                nudPorcentagem.Visible = true;
                lblHoraExtra.Visible = true;
                nudHorasExtras.Visible = true;
            }

            if (chkHoraExtra.Checked == false)
            {
                lblPorcentagem.Visible = false;
                nudPorcentagem.Visible = false;
                lblHoraExtra.Visible = false;
                nudHorasExtras.Visible = false;
            }
        }

       
        private void btnCalcular_Click_1(object sender, EventArgs e)
        {
            Calcular();
        }




        private void Calcular()
        {
            try
            {


                int he = Convert.ToInt32(nudHorasExtras.Value);
                decimal porcentagem = nudPorcentagem.Value;
                int diastrabalhados = folha.CalcularDiasTrabalhados(Convert.ToInt32(nudFaltasPriSemana.Value), Convert.ToInt32(nudFaltaSegSemana.Value), Convert.ToInt32(nudFaltaTerSemana.Value), Convert.ToInt32(nudFaltaQuarSemana.Value));
                int finaisdesemana = folha.CalcularFinaisdeSemanas(Convert.ToInt32(nudFaltasPriSemana.Value), Convert.ToInt32(nudFaltaSegSemana.Value), Convert.ToInt32(nudFaltaTerSemana.Value), Convert.ToInt32(nudFaltaQuarSemana.Value));
                int totaldefaltas = folha.CalcularTotalDeFaltas(Convert.ToInt32(nudFaltasPriSemana.Value), Convert.ToInt32(nudFaltaSegSemana.Value), Convert.ToInt32(nudFaltaTerSemana.Value), Convert.ToInt32(nudFaltaQuarSemana.Value));
                int horasatrasadas = Convert.ToInt32(nudHorasAtradas.Value);

                FuncionarioDTO funcionario = cboFuncionario.SelectedItem as FuncionarioDTO;
                SalarioBruto = funcionario.SalarioBruto;

                if (nudHorasExtras.Value > 0 && chkVT.Checked == true)
                {

                    decimal valorporhora = folha.CalcularValorPorHora(SalarioBruto, ht);
                    decimal totalhorasextras = folha.CalcularValorHoraExtra(valorporhora, porcentagem, he);
                    DSR = folha.CalcularDSR(totalhorasextras, diastrabalhados, finaisdesemana);
                    decimal TotalSalarioBruto = SalarioBruto + totalhorasextras + DSR;
                    valordeatrasos = folha.CalcularValorAtrasos(valorporhora, horasatrasadas);


                    INSS = folha.CalcularINSS(SalarioBruto, TotalSalarioBruto);
                    ValordeDesconto = folha.ValordeDesconto(TotalSalarioBruto, INSS);
                    IR = folha.CalcularIR(TotalSalarioBruto, INSS);
                    FGTS = folha.CalcularFGTS(TotalSalarioBruto);
                    VT = SalarioBruto * 0.06m;

                    SalarioLiquido = SalarioBruto + totalhorasextras + DSR - INSS - ValordeDesconto - VT - valordeatrasos - nudVR.Value - nudRefeicao.Value - nudConvenio.Value;


                    lblText.Visible = true;
                    lblSalarioLiquido.Visible = true;
                    lblSalarioLiquido.Text = SalarioLiquido.ToString();
                    lblINSS.Text = Math.Round(INSS).ToString();
                    lblFGTS.Text = Math.Round(FGTS).ToString();
                    lblDSR.Text = Math.Round(DSR).ToString();
                    lblIR.Text = Math.Round(IR).ToString();
                    lblVT.Text = Math.Round(VT).ToString();
                    lblValordedesconto.Text = Math.Round(ValordeDesconto).ToString();
                    lblValordeatraso.Text = Math.Round(valordeatrasos).ToString();

                }
                else if (nudHorasExtras.Value > 0 && chkVT.Checked == false)
                {
                    decimal valorporhora = folha.CalcularValorPorHora(SalarioBruto, ht);
                    decimal totalhorasextras = folha.CalcularValorHoraExtra(valorporhora, porcentagem, he);
                    DSR = folha.CalcularDSR(totalhorasextras, diastrabalhados, finaisdesemana);
                    decimal TotalSalarioBruto = SalarioBruto + totalhorasextras + DSR;
                    valordeatrasos = folha.CalcularValorAtrasos(valorporhora, horasatrasadas);



                    INSS = folha.CalcularINSS(SalarioBruto, TotalSalarioBruto);
                    ValordeDesconto = folha.ValordeDesconto(TotalSalarioBruto, INSS);

                    IR = folha.CalcularIR(TotalSalarioBruto, INSS);
                    FGTS = folha.CalcularFGTS(TotalSalarioBruto);


                    SalarioLiquido = SalarioBruto + totalhorasextras + DSR - INSS - ValordeDesconto - VT - valordeatrasos - nudVR.Value - nudRefeicao.Value - nudConvenio.Value;

                    lblText.Visible = true;
                    lblSalarioLiquido.Visible = true;
                    lblSalarioLiquido.Text = SalarioLiquido.ToString();
                    lblINSS.Text = Math.Round(INSS).ToString();
                    lblFGTS.Text = Math.Round(FGTS).ToString();
                    lblDSR.Text = Math.Round(DSR).ToString();
                    lblIR.Text = Math.Round(IR).ToString();
                    lblVT.Text = Math.Round(VT).ToString();
                    lblValordedesconto.Text = Math.Round(ValordeDesconto).ToString();
                    lblValordeatraso.Text = Math.Round(valordeatrasos).ToString();

                }
                else if (nudHorasExtras.Value == 0 && chkVT.Checked == true)
                {

                    decimal descontodosalariobruto = SalarioBruto / 30 * totaldefaltas;
                    SalarioBruto = SalarioBruto - descontodosalariobruto;

                    decimal valorporhora = folha.CalcularValorPorHora(SalarioBruto, ht);

                    INSS = folha.CalcularINSS(SalarioBruto, SalarioBruto);
                    ValordeDesconto = folha.ValordeDesconto(SalarioBruto, SalarioBruto);
                    valordeatrasos = folha.CalcularValorAtrasos(valorporhora, horasatrasadas);

                    IR = folha.CalcularIR(SalarioBruto, INSS);
                    FGTS = folha.CalcularFGTS(SalarioBruto);
                    VT = SalarioBruto * 0.06m;

                    SalarioLiquido = SalarioBruto - INSS - ValordeDesconto - VT - valordeatrasos - nudVR.Value - nudRefeicao.Value - nudConvenio.Value;

                    lblText.Visible = true;
                    lblSalarioLiquido.Visible = true;
                    lblSalarioLiquido.Text = SalarioLiquido.ToString();
                    lblINSS.Text = INSS.ToString();
                    lblFGTS.Text = FGTS.ToString();
                    lblDSR.Text = DSR.ToString();
                    lblIR.Text = IR.ToString();
                    lblVT.Text = VT.ToString();
                    lblValordedesconto.Text = ValordeDesconto.ToString();
                    lblValordeatraso.Text = valordeatrasos.ToString();
                }

                else if (nudHorasExtras.Value == 0 && chkVT.Checked == false)
                {
                    decimal descontodosalariobruto = SalarioBruto / 30 * totaldefaltas;
                    SalarioBruto = SalarioBruto - descontodosalariobruto;

                    decimal valorporhora = folha.CalcularValorPorHora(SalarioBruto, ht);
                    valordeatrasos = folha.CalcularValorAtrasos(valorporhora, horasatrasadas);

                    INSS = folha.CalcularINSS(SalarioBruto, SalarioBruto);
                    ValordeDesconto = folha.ValordeDesconto(SalarioBruto, SalarioBruto);

                    IR = folha.CalcularIR(SalarioBruto, INSS);
                    FGTS = folha.CalcularFGTS(SalarioBruto);

                    SalarioLiquido = SalarioBruto - INSS - ValordeDesconto - VT - valordeatrasos - nudVR.Value - nudRefeicao.Value - nudConvenio.Value;

                    lblText.Visible = true;
                    lblSalarioLiquido.Visible = true;
                    lblSalarioLiquido.Text = SalarioLiquido.ToString();
                    lblINSS.Text = INSS.ToString();
                    lblFGTS.Text = FGTS.ToString();
                    lblDSR.Text = DSR.ToString();
                    lblIR.Text = IR.ToString();
                    lblVT.Text = VT.ToString();
                    lblValordedesconto.Text = ValordeDesconto.ToString();
                    lblValordeatraso.Text = valordeatrasos.ToString();
                }
            }
            catch (Exception ex)
            {

                MessageBox.Show("Ocorreu o erro : " + ex.Message, "Lottus Store");
            }
        }


    }
}
