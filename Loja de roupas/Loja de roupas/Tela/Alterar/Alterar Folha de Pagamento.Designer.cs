﻿namespace Loja_de_roupas.DB.Telas.Alterar
{
    partial class Alterar_Folha_de_Pagamento
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Alterar_Folha_de_Pagamento));
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.nudFaltaTerSemana = new System.Windows.Forms.NumericUpDown();
            this.nudFaltaQuarSemana = new System.Windows.Forms.NumericUpDown();
            this.nudFaltaSegSemana = new System.Windows.Forms.NumericUpDown();
            this.nudFaltasPriSemana = new System.Windows.Forms.NumericUpDown();
            this.label13 = new System.Windows.Forms.Label();
            this.nudRefeicao = new System.Windows.Forms.NumericUpDown();
            this.lblConvenio = new System.Windows.Forms.Label();
            this.nudConvenio = new System.Windows.Forms.NumericUpDown();
            this.chkConvenio = new System.Windows.Forms.CheckBox();
            this.label12 = new System.Windows.Forms.Label();
            this.nudVR = new System.Windows.Forms.NumericUpDown();
            this.lblValordeatraso = new System.Windows.Forms.Label();
            this.lblValordedesconto = new System.Windows.Forms.Label();
            this.lblVT = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.lblDSR = new System.Windows.Forms.Label();
            this.lblIR = new System.Windows.Forms.Label();
            this.lblFGTS = new System.Windows.Forms.Label();
            this.lblINSS = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.nudHorasAtradas = new System.Windows.Forms.NumericUpDown();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.chkHoraExtra = new System.Windows.Forms.CheckBox();
            this.btnSalvar = new System.Windows.Forms.Button();
            this.btnCalcular = new System.Windows.Forms.Button();
            this.lblSalarioLiquido = new System.Windows.Forms.Label();
            this.lblText = new System.Windows.Forms.Label();
            this.lblPorcentagem = new System.Windows.Forms.Label();
            this.nudPorcentagem = new System.Windows.Forms.NumericUpDown();
            this.lblHoraExtra = new System.Windows.Forms.Label();
            this.nudHorasExtras = new System.Windows.Forms.NumericUpDown();
            this.chkVT = new System.Windows.Forms.CheckBox();
            this.cboFuncionario = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudFaltaTerSemana)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudFaltaQuarSemana)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudFaltaSegSemana)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudFaltasPriSemana)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudRefeicao)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudConvenio)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudVR)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudHorasAtradas)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudPorcentagem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudHorasExtras)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox1.Image = global::Loja_de_roupas.Properties.Resources.lottus_beatifull;
            this.pictureBox1.Location = new System.Drawing.Point(82, 36);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(45, 35);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 70;
            this.pictureBox1.TabStop = false;
            // 
            // nudFaltaTerSemana
            // 
            this.nudFaltaTerSemana.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.nudFaltaTerSemana.Location = new System.Drawing.Point(251, 240);
            this.nudFaltaTerSemana.Margin = new System.Windows.Forms.Padding(4);
            this.nudFaltaTerSemana.Name = "nudFaltaTerSemana";
            this.nudFaltaTerSemana.Size = new System.Drawing.Size(190, 22);
            this.nudFaltaTerSemana.TabIndex = 3;
            // 
            // nudFaltaQuarSemana
            // 
            this.nudFaltaQuarSemana.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.nudFaltaQuarSemana.Location = new System.Drawing.Point(252, 269);
            this.nudFaltaQuarSemana.Margin = new System.Windows.Forms.Padding(4);
            this.nudFaltaQuarSemana.Name = "nudFaltaQuarSemana";
            this.nudFaltaQuarSemana.Size = new System.Drawing.Size(190, 22);
            this.nudFaltaQuarSemana.TabIndex = 4;
            // 
            // nudFaltaSegSemana
            // 
            this.nudFaltaSegSemana.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.nudFaltaSegSemana.Location = new System.Drawing.Point(251, 212);
            this.nudFaltaSegSemana.Margin = new System.Windows.Forms.Padding(4);
            this.nudFaltaSegSemana.Name = "nudFaltaSegSemana";
            this.nudFaltaSegSemana.Size = new System.Drawing.Size(190, 22);
            this.nudFaltaSegSemana.TabIndex = 2;
            // 
            // nudFaltasPriSemana
            // 
            this.nudFaltasPriSemana.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.nudFaltasPriSemana.Location = new System.Drawing.Point(251, 185);
            this.nudFaltasPriSemana.Margin = new System.Windows.Forms.Padding(4);
            this.nudFaltasPriSemana.Name = "nudFaltasPriSemana";
            this.nudFaltasPriSemana.Size = new System.Drawing.Size(190, 22);
            this.nudFaltasPriSemana.TabIndex = 1;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.BackColor = System.Drawing.Color.Transparent;
            this.label13.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.label13.Location = new System.Drawing.Point(472, 159);
            this.label13.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(100, 16);
            this.label13.TabIndex = 111;
            this.label13.Text = "Vale Refeição :";
            // 
            // nudRefeicao
            // 
            this.nudRefeicao.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.nudRefeicao.DecimalPlaces = 2;
            this.nudRefeicao.Location = new System.Drawing.Point(584, 159);
            this.nudRefeicao.Margin = new System.Windows.Forms.Padding(4);
            this.nudRefeicao.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.nudRefeicao.Name = "nudRefeicao";
            this.nudRefeicao.Size = new System.Drawing.Size(193, 22);
            this.nudRefeicao.TabIndex = 7;
            // 
            // lblConvenio
            // 
            this.lblConvenio.AutoSize = true;
            this.lblConvenio.BackColor = System.Drawing.Color.Transparent;
            this.lblConvenio.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.lblConvenio.Location = new System.Drawing.Point(460, 219);
            this.lblConvenio.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblConvenio.Name = "lblConvenio";
            this.lblConvenio.Size = new System.Drawing.Size(119, 16);
            this.lblConvenio.TabIndex = 110;
            this.lblConvenio.Text = "Convênio Médico :";
            // 
            // nudConvenio
            // 
            this.nudConvenio.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.nudConvenio.DecimalPlaces = 2;
            this.nudConvenio.Location = new System.Drawing.Point(584, 219);
            this.nudConvenio.Margin = new System.Windows.Forms.Padding(4);
            this.nudConvenio.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.nudConvenio.Name = "nudConvenio";
            this.nudConvenio.Size = new System.Drawing.Size(193, 22);
            this.nudConvenio.TabIndex = 8;
            // 
            // chkConvenio
            // 
            this.chkConvenio.AutoSize = true;
            this.chkConvenio.BackColor = System.Drawing.Color.Transparent;
            this.chkConvenio.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.chkConvenio.ForeColor = System.Drawing.Color.DarkGray;
            this.chkConvenio.Location = new System.Drawing.Point(684, 189);
            this.chkConvenio.Margin = new System.Windows.Forms.Padding(4);
            this.chkConvenio.Name = "chkConvenio";
            this.chkConvenio.Size = new System.Drawing.Size(129, 20);
            this.chkConvenio.TabIndex = 79;
            this.chkConvenio.Text = "Convênio médico";
            this.chkConvenio.UseVisualStyleBackColor = false;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.Color.Transparent;
            this.label12.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.label12.Location = new System.Drawing.Point(112, 327);
            this.label12.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(119, 16);
            this.label12.TabIndex = 109;
            this.label12.Text = "Vale alimentação :";
            // 
            // nudVR
            // 
            this.nudVR.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.nudVR.DecimalPlaces = 2;
            this.nudVR.Location = new System.Drawing.Point(252, 325);
            this.nudVR.Margin = new System.Windows.Forms.Padding(4);
            this.nudVR.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.nudVR.Name = "nudVR";
            this.nudVR.Size = new System.Drawing.Size(190, 22);
            this.nudVR.TabIndex = 6;
            // 
            // lblValordeatraso
            // 
            this.lblValordeatraso.AutoSize = true;
            this.lblValordeatraso.BackColor = System.Drawing.Color.Transparent;
            this.lblValordeatraso.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.lblValordeatraso.Location = new System.Drawing.Point(543, 416);
            this.lblValordeatraso.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblValordeatraso.Name = "lblValordeatraso";
            this.lblValordeatraso.Size = new System.Drawing.Size(12, 16);
            this.lblValordeatraso.TabIndex = 108;
            this.lblValordeatraso.Text = "-";
            // 
            // lblValordedesconto
            // 
            this.lblValordedesconto.AutoSize = true;
            this.lblValordedesconto.BackColor = System.Drawing.Color.Transparent;
            this.lblValordedesconto.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.lblValordedesconto.Location = new System.Drawing.Point(377, 416);
            this.lblValordedesconto.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblValordedesconto.Name = "lblValordedesconto";
            this.lblValordedesconto.Size = new System.Drawing.Size(12, 16);
            this.lblValordedesconto.TabIndex = 107;
            this.lblValordedesconto.Text = "-";
            // 
            // lblVT
            // 
            this.lblVT.AutoSize = true;
            this.lblVT.BackColor = System.Drawing.Color.Transparent;
            this.lblVT.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.lblVT.Location = new System.Drawing.Point(183, 416);
            this.lblVT.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblVT.Name = "lblVT";
            this.lblVT.Size = new System.Drawing.Size(12, 16);
            this.lblVT.TabIndex = 106;
            this.lblVT.Text = "-";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.BackColor = System.Drawing.Color.Transparent;
            this.label15.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.label15.Location = new System.Drawing.Point(429, 416);
            this.label15.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(109, 16);
            this.label15.TabIndex = 105;
            this.label15.Text = "Valor de atraso  :";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.BackColor = System.Drawing.Color.Transparent;
            this.label16.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.label16.Location = new System.Drawing.Point(251, 416);
            this.label16.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(127, 16);
            this.label16.TabIndex = 104;
            this.label16.Text = "Valor de desconto  :";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.BackColor = System.Drawing.Color.Transparent;
            this.label17.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.label17.Location = new System.Drawing.Point(147, 416);
            this.label17.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(35, 16);
            this.label17.TabIndex = 103;
            this.label17.Text = "VT  :";
            // 
            // lblDSR
            // 
            this.lblDSR.AutoSize = true;
            this.lblDSR.BackColor = System.Drawing.Color.Transparent;
            this.lblDSR.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.lblDSR.Location = new System.Drawing.Point(684, 389);
            this.lblDSR.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblDSR.Name = "lblDSR";
            this.lblDSR.Size = new System.Drawing.Size(12, 16);
            this.lblDSR.TabIndex = 102;
            this.lblDSR.Text = "-";
            // 
            // lblIR
            // 
            this.lblIR.AutoSize = true;
            this.lblIR.BackColor = System.Drawing.Color.Transparent;
            this.lblIR.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.lblIR.Location = new System.Drawing.Point(543, 389);
            this.lblIR.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblIR.Name = "lblIR";
            this.lblIR.Size = new System.Drawing.Size(12, 16);
            this.lblIR.TabIndex = 101;
            this.lblIR.Text = "-";
            // 
            // lblFGTS
            // 
            this.lblFGTS.AutoSize = true;
            this.lblFGTS.BackColor = System.Drawing.Color.Transparent;
            this.lblFGTS.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.lblFGTS.Location = new System.Drawing.Point(377, 389);
            this.lblFGTS.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblFGTS.Name = "lblFGTS";
            this.lblFGTS.Size = new System.Drawing.Size(12, 16);
            this.lblFGTS.TabIndex = 100;
            this.lblFGTS.Text = "-";
            // 
            // lblINSS
            // 
            this.lblINSS.AutoSize = true;
            this.lblINSS.BackColor = System.Drawing.Color.Transparent;
            this.lblINSS.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.lblINSS.Location = new System.Drawing.Point(183, 389);
            this.lblINSS.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblINSS.Name = "lblINSS";
            this.lblINSS.Size = new System.Drawing.Size(12, 16);
            this.lblINSS.TabIndex = 99;
            this.lblINSS.Text = "-";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.ForeColor = System.Drawing.Color.White;
            this.label8.Location = new System.Drawing.Point(640, 389);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(43, 16);
            this.label8.TabIndex = 98;
            this.label8.Text = "DSR :";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.label9.Location = new System.Drawing.Point(511, 389);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(27, 16);
            this.label9.TabIndex = 97;
            this.label9.Text = "IR :";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.label10.Location = new System.Drawing.Point(328, 389);
            this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(50, 16);
            this.label10.TabIndex = 96;
            this.label10.Text = "FGTS :";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.Transparent;
            this.label11.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.label11.Location = new System.Drawing.Point(137, 389);
            this.label11.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(45, 16);
            this.label11.TabIndex = 95;
            this.label11.Text = "INSS :";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.label7.Location = new System.Drawing.Point(115, 299);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(116, 16);
            this.label7.TabIndex = 94;
            this.label7.Text = "Horas Atrasadas :";
            // 
            // nudHorasAtradas
            // 
            this.nudHorasAtradas.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.nudHorasAtradas.Location = new System.Drawing.Point(252, 297);
            this.nudHorasAtradas.Margin = new System.Windows.Forms.Padding(4);
            this.nudHorasAtradas.Name = "nudHorasAtradas";
            this.nudHorasAtradas.Size = new System.Drawing.Size(190, 22);
            this.nudHorasAtradas.TabIndex = 5;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.label5.Location = new System.Drawing.Point(77, 271);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(162, 16);
            this.label5.TabIndex = 93;
            this.label5.Text = "Faltas na quarta semana :";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.label6.Location = new System.Drawing.Point(71, 243);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(169, 16);
            this.label6.TabIndex = 92;
            this.label6.Text = "Faltas na terceira semana :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(66, 215);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(177, 16);
            this.label2.TabIndex = 91;
            this.label2.Text = "Faltas na segunda semana :";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.label4.Location = new System.Drawing.Point(70, 187);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(173, 16);
            this.label4.TabIndex = 90;
            this.label4.Text = "Faltas na primeira semana :";
            // 
            // chkHoraExtra
            // 
            this.chkHoraExtra.AutoSize = true;
            this.chkHoraExtra.BackColor = System.Drawing.Color.Transparent;
            this.chkHoraExtra.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.chkHoraExtra.ForeColor = System.Drawing.Color.DarkGray;
            this.chkHoraExtra.Location = new System.Drawing.Point(590, 189);
            this.chkHoraExtra.Margin = new System.Windows.Forms.Padding(4);
            this.chkHoraExtra.Name = "chkHoraExtra";
            this.chkHoraExtra.Size = new System.Drawing.Size(86, 20);
            this.chkHoraExtra.TabIndex = 78;
            this.chkHoraExtra.Text = "Hora extra";
            this.chkHoraExtra.UseVisualStyleBackColor = false;
            // 
            // btnSalvar
            // 
            this.btnSalvar.BackColor = System.Drawing.Color.Transparent;
            this.btnSalvar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSalvar.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.btnSalvar.Location = new System.Drawing.Point(643, 337);
            this.btnSalvar.Margin = new System.Windows.Forms.Padding(4);
            this.btnSalvar.Name = "btnSalvar";
            this.btnSalvar.Size = new System.Drawing.Size(121, 36);
            this.btnSalvar.TabIndex = 12;
            this.btnSalvar.Text = "Salvar";
            this.btnSalvar.UseVisualStyleBackColor = false;
            // 
            // btnCalcular
            // 
            this.btnCalcular.BackColor = System.Drawing.Color.Transparent;
            this.btnCalcular.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCalcular.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.btnCalcular.Location = new System.Drawing.Point(473, 337);
            this.btnCalcular.Margin = new System.Windows.Forms.Padding(4);
            this.btnCalcular.Name = "btnCalcular";
            this.btnCalcular.Size = new System.Drawing.Size(116, 36);
            this.btnCalcular.TabIndex = 11;
            this.btnCalcular.Text = "Calcular";
            this.btnCalcular.UseVisualStyleBackColor = false;
            this.btnCalcular.Click += new System.EventHandler(this.btnCalcular_Click_1);
            // 
            // lblSalarioLiquido
            // 
            this.lblSalarioLiquido.AutoSize = true;
            this.lblSalarioLiquido.BackColor = System.Drawing.Color.Transparent;
            this.lblSalarioLiquido.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.lblSalarioLiquido.Location = new System.Drawing.Point(581, 308);
            this.lblSalarioLiquido.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblSalarioLiquido.Name = "lblSalarioLiquido";
            this.lblSalarioLiquido.Size = new System.Drawing.Size(12, 16);
            this.lblSalarioLiquido.TabIndex = 83;
            this.lblSalarioLiquido.Text = "-";
            // 
            // lblText
            // 
            this.lblText.AutoSize = true;
            this.lblText.BackColor = System.Drawing.Color.Transparent;
            this.lblText.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.lblText.Location = new System.Drawing.Point(458, 308);
            this.lblText.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblText.Name = "lblText";
            this.lblText.Size = new System.Drawing.Size(104, 16);
            this.lblText.TabIndex = 89;
            this.lblText.Text = "Salário Líquido :";
            // 
            // lblPorcentagem
            // 
            this.lblPorcentagem.AutoSize = true;
            this.lblPorcentagem.BackColor = System.Drawing.Color.Transparent;
            this.lblPorcentagem.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.lblPorcentagem.Location = new System.Drawing.Point(477, 280);
            this.lblPorcentagem.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblPorcentagem.Name = "lblPorcentagem";
            this.lblPorcentagem.Size = new System.Drawing.Size(95, 16);
            this.lblPorcentagem.TabIndex = 88;
            this.lblPorcentagem.Text = "Porcentagem :";
            // 
            // nudPorcentagem
            // 
            this.nudPorcentagem.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.nudPorcentagem.DecimalPlaces = 2;
            this.nudPorcentagem.Location = new System.Drawing.Point(584, 278);
            this.nudPorcentagem.Margin = new System.Windows.Forms.Padding(4);
            this.nudPorcentagem.Name = "nudPorcentagem";
            this.nudPorcentagem.Size = new System.Drawing.Size(191, 22);
            this.nudPorcentagem.TabIndex = 10;
            // 
            // lblHoraExtra
            // 
            this.lblHoraExtra.AutoSize = true;
            this.lblHoraExtra.BackColor = System.Drawing.Color.Transparent;
            this.lblHoraExtra.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.lblHoraExtra.Location = new System.Drawing.Point(481, 249);
            this.lblHoraExtra.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblHoraExtra.Name = "lblHoraExtra";
            this.lblHoraExtra.Size = new System.Drawing.Size(91, 16);
            this.lblHoraExtra.TabIndex = 87;
            this.lblHoraExtra.Text = "Horas Extras :";
            // 
            // nudHorasExtras
            // 
            this.nudHorasExtras.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.nudHorasExtras.Location = new System.Drawing.Point(584, 247);
            this.nudHorasExtras.Margin = new System.Windows.Forms.Padding(4);
            this.nudHorasExtras.Name = "nudHorasExtras";
            this.nudHorasExtras.Size = new System.Drawing.Size(191, 22);
            this.nudHorasExtras.TabIndex = 9;
            // 
            // chkVT
            // 
            this.chkVT.AutoSize = true;
            this.chkVT.BackColor = System.Drawing.Color.Transparent;
            this.chkVT.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.chkVT.ForeColor = System.Drawing.Color.DarkGray;
            this.chkVT.Location = new System.Drawing.Point(471, 188);
            this.chkVT.Margin = new System.Windows.Forms.Padding(4);
            this.chkVT.Name = "chkVT";
            this.chkVT.Size = new System.Drawing.Size(115, 20);
            this.chkVT.TabIndex = 77;
            this.chkVT.Text = "Vale transporte";
            this.chkVT.UseVisualStyleBackColor = false;
            // 
            // cboFuncionario
            // 
            this.cboFuncionario.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cboFuncionario.FormattingEnabled = true;
            this.cboFuncionario.Location = new System.Drawing.Point(251, 156);
            this.cboFuncionario.Margin = new System.Windows.Forms.Padding(4);
            this.cboFuncionario.Name = "cboFuncionario";
            this.cboFuncionario.Size = new System.Drawing.Size(191, 24);
            this.cboFuncionario.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.label1.Location = new System.Drawing.Point(137, 159);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(84, 16);
            this.label1.TabIndex = 86;
            this.label1.Text = "Funcionário :";
            // 
            // Alterar_Folha_de_Pagamento
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::Loja_de_roupas.Properties.Resources.Lotus_Store_TELA;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(862, 478);
            this.Controls.Add(this.nudFaltaTerSemana);
            this.Controls.Add(this.nudFaltaQuarSemana);
            this.Controls.Add(this.nudFaltaSegSemana);
            this.Controls.Add(this.nudFaltasPriSemana);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.nudRefeicao);
            this.Controls.Add(this.lblConvenio);
            this.Controls.Add(this.nudConvenio);
            this.Controls.Add(this.chkConvenio);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.nudVR);
            this.Controls.Add(this.lblValordeatraso);
            this.Controls.Add(this.lblValordedesconto);
            this.Controls.Add(this.lblVT);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.lblDSR);
            this.Controls.Add(this.lblIR);
            this.Controls.Add(this.lblFGTS);
            this.Controls.Add(this.lblINSS);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.nudHorasAtradas);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.chkHoraExtra);
            this.Controls.Add(this.btnSalvar);
            this.Controls.Add(this.btnCalcular);
            this.Controls.Add(this.lblSalarioLiquido);
            this.Controls.Add(this.lblText);
            this.Controls.Add(this.lblPorcentagem);
            this.Controls.Add(this.nudPorcentagem);
            this.Controls.Add(this.lblHoraExtra);
            this.Controls.Add(this.nudHorasExtras);
            this.Controls.Add(this.chkVT);
            this.Controls.Add(this.cboFuncionario);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pictureBox1);
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Alterar_Folha_de_Pagamento";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Alterar Folha de Pagamento";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudFaltaTerSemana)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudFaltaQuarSemana)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudFaltaSegSemana)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudFaltasPriSemana)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudRefeicao)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudConvenio)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudVR)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudHorasAtradas)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudPorcentagem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudHorasExtras)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.NumericUpDown nudFaltaTerSemana;
        private System.Windows.Forms.NumericUpDown nudFaltaQuarSemana;
        private System.Windows.Forms.NumericUpDown nudFaltaSegSemana;
        private System.Windows.Forms.NumericUpDown nudFaltasPriSemana;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.NumericUpDown nudRefeicao;
        private System.Windows.Forms.Label lblConvenio;
        private System.Windows.Forms.NumericUpDown nudConvenio;
        private System.Windows.Forms.CheckBox chkConvenio;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.NumericUpDown nudVR;
        private System.Windows.Forms.Label lblValordeatraso;
        private System.Windows.Forms.Label lblValordedesconto;
        private System.Windows.Forms.Label lblVT;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label lblDSR;
        private System.Windows.Forms.Label lblIR;
        private System.Windows.Forms.Label lblFGTS;
        private System.Windows.Forms.Label lblINSS;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.NumericUpDown nudHorasAtradas;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.CheckBox chkHoraExtra;
        private System.Windows.Forms.Button btnSalvar;
        private System.Windows.Forms.Button btnCalcular;
        private System.Windows.Forms.Label lblSalarioLiquido;
        private System.Windows.Forms.Label lblText;
        private System.Windows.Forms.Label lblPorcentagem;
        private System.Windows.Forms.NumericUpDown nudPorcentagem;
        private System.Windows.Forms.Label lblHoraExtra;
        private System.Windows.Forms.NumericUpDown nudHorasExtras;
        private System.Windows.Forms.CheckBox chkVT;
        private System.Windows.Forms.ComboBox cboFuncionario;
        private System.Windows.Forms.Label label1;
    }
}