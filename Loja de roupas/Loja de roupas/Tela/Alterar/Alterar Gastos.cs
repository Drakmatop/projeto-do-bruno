﻿using Loja_de_roupas.DB.Gastos;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Loja_de_roupas.DB.Telas.Alterar
{
    public partial class Alterar_Gastos : Form
    {
        public Alterar_Gastos()
        {
            InitializeComponent();
        }
        GastoDTO gasto = new GastoDTO();
        public void LoadScreen(GastoDTO gasto)
        {
            this.gasto = gasto;
            txtGasto.Text = gasto.Gasto;
            nudValor.Value = gasto.Valor;
            cboTipo.SelectedItem = gasto.Tipo;
            dtpPagamento.Value = gasto.Pagamento;
        }
        private void btnCadastrar_Click(object sender, EventArgs e)
        {
            try
            {
                GastoDTO dto = new GastoDTO();

                dto.Gasto = txtGasto.Text;
                dto.Tipo = cboTipo.SelectedItem.ToString();
                dto.Pagamento = dtpPagamento.Value;
                dto.Valor = nudValor.Value;

                GastoBusiness business = new GastoBusiness();
                business.Alterar(dto);
                MessageBox.Show("Gasto alterado com sucesso", "Lottus Store");
                txtGasto.Clear();
                nudValor.Value = 0.00m;
                dtpPagamento.Value = DateTime.Now;
            }
            catch (Exception ex)
            {

                MessageBox.Show("Ocorreu o erro : " + ex.Message, "Lottus Store");
            }
        }

        private void cboTipo_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
