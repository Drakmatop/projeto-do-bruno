﻿using Loja_de_roupas.DB.Fornecedor;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Loja_de_roupas.DB.Telas.Alterar
{
    public partial class Alterar_Fornecedor : Form
    {
        public Alterar_Fornecedor()
        {
            InitializeComponent();
        }
        FornecedorDTO fornecedor = new FornecedorDTO();
        public void LoadScreen(FornecedorDTO fornecedor)
        {
            this.fornecedor = fornecedor;
            txtNome.Text = fornecedor.Nome;
            txtTelefone.Text = fornecedor.Telefone;
            txtEmail.Text = fornecedor.Email;
            txtCep.Text = fornecedor.CEP;
        }
        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                FornecedorDTO dto = new FornecedorDTO();
                dto.Nome = txtNome.Text.Trim();
                dto.Telefone = txtTelefone.Text.Trim();
                dto.Email = txtEmail.Text.Trim();
                dto.CEP = txtCep.Text.Trim();

                FornecedorBusiness business = new FornecedorBusiness();
                business.Alterar(dto);
                MessageBox.Show("Alteração efetuada com sucesso", "Lottus Store");
            }
            catch (Exception ex)
            {

                MessageBox.Show("Ocorreu o erro : " + ex.Message, "Lottus Store");
            }
        }
    }
}
