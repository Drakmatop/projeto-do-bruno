﻿using Loja_de_roupas.DB.Base.Clientes;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Loja_de_roupas.DB.Telas.Alterar
{
    public partial class Alterar_Cliente : Form
    {
        public Alterar_Cliente()
        {
            InitializeComponent();
        }
        ClienteDTO cliente;
        public void LoadScreen(ClienteDTO cliente)
        {
            this.cliente = cliente;
            txtComplemento.Text = cliente.Complemento;
            txtCpf.Text = cliente.CPF;
            txtNome.Text = cliente.Nome;
            txtSobrenome.Text = cliente.Sobrenome;
            txtTelefone.Text = cliente.Telefone;
            txtCep.Text = cliente.CEP;
            nudNcasa.Value = cliente.Numero;
        }

        private void btnCadastrar_Click(object sender, EventArgs e)
        {
            try
            {
                ClienteDTO dto = new ClienteDTO();
                dto.Nome = txtNome.Text.Trim();
                dto.Sobrenome = txtSobrenome.Text.Trim();
                dto.Telefone = txtTelefone.Text.Trim();
                dto.CEP = txtCep.Text.Trim();
                dto.Complemento = txtComplemento.Text.Trim();
                dto.Numero = Convert.ToInt32(nudNcasa.Value);
                dto.CPF = txtCpf.Text.Trim();

                ClienteBusiness business = new ClienteBusiness();
                business.Alterar(dto);
                

                MessageBox.Show("Alteração efetuada com sucesso", "Lottus Store");
            }
            catch (Exception ex)
            {

                MessageBox.Show("Ocorreu o erro : " + ex.Message, "Lottus Store");
            }

        }
    }
}
