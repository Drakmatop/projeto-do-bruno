﻿using Loja_de_roupas.DB.Base.Categoria;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Loja_de_roupas.DB.Telas.Alterar
{
    public partial class Alterar_Categoria : Form
    {
        public Alterar_Categoria()
        {
            InitializeComponent();
        }
        CategoriaDTO categoria;
        public void LoadScreen(CategoriaDTO categoria)
        {
            this.categoria = categoria;
            txtCategoria.Text = categoria.Nome;
        }
        private void button1_Click(object sender, EventArgs e)
        {
            CategoriaDTO dto = new CategoriaDTO();
            dto.Nome = txtCategoria.Text;
            dto.Id = categoria.Id;

            CategoriaBusiness business = new CategoriaBusiness();
            business.Alterar(dto);
            MessageBox.Show("Alteração efetuada com sucesso", "Lottus Store");
        }

        private void txtCategoria_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
