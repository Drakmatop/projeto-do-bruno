﻿using Loja_de_roupas.DB.Base.Categoria;
using Loja_de_roupas.DB.Fornecedor;
using Loja_de_roupas.DB.Produto;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Loja_de_roupas.DB.Telas.Alterar
{
    public partial class Alterar_Produto : Form
    {
        public Alterar_Produto()
        {
            InitializeComponent();
            CarregarCombos();
        }
        void CarregarCombos()
        {

            CategoriaBusiness catebus = new CategoriaBusiness();
            List<CategoriaDTO> cateList = catebus.Listar();

            cboCategoria.DisplayMember = nameof(CategoriaDTO.Nome);
            cboCategoria.ValueMember = nameof(CategoriaDTO.Id);
            cboCategoria.DataSource = cateList;


            string nome = "";
            FornecedorBusiness fornbus = new FornecedorBusiness();
            List<FornecedorDTO> fornList = fornbus.Consultar(nome);

            cboFornecedor.DisplayMember = nameof(FornecedorDTO.Nome);
            cboFornecedor.ValueMember = nameof(FornecedorDTO.Id);
            cboFornecedor.DataSource = fornList;

        }

        ProdutoDTO produto = new ProdutoDTO();
        public void LoadScreen(ProdutoDTO produto)
        {
            this.produto = produto;
            txtProduto.Text = produto.Nome;
            txtTamanho.Text = produto.Tamanho;
            txtCor.Text = produto.Cor;
            nudPrecoCompra.Value = produto.PrecoCompra;
            nudPrecoVenda.Value = produto.PrecoVenda;

        }

        private void bnt1_Click_1(object sender, EventArgs e)
        {
            try
            {
                CategoriaDTO cat = cboCategoria.SelectedItem as CategoriaDTO;
                FornecedorDTO forn = cboFornecedor.SelectedItem as FornecedorDTO;


                ProdutoDTO dto = new ProdutoDTO();
                dto.Nome = txtProduto.Text.Trim();
                dto.Cor = txtCor.Text.Trim();
                dto.PrecoCompra = nudPrecoCompra.Value;
                dto.PrecoVenda = nudPrecoVenda.Value;
                dto.Tamanho = txtTamanho.Text.Trim();
                dto.CategoriaId = cat.Id;
                dto.FornecedorId = forn.Id;


                ProdutoBusiness_ business = new ProdutoBusiness_();
                business.Alterar(dto);
               

                MessageBox.Show("Alteração efetuada com sucesso", "Lottus Store");
            }
            catch (Exception ex)
            {

                MessageBox.Show("Ocorreu o erro : " + ex.Message, "Lottus Store");
            }
        }

        private void cboCategoria_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((Keys)e.KeyChar == Keys.Enter)
            {
                bnt1_Click_1(null, null);
            }
        }
    }
}
