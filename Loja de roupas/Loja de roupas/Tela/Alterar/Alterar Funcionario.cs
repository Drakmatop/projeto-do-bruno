﻿using Loja_de_roupas.DB.Funcionario;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Loja_de_roupas.DB.Telas.Alterar
{
    public partial class Alterar_Funcionario : Form
    {
        public Alterar_Funcionario()
        {
            InitializeComponent();
        }
        FuncionarioDTO funcionario;
        public void LoadScreen(FuncionarioDTO funcionario)
        {
            this.funcionario = funcionario;
            rndADM.Checked = funcionario.PermissaoAdm;
            rdnVendedor.Checked = funcionario.PermissaoVendedor;
            txtNome.Text = funcionario.Nome;
            txtSobrenome.Text = funcionario.Sobrenome;
            txtTelefone.Text = funcionario.Telefone;
            txtCep.Text = funcionario.Cep;
            dtpData.Value = funcionario.DataNascimento;
            txtUsuario.Text = funcionario.Login;
            txtSenha.Text = funcionario.Senha;
            txtSalarioB.Text = funcionario.SalarioBruto.ToString();
            dtpData.Value = funcionario.DataNascimento;
        }

        private void bnt1_Click(object sender, EventArgs e)
        {
            try
            {
                bool a = rndADM.Checked;
                bool b = rdnVendedor.Checked;


                funcionario.Nome = txtNome.Text.Trim();
                funcionario.Sobrenome = txtSobrenome.Text.Trim();
                funcionario.Telefone = txtTelefone.Text.Trim();
                funcionario.Cep = txtCep.Text.Trim();
                funcionario.DataNascimento = dtpData.Value;
                funcionario.Login = txtUsuario.Text.Trim();
                funcionario.Senha = txtSenha.Text.Trim();
                funcionario.PermissaoAdm = a;
                funcionario.PermissaoVendedor = b;
                funcionario.SalarioBruto = Convert.ToDecimal(txtSalarioB.Text);
                FuncionarioBusiness business = new FuncionarioBusiness();
                business.Alterar(funcionario);

                MessageBox.Show("Alteração efetuada com sucesso", "Lottus Store");
            }
            catch (Exception ex)
            {

                MessageBox.Show("Ocorreu um erro" + ex.Message, "Lottus Store");
            }
        }

        private void txtSenha_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
