﻿using Loja_de_roupas;
using nsf._2018.diferenciais.RedesSociais;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace nsf._2018.diferenciais
{
    public partial class frmTwitter : Form
    {
        public frmTwitter()
        {
            InitializeComponent();
        }

        private void frmQrCode_Load(object sender, EventArgs e)
        {

        }
        Twitter twitter = new Twitter();

        private void btnEnviar_Click(object sender, EventArgs e)
        {

        }

        private void btnCarregar_Click(object sender, EventArgs e)
        {

            
        }

        private void groupBox2_Enter(object sender, EventArgs e)
        {

        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void txtMensagem_TextChanged(object sender, EventArgs e)
        {

        }

        private void btnEnviar_Click_1(object sender, EventArgs e)
        {
            if (txtMensagem.Text == string.Empty)
            {
                MessageBox.Show("Escreva algo para ser twittado", "Lottus Store");
            }
            twitter.Enviar(txtMensagem.Text);
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            frmMenu tela = new frmMenu();
            tela.Show();
            this.Hide();
        }

        private void txtMensagem_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((Keys)e.KeyChar == Keys.Enter)
            {
                btnEnviar_Click_1(null, null);
            }
        }
    }
}
