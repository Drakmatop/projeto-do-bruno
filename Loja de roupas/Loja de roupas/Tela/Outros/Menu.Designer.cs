﻿namespace Loja_de_roupas
{
    partial class frmMenu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMenu));
            this.lblFalar = new System.Windows.Forms.Label();
            this.menuStrip2 = new System.Windows.Forms.MenuStrip();
            this.clienteToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.cadastrarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.consultarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.funcionariosToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.cadastrarToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.consultarToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.produtoToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.cadastrarToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.consultarToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.fornecedorToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.cadastrarToolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.consultarToolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.pedidoToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.cadastrarToolStripMenuItem4 = new System.Windows.Forms.ToolStripMenuItem();
            this.consultarToolStripMenuItem4 = new System.Windows.Forms.ToolStripMenuItem();
            this.cartegoriaToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.cadastrarToolStripMenuItem5 = new System.Windows.Forms.ToolStripMenuItem();
            this.consultarToolStripMenuItem5 = new System.Windows.Forms.ToolStripMenuItem();
            this.folhaDePagaentoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cadastrarToolStripMenuItem6 = new System.Windows.Forms.ToolStripMenuItem();
            this.consultarToolStripMenuItem6 = new System.Windows.Forms.ToolStripMenuItem();
            this.comprasToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.cadastrarToolStripMenuItem7 = new System.Windows.Forms.ToolStripMenuItem();
            this.consultarToolStripMenuItem7 = new System.Windows.Forms.ToolStripMenuItem();
            this.loginToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.consultarToolStripMenuItem8 = new System.Windows.Forms.ToolStripMenuItem();
            this.todosOsDadosToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.consultarToolStripMenuItem9 = new System.Windows.Forms.ToolStripMenuItem();
            this.fluxoDeCaixaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.consultarToolStripMenuItem10 = new System.Windows.Forms.ToolStripMenuItem();
            this.gastosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cadastrarToolStripMenuItem11 = new System.Windows.Forms.ToolStripMenuItem();
            this.consultarToolStripMenuItem12 = new System.Windows.Forms.ToolStripMenuItem();
            this.cadastrarToolStripMenuItem10 = new System.Windows.Forms.ToolStripMenuItem();
            this.consultarToolStripMenuItem11 = new System.Windows.Forms.ToolStripMenuItem();
            this.emailsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.enviarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.twitterToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.twittsGeraisToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.meusTwittsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.twittarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.imgFalar = new System.Windows.Forms.PictureBox();
            this.imgParar = new System.Windows.Forms.PictureBox();
            this.lblProcess = new System.Windows.Forms.Label();
            this.imgRec = new System.Windows.Forms.PictureBox();
            this.menuStrip2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imgFalar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgParar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgRec)).BeginInit();
            this.SuspendLayout();
            // 
            // lblFalar
            // 
            this.lblFalar.AutoSize = true;
            this.lblFalar.BackColor = System.Drawing.Color.Transparent;
            this.lblFalar.Font = new System.Drawing.Font("Microsoft Sans Serif", 99.75F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFalar.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.lblFalar.Location = new System.Drawing.Point(120, 207);
            this.lblFalar.Name = "lblFalar";
            this.lblFalar.Size = new System.Drawing.Size(781, 152);
            this.lblFalar.TabIndex = 12;
            this.lblFalar.Text = "Lottus Store";
            this.lblFalar.Click += new System.EventHandler(this.label1_Click);
            // 
            // menuStrip2
            // 
            this.menuStrip2.BackColor = System.Drawing.Color.Transparent;
            this.menuStrip2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.menuStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.clienteToolStripMenuItem1,
            this.funcionariosToolStripMenuItem1,
            this.produtoToolStripMenuItem1,
            this.fornecedorToolStripMenuItem1,
            this.pedidoToolStripMenuItem1,
            this.cartegoriaToolStripMenuItem1,
            this.folhaDePagaentoToolStripMenuItem,
            this.comprasToolStripMenuItem1,
            this.loginToolStripMenuItem1,
            this.todosOsDadosToolStripMenuItem1,
            this.fluxoDeCaixaToolStripMenuItem,
            this.gastosToolStripMenuItem,
            this.cadastrarToolStripMenuItem10,
            this.emailsToolStripMenuItem,
            this.twitterToolStripMenuItem});
            this.menuStrip2.Location = new System.Drawing.Point(0, 423);
            this.menuStrip2.Name = "menuStrip2";
            this.menuStrip2.Size = new System.Drawing.Size(1083, 24);
            this.menuStrip2.TabIndex = 0;
            this.menuStrip2.Text = "menuStrip2";
            // 
            // clienteToolStripMenuItem1
            // 
            this.clienteToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cadastrarToolStripMenuItem,
            this.consultarToolStripMenuItem});
            this.clienteToolStripMenuItem1.ForeColor = System.Drawing.SystemColors.ButtonShadow;
            this.clienteToolStripMenuItem1.Name = "clienteToolStripMenuItem1";
            this.clienteToolStripMenuItem1.Size = new System.Drawing.Size(59, 20);
            this.clienteToolStripMenuItem1.Text = "Cliente ";
            // 
            // cadastrarToolStripMenuItem
            // 
            this.cadastrarToolStripMenuItem.Name = "cadastrarToolStripMenuItem";
            this.cadastrarToolStripMenuItem.Size = new System.Drawing.Size(125, 22);
            this.cadastrarToolStripMenuItem.Text = "Cadastrar";
            this.cadastrarToolStripMenuItem.Click += new System.EventHandler(this.cadastrarToolStripMenuItem_Click_1);
            // 
            // consultarToolStripMenuItem
            // 
            this.consultarToolStripMenuItem.Name = "consultarToolStripMenuItem";
            this.consultarToolStripMenuItem.Size = new System.Drawing.Size(125, 22);
            this.consultarToolStripMenuItem.Text = "Consultar";
            this.consultarToolStripMenuItem.Click += new System.EventHandler(this.consultarToolStripMenuItem_Click_1);
            // 
            // funcionariosToolStripMenuItem1
            // 
            this.funcionariosToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cadastrarToolStripMenuItem1,
            this.consultarToolStripMenuItem1});
            this.funcionariosToolStripMenuItem1.ForeColor = System.Drawing.SystemColors.ButtonShadow;
            this.funcionariosToolStripMenuItem1.Name = "funcionariosToolStripMenuItem1";
            this.funcionariosToolStripMenuItem1.Size = new System.Drawing.Size(90, 20);
            this.funcionariosToolStripMenuItem1.Text = "Funcionarios ";
            // 
            // cadastrarToolStripMenuItem1
            // 
            this.cadastrarToolStripMenuItem1.Name = "cadastrarToolStripMenuItem1";
            this.cadastrarToolStripMenuItem1.Size = new System.Drawing.Size(125, 22);
            this.cadastrarToolStripMenuItem1.Text = "Cadastrar";
            this.cadastrarToolStripMenuItem1.Click += new System.EventHandler(this.cadastrarToolStripMenuItem1_Click);
            // 
            // consultarToolStripMenuItem1
            // 
            this.consultarToolStripMenuItem1.Name = "consultarToolStripMenuItem1";
            this.consultarToolStripMenuItem1.Size = new System.Drawing.Size(125, 22);
            this.consultarToolStripMenuItem1.Text = "Consultar";
            this.consultarToolStripMenuItem1.Click += new System.EventHandler(this.consultarToolStripMenuItem1_Click_1);
            // 
            // produtoToolStripMenuItem1
            // 
            this.produtoToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cadastrarToolStripMenuItem2,
            this.consultarToolStripMenuItem2});
            this.produtoToolStripMenuItem1.ForeColor = System.Drawing.SystemColors.ButtonShadow;
            this.produtoToolStripMenuItem1.Name = "produtoToolStripMenuItem1";
            this.produtoToolStripMenuItem1.Size = new System.Drawing.Size(62, 20);
            this.produtoToolStripMenuItem1.Text = "Produto";
            // 
            // cadastrarToolStripMenuItem2
            // 
            this.cadastrarToolStripMenuItem2.Name = "cadastrarToolStripMenuItem2";
            this.cadastrarToolStripMenuItem2.Size = new System.Drawing.Size(125, 22);
            this.cadastrarToolStripMenuItem2.Text = "Cadastrar";
            this.cadastrarToolStripMenuItem2.Click += new System.EventHandler(this.cadastrarToolStripMenuItem2_Click_1);
            // 
            // consultarToolStripMenuItem2
            // 
            this.consultarToolStripMenuItem2.Name = "consultarToolStripMenuItem2";
            this.consultarToolStripMenuItem2.Size = new System.Drawing.Size(125, 22);
            this.consultarToolStripMenuItem2.Text = "Consultar";
            this.consultarToolStripMenuItem2.Click += new System.EventHandler(this.consultarToolStripMenuItem2_Click);
            // 
            // fornecedorToolStripMenuItem1
            // 
            this.fornecedorToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cadastrarToolStripMenuItem3,
            this.consultarToolStripMenuItem3});
            this.fornecedorToolStripMenuItem1.ForeColor = System.Drawing.SystemColors.ButtonShadow;
            this.fornecedorToolStripMenuItem1.Name = "fornecedorToolStripMenuItem1";
            this.fornecedorToolStripMenuItem1.Size = new System.Drawing.Size(79, 20);
            this.fornecedorToolStripMenuItem1.Text = "Fornecedor";
            // 
            // cadastrarToolStripMenuItem3
            // 
            this.cadastrarToolStripMenuItem3.Name = "cadastrarToolStripMenuItem3";
            this.cadastrarToolStripMenuItem3.Size = new System.Drawing.Size(125, 22);
            this.cadastrarToolStripMenuItem3.Text = "Cadastrar";
            this.cadastrarToolStripMenuItem3.Click += new System.EventHandler(this.cadastrarToolStripMenuItem3_Click_1);
            // 
            // consultarToolStripMenuItem3
            // 
            this.consultarToolStripMenuItem3.Name = "consultarToolStripMenuItem3";
            this.consultarToolStripMenuItem3.Size = new System.Drawing.Size(125, 22);
            this.consultarToolStripMenuItem3.Text = "Consultar";
            this.consultarToolStripMenuItem3.Click += new System.EventHandler(this.consultarToolStripMenuItem3_Click_1);
            // 
            // pedidoToolStripMenuItem1
            // 
            this.pedidoToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cadastrarToolStripMenuItem4,
            this.consultarToolStripMenuItem4});
            this.pedidoToolStripMenuItem1.ForeColor = System.Drawing.SystemColors.ButtonShadow;
            this.pedidoToolStripMenuItem1.Name = "pedidoToolStripMenuItem1";
            this.pedidoToolStripMenuItem1.Size = new System.Drawing.Size(56, 20);
            this.pedidoToolStripMenuItem1.Text = "Pedido";
            // 
            // cadastrarToolStripMenuItem4
            // 
            this.cadastrarToolStripMenuItem4.Name = "cadastrarToolStripMenuItem4";
            this.cadastrarToolStripMenuItem4.Size = new System.Drawing.Size(125, 22);
            this.cadastrarToolStripMenuItem4.Text = "Cadastrar";
            this.cadastrarToolStripMenuItem4.Click += new System.EventHandler(this.cadastrarToolStripMenuItem4_Click_1);
            // 
            // consultarToolStripMenuItem4
            // 
            this.consultarToolStripMenuItem4.Name = "consultarToolStripMenuItem4";
            this.consultarToolStripMenuItem4.Size = new System.Drawing.Size(125, 22);
            this.consultarToolStripMenuItem4.Text = "Consultar";
            this.consultarToolStripMenuItem4.Click += new System.EventHandler(this.consultarToolStripMenuItem4_Click_1);
            // 
            // cartegoriaToolStripMenuItem1
            // 
            this.cartegoriaToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cadastrarToolStripMenuItem5,
            this.consultarToolStripMenuItem5});
            this.cartegoriaToolStripMenuItem1.ForeColor = System.Drawing.SystemColors.ButtonShadow;
            this.cartegoriaToolStripMenuItem1.Name = "cartegoriaToolStripMenuItem1";
            this.cartegoriaToolStripMenuItem1.Size = new System.Drawing.Size(70, 20);
            this.cartegoriaToolStripMenuItem1.Text = "Categoria";
            // 
            // cadastrarToolStripMenuItem5
            // 
            this.cadastrarToolStripMenuItem5.Name = "cadastrarToolStripMenuItem5";
            this.cadastrarToolStripMenuItem5.Size = new System.Drawing.Size(125, 22);
            this.cadastrarToolStripMenuItem5.Text = "Cadastrar";
            this.cadastrarToolStripMenuItem5.Click += new System.EventHandler(this.cadastrarToolStripMenuItem5_Click_1);
            // 
            // consultarToolStripMenuItem5
            // 
            this.consultarToolStripMenuItem5.Name = "consultarToolStripMenuItem5";
            this.consultarToolStripMenuItem5.Size = new System.Drawing.Size(125, 22);
            this.consultarToolStripMenuItem5.Text = "Consultar";
            this.consultarToolStripMenuItem5.Click += new System.EventHandler(this.consultarToolStripMenuItem5_Click_1);
            // 
            // folhaDePagaentoToolStripMenuItem
            // 
            this.folhaDePagaentoToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cadastrarToolStripMenuItem6,
            this.consultarToolStripMenuItem6});
            this.folhaDePagaentoToolStripMenuItem.ForeColor = System.Drawing.SystemColors.ButtonShadow;
            this.folhaDePagaentoToolStripMenuItem.Name = "folhaDePagaentoToolStripMenuItem";
            this.folhaDePagaentoToolStripMenuItem.Size = new System.Drawing.Size(128, 20);
            this.folhaDePagaentoToolStripMenuItem.Text = "Folha de Pagamento";
            // 
            // cadastrarToolStripMenuItem6
            // 
            this.cadastrarToolStripMenuItem6.Name = "cadastrarToolStripMenuItem6";
            this.cadastrarToolStripMenuItem6.Size = new System.Drawing.Size(125, 22);
            this.cadastrarToolStripMenuItem6.Text = "Cadastrar";
            this.cadastrarToolStripMenuItem6.Click += new System.EventHandler(this.cadastrarToolStripMenuItem6_Click_1);
            // 
            // consultarToolStripMenuItem6
            // 
            this.consultarToolStripMenuItem6.Name = "consultarToolStripMenuItem6";
            this.consultarToolStripMenuItem6.Size = new System.Drawing.Size(125, 22);
            this.consultarToolStripMenuItem6.Text = "Consultar";
            this.consultarToolStripMenuItem6.Click += new System.EventHandler(this.consultarToolStripMenuItem6_Click_1);
            // 
            // comprasToolStripMenuItem1
            // 
            this.comprasToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cadastrarToolStripMenuItem7,
            this.consultarToolStripMenuItem7});
            this.comprasToolStripMenuItem1.ForeColor = System.Drawing.SystemColors.ButtonShadow;
            this.comprasToolStripMenuItem1.Name = "comprasToolStripMenuItem1";
            this.comprasToolStripMenuItem1.Size = new System.Drawing.Size(67, 20);
            this.comprasToolStripMenuItem1.Text = "Compras";
            // 
            // cadastrarToolStripMenuItem7
            // 
            this.cadastrarToolStripMenuItem7.Name = "cadastrarToolStripMenuItem7";
            this.cadastrarToolStripMenuItem7.Size = new System.Drawing.Size(125, 22);
            this.cadastrarToolStripMenuItem7.Text = "Cadastrar";
            this.cadastrarToolStripMenuItem7.Click += new System.EventHandler(this.cadastrarToolStripMenuItem7_Click);
            // 
            // consultarToolStripMenuItem7
            // 
            this.consultarToolStripMenuItem7.Name = "consultarToolStripMenuItem7";
            this.consultarToolStripMenuItem7.Size = new System.Drawing.Size(125, 22);
            this.consultarToolStripMenuItem7.Text = "Consultar";
            this.consultarToolStripMenuItem7.Click += new System.EventHandler(this.consultarToolStripMenuItem7_Click_1);
            // 
            // loginToolStripMenuItem1
            // 
            this.loginToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.consultarToolStripMenuItem8});
            this.loginToolStripMenuItem1.ForeColor = System.Drawing.SystemColors.ButtonShadow;
            this.loginToolStripMenuItem1.Name = "loginToolStripMenuItem1";
            this.loginToolStripMenuItem1.Size = new System.Drawing.Size(46, 20);
            this.loginToolStripMenuItem1.Text = "login";
            // 
            // consultarToolStripMenuItem8
            // 
            this.consultarToolStripMenuItem8.Name = "consultarToolStripMenuItem8";
            this.consultarToolStripMenuItem8.Size = new System.Drawing.Size(125, 22);
            this.consultarToolStripMenuItem8.Text = "Consultar";
            this.consultarToolStripMenuItem8.Click += new System.EventHandler(this.consultarToolStripMenuItem8_Click_1);
            // 
            // todosOsDadosToolStripMenuItem1
            // 
            this.todosOsDadosToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.consultarToolStripMenuItem9});
            this.todosOsDadosToolStripMenuItem1.ForeColor = System.Drawing.SystemColors.ButtonShadow;
            this.todosOsDadosToolStripMenuItem1.Name = "todosOsDadosToolStripMenuItem1";
            this.todosOsDadosToolStripMenuItem1.Size = new System.Drawing.Size(102, 20);
            this.todosOsDadosToolStripMenuItem1.Text = "Todos os Dados";
            // 
            // consultarToolStripMenuItem9
            // 
            this.consultarToolStripMenuItem9.Name = "consultarToolStripMenuItem9";
            this.consultarToolStripMenuItem9.Size = new System.Drawing.Size(125, 22);
            this.consultarToolStripMenuItem9.Text = "Consultar";
            this.consultarToolStripMenuItem9.Click += new System.EventHandler(this.consultarToolStripMenuItem9_Click);
            // 
            // fluxoDeCaixaToolStripMenuItem
            // 
            this.fluxoDeCaixaToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.consultarToolStripMenuItem10});
            this.fluxoDeCaixaToolStripMenuItem.ForeColor = System.Drawing.SystemColors.ButtonShadow;
            this.fluxoDeCaixaToolStripMenuItem.Name = "fluxoDeCaixaToolStripMenuItem";
            this.fluxoDeCaixaToolStripMenuItem.Size = new System.Drawing.Size(94, 20);
            this.fluxoDeCaixaToolStripMenuItem.Text = "Fluxo de Caixa";
            // 
            // consultarToolStripMenuItem10
            // 
            this.consultarToolStripMenuItem10.Name = "consultarToolStripMenuItem10";
            this.consultarToolStripMenuItem10.Size = new System.Drawing.Size(125, 22);
            this.consultarToolStripMenuItem10.Text = "Consultar";
            this.consultarToolStripMenuItem10.Click += new System.EventHandler(this.consultarToolStripMenuItem10_Click);
            // 
            // gastosToolStripMenuItem
            // 
            this.gastosToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cadastrarToolStripMenuItem11,
            this.consultarToolStripMenuItem12});
            this.gastosToolStripMenuItem.ForeColor = System.Drawing.SystemColors.ButtonShadow;
            this.gastosToolStripMenuItem.Name = "gastosToolStripMenuItem";
            this.gastosToolStripMenuItem.Size = new System.Drawing.Size(54, 20);
            this.gastosToolStripMenuItem.Text = "Gastos";
            // 
            // cadastrarToolStripMenuItem11
            // 
            this.cadastrarToolStripMenuItem11.Name = "cadastrarToolStripMenuItem11";
            this.cadastrarToolStripMenuItem11.Size = new System.Drawing.Size(125, 22);
            this.cadastrarToolStripMenuItem11.Text = "Cadastrar";
            this.cadastrarToolStripMenuItem11.Click += new System.EventHandler(this.cadastrarToolStripMenuItem11_Click);
            // 
            // consultarToolStripMenuItem12
            // 
            this.consultarToolStripMenuItem12.Name = "consultarToolStripMenuItem12";
            this.consultarToolStripMenuItem12.Size = new System.Drawing.Size(125, 22);
            this.consultarToolStripMenuItem12.Text = "Consultar";
            this.consultarToolStripMenuItem12.Click += new System.EventHandler(this.consultarToolStripMenuItem12_Click);
            // 
            // cadastrarToolStripMenuItem10
            // 
            this.cadastrarToolStripMenuItem10.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.consultarToolStripMenuItem11});
            this.cadastrarToolStripMenuItem10.ForeColor = System.Drawing.SystemColors.ButtonShadow;
            this.cadastrarToolStripMenuItem10.Name = "cadastrarToolStripMenuItem10";
            this.cadastrarToolStripMenuItem10.Size = new System.Drawing.Size(61, 20);
            this.cadastrarToolStripMenuItem10.Text = "Estoque";
            // 
            // consultarToolStripMenuItem11
            // 
            this.consultarToolStripMenuItem11.Name = "consultarToolStripMenuItem11";
            this.consultarToolStripMenuItem11.Size = new System.Drawing.Size(125, 22);
            this.consultarToolStripMenuItem11.Text = "Consultar";
            this.consultarToolStripMenuItem11.Click += new System.EventHandler(this.consultarToolStripMenuItem11_Click);
            // 
            // emailsToolStripMenuItem
            // 
            this.emailsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.enviarToolStripMenuItem});
            this.emailsToolStripMenuItem.ForeColor = System.Drawing.SystemColors.ButtonShadow;
            this.emailsToolStripMenuItem.Name = "emailsToolStripMenuItem";
            this.emailsToolStripMenuItem.Size = new System.Drawing.Size(48, 20);
            this.emailsToolStripMenuItem.Text = "Email";
            // 
            // enviarToolStripMenuItem
            // 
            this.enviarToolStripMenuItem.Name = "enviarToolStripMenuItem";
            this.enviarToolStripMenuItem.Size = new System.Drawing.Size(106, 22);
            this.enviarToolStripMenuItem.Text = "Enviar";
            this.enviarToolStripMenuItem.Click += new System.EventHandler(this.enviarToolStripMenuItem_Click);
            // 
            // twitterToolStripMenuItem
            // 
            this.twitterToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.twittsGeraisToolStripMenuItem,
            this.meusTwittsToolStripMenuItem,
            this.twittarToolStripMenuItem});
            this.twitterToolStripMenuItem.ForeColor = System.Drawing.SystemColors.ButtonShadow;
            this.twitterToolStripMenuItem.Name = "twitterToolStripMenuItem";
            this.twitterToolStripMenuItem.Size = new System.Drawing.Size(55, 20);
            this.twitterToolStripMenuItem.Text = "Twitter";
            // 
            // twittsGeraisToolStripMenuItem
            // 
            this.twittsGeraisToolStripMenuItem.Name = "twittsGeraisToolStripMenuItem";
            this.twittsGeraisToolStripMenuItem.Size = new System.Drawing.Size(140, 22);
            this.twittsGeraisToolStripMenuItem.Text = "Twitts Gerais";
            this.twittsGeraisToolStripMenuItem.Click += new System.EventHandler(this.twittsGeraisToolStripMenuItem_Click);
            // 
            // meusTwittsToolStripMenuItem
            // 
            this.meusTwittsToolStripMenuItem.Name = "meusTwittsToolStripMenuItem";
            this.meusTwittsToolStripMenuItem.Size = new System.Drawing.Size(140, 22);
            this.meusTwittsToolStripMenuItem.Text = "Meus Twitts";
            this.meusTwittsToolStripMenuItem.Click += new System.EventHandler(this.meusTwittsToolStripMenuItem_Click);
            // 
            // twittarToolStripMenuItem
            // 
            this.twittarToolStripMenuItem.Name = "twittarToolStripMenuItem";
            this.twittarToolStripMenuItem.Size = new System.Drawing.Size(140, 22);
            this.twittarToolStripMenuItem.Text = "Twittar";
            this.twittarToolStripMenuItem.Click += new System.EventHandler(this.twittarToolStripMenuItem_Click);
            // 
            // imgFalar
            // 
            this.imgFalar.BackColor = System.Drawing.Color.Transparent;
            this.imgFalar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.imgFalar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.imgFalar.Image = global::Loja_de_roupas.Properties.Resources.voice_mic;
            this.imgFalar.Location = new System.Drawing.Point(985, 22);
            this.imgFalar.Name = "imgFalar";
            this.imgFalar.Size = new System.Drawing.Size(40, 30);
            this.imgFalar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.imgFalar.TabIndex = 13;
            this.imgFalar.TabStop = false;
            this.imgFalar.Click += new System.EventHandler(this.imgFalar_Click);
            // 
            // imgParar
            // 
            this.imgParar.BackColor = System.Drawing.Color.Transparent;
            this.imgParar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.imgParar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.imgParar.Image = global::Loja_de_roupas.Properties.Resources.stop_button;
            this.imgParar.Location = new System.Drawing.Point(1031, 22);
            this.imgParar.Name = "imgParar";
            this.imgParar.Size = new System.Drawing.Size(40, 30);
            this.imgParar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.imgParar.TabIndex = 14;
            this.imgParar.TabStop = false;
            this.imgParar.Click += new System.EventHandler(this.imgParar_Click);
            // 
            // lblProcess
            // 
            this.lblProcess.AutoSize = true;
            this.lblProcess.BackColor = System.Drawing.Color.Transparent;
            this.lblProcess.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblProcess.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.lblProcess.Location = new System.Drawing.Point(456, 384);
            this.lblProcess.Name = "lblProcess";
            this.lblProcess.Size = new System.Drawing.Size(162, 25);
            this.lblProcess.TabIndex = 15;
            this.lblProcess.Text = "Processando ...";
            // 
            // imgRec
            // 
            this.imgRec.BackColor = System.Drawing.Color.Transparent;
            this.imgRec.Image = global::Loja_de_roupas.Properties.Resources.rec;
            this.imgRec.Location = new System.Drawing.Point(1010, 58);
            this.imgRec.Name = "imgRec";
            this.imgRec.Size = new System.Drawing.Size(39, 36);
            this.imgRec.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.imgRec.TabIndex = 16;
            this.imgRec.TabStop = false;
            this.imgRec.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // frmMenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::Loja_de_roupas.Properties.Resources.logo;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1083, 447);
            this.Controls.Add(this.imgRec);
            this.Controls.Add(this.lblProcess);
            this.Controls.Add(this.imgParar);
            this.Controls.Add(this.imgFalar);
            this.Controls.Add(this.lblFalar);
            this.Controls.Add(this.menuStrip2);
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "frmMenu";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Menu";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Menu_FormClosed);
            this.Load += new System.EventHandler(this.Menu_Load);
            this.menuStrip2.ResumeLayout(false);
            this.menuStrip2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imgFalar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgParar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgRec)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label lblFalar;
        private System.Windows.Forms.MenuStrip menuStrip2;
        private System.Windows.Forms.ToolStripMenuItem clienteToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem cadastrarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem consultarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem funcionariosToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem cadastrarToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem consultarToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem produtoToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem cadastrarToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem consultarToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem fornecedorToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem cadastrarToolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem consultarToolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem pedidoToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem cadastrarToolStripMenuItem4;
        private System.Windows.Forms.ToolStripMenuItem consultarToolStripMenuItem4;
        private System.Windows.Forms.ToolStripMenuItem cartegoriaToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem cadastrarToolStripMenuItem5;
        private System.Windows.Forms.ToolStripMenuItem consultarToolStripMenuItem5;
        private System.Windows.Forms.ToolStripMenuItem folhaDePagaentoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cadastrarToolStripMenuItem6;
        private System.Windows.Forms.ToolStripMenuItem consultarToolStripMenuItem6;
        private System.Windows.Forms.ToolStripMenuItem comprasToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem cadastrarToolStripMenuItem7;
        private System.Windows.Forms.ToolStripMenuItem consultarToolStripMenuItem7;
        private System.Windows.Forms.ToolStripMenuItem loginToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem consultarToolStripMenuItem8;
        private System.Windows.Forms.ToolStripMenuItem todosOsDadosToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem consultarToolStripMenuItem9;
        private System.Windows.Forms.ToolStripMenuItem fluxoDeCaixaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem consultarToolStripMenuItem10;
        private System.Windows.Forms.ToolStripMenuItem gastosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cadastrarToolStripMenuItem11;
        private System.Windows.Forms.ToolStripMenuItem cadastrarToolStripMenuItem10;
        private System.Windows.Forms.ToolStripMenuItem consultarToolStripMenuItem11;
        private System.Windows.Forms.ToolStripMenuItem consultarToolStripMenuItem12;
        private System.Windows.Forms.ToolStripMenuItem emailsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem enviarToolStripMenuItem;
        private System.Windows.Forms.PictureBox imgFalar;
        private System.Windows.Forms.PictureBox imgParar;
        private System.Windows.Forms.Label lblProcess;
        private System.Windows.Forms.PictureBox imgRec;
        private System.Windows.Forms.ToolStripMenuItem twitterToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem twittsGeraisToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem meusTwittsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem twittarToolStripMenuItem;
    }
}