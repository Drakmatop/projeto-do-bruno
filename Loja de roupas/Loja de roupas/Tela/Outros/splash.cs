﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Loja_de_roupas.DB.Telas.Outros
{
    public partial class splash : Form
    {
        public splash()
        {
            InitializeComponent();
            
            

            Task.Factory.StartNew(() =>
            {
                
                
                // Espera 2 segundos para iniciar o sistema
                System.Threading.Thread.Sleep(0000);

                Invoke(new Action(() =>
                {
                    while (progressBar1.Value < 100)
                    {
                        System.Threading.Thread.Sleep(0016);

                        progressBar1.Value = progressBar1.Value + 1;

                     }
                    // Abre a tela Inicial
                    frmLogin frm = new frmLogin();
                    frm.Show();
                    Hide();
                }));
            });



        }

        private void label1_Click(object sender, EventArgs e)
        {

        }
    }
}
