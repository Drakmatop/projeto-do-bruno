﻿namespace Loja_de_roupas.Tela.Outros
{
    partial class frmMeusTwitts
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMeusTwitts));
            this.btnCarregar = new System.Windows.Forms.Button();
            this.lstTweets = new System.Windows.Forms.ListBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // btnCarregar
            // 
            this.btnCarregar.BackColor = System.Drawing.Color.Transparent;
            this.btnCarregar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCarregar.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnCarregar.Location = new System.Drawing.Point(95, 336);
            this.btnCarregar.Name = "btnCarregar";
            this.btnCarregar.Size = new System.Drawing.Size(553, 30);
            this.btnCarregar.TabIndex = 7;
            this.btnCarregar.Text = "Carregar Tweets";
            this.btnCarregar.UseVisualStyleBackColor = false;
            this.btnCarregar.Click += new System.EventHandler(this.btnCarregar_Click);
            // 
            // lstTweets
            // 
            this.lstTweets.Font = new System.Drawing.Font("Arial", 11F);
            this.lstTweets.ForeColor = System.Drawing.Color.White;
            this.lstTweets.FormattingEnabled = true;
            this.lstTweets.HorizontalScrollbar = true;
            this.lstTweets.ItemHeight = 17;
            this.lstTweets.Location = new System.Drawing.Point(95, 189);
            this.lstTweets.Name = "lstTweets";
            this.lstTweets.Size = new System.Drawing.Size(553, 140);
            this.lstTweets.TabIndex = 6;
            this.lstTweets.SelectedIndexChanged += new System.EventHandler(this.lstTweets_SelectedIndexChanged);
            this.lstTweets.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.lstTweets_KeyPress);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.Image = global::Loja_de_roupas.Properties.Resources.lottus_beatifull;
            this.pictureBox1.Location = new System.Drawing.Point(81, 32);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(46, 34);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 40;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // frmMeusTwitts
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::Loja_de_roupas.Properties.Resources.Lotus_Store_TELA;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(736, 404);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.btnCarregar);
            this.Controls.Add(this.lstTweets);
            this.DoubleBuffered = true;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmMeusTwitts";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Meus Twitts";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnCarregar;
        private System.Windows.Forms.ListBox lstTweets;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}