﻿using nsf._2018.diferenciais.RedesSociais;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Loja_de_roupas.Tela.Outros
{
    public partial class frmMeusTwitts : Form
    {
        public frmMeusTwitts()
        {
            InitializeComponent();
        }
        private void btnCarregar_Click(object sender, EventArgs e)
        {
            try
            {
                Twitter twitter = new Twitter();

                List<TweetResponse> tweets = twitter.Meustwitts();

                foreach (TweetResponse item in tweets)
                {
                    lstTweets.Items.Add($"{item.Criacao.ToString("dd/MM HH:mm")}   {item.Usuario}");
                    lstTweets.Items.Add($"{item.Mensagem}");
                    lstTweets.Items.Add("");
                }
                lstTweets.ForeColor = Color.Black;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocorreu o erro : " + ex.Message);
            }
        }

        private void lstTweets_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            frmMenu tela = new frmMenu();
            tela.Show();
            this.Hide();
        }

        private void lstTweets_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((Keys)e.KeyChar == Keys.Enter)
            {
                btnCarregar_Click(null, null);
            }
        }
    }
}
