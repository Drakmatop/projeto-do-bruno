﻿using Loja_de_roupas.DB.Funcionario;
using Nsf._2018.Modulo2.DB.Filosofia.Plugin;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Loja_de_roupas
{
    public partial class frmLogin : Form
    {
        public frmLogin()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
            FuncionarioBusiness business = new FuncionarioBusiness();
            FuncionarioDTO Funcionario = business.Logar(txtUsuario.Text, txtSenha.Text);
            if (Funcionario != null)
            {
                UserSess.Usuariologado = Funcionario;
                frmMenu frm = new frmMenu();
                frm.Show();
                Hide();
            }
            else
            {
                MessageBox.Show("Credenciais inválidas.");
            }
            }
            catch (Exception ex)
            {

                MessageBox.Show("Ocorreu um erro" + ex.Message, "Lottus Store");
            }




            


        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            try
            {
            frmMenu tela = new frmMenu();
            tela.Show();
            this.Hide();
            }
            catch (Exception ex)
            {

                MessageBox.Show("Ocorreu um erro" + ex.Message, "Lottus Store");
            }

        }

       

        private void label4_Click(object sender, EventArgs e)
        {
            if (txtUsuario.Text == string.Empty)
            {
                MessageBox.Show("Preencha o campo de usuário para continuar");
            }
            else
            {
                FuncionarioBusiness business = new FuncionarioBusiness();
                FuncionarioDTO login = business.ConsultarLoginPeloUsuario(txtUsuario.Text);
                if (login == null)
                {
                    MessageBox.Show("Este usuário não existe, digite o usuário corretamente");
                }
                else
                {

                    Email email = new Email();
                    email.Mensagem = "Olá " + login.Nome + @", foi solicitado um envio de e-mail contendo a sua senha, caso não seja você a ter solicitado, relate o ocorrido ao seu supervisor.
a sua senha é : " + login.Senha;
                    email.Para = login.Email;
                    email.Assunto = "Relembre sua senha";
                    email.Enviar();
                    MessageBox.Show("Uma mensagem foi enviada no seu e-mail contendo sua senha, verifique-o e tente fazer login novamente");
                }
            }


        }

        private void frmLogin_Load(object sender, EventArgs e)
        {

        }

        private void txtSenha_TextChanged(object sender, EventArgs e)
        {
        }

        private void txtSenha_Enter(object sender, EventArgs e)
        {

        }

        private void button1_Enter(object sender, EventArgs e)
        {


        }

        private void txtSenha_DragEnter(object sender, DragEventArgs e)
        {
            button1_Click(null, null);
        }

        private void txtSenha_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((Keys)e.KeyChar == Keys.Enter )
            {
                button1_Click(null, null);
            }
        }
    }
}
