﻿using Loja_de_roupas.DB.Funcionario;
using nsf._2018.diferenciais.APIs.Plugin;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Loja_de_roupas
{
    public partial class frmMenu : Form
    {
        public frmMenu()
        {
            InitializeComponent();
            ValidarLogin();
            lblProcess.Visible = false;
            imgRec.Visible = false;
        }
        
        private void Menu_Load(object sender, EventArgs e)
        {

        }
        

        public void ValidarLogin()
        {
            try
            {
                if (UserSess.Usuariologado.PermissaoAdm == false)
                {
                    cadastrarToolStripMenuItem.Enabled = false;
                    cadastrarToolStripMenuItem2.Enabled = false;
                    cadastrarToolStripMenuItem3.Enabled = false;
                    cadastrarToolStripMenuItem4.Enabled = false;
                    cadastrarToolStripMenuItem5.Enabled = false;
                    cadastrarToolStripMenuItem6.Enabled = false;
                }

            }
            catch (Exception)
            {

                MessageBox.Show("Ocorreu um erro, Logue novamente", "Lottus Store");
                Application.Exit();
            }
        }
        

        private void Menu_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }
        
        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void cadastrarToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            frmCadastrarCliente tela = new frmCadastrarCliente();
            tela.Show();
            this.Hide();
        }

        private void consultarToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            Consultar_Cliente tela = new Consultar_Cliente();
            tela.Show();
            this.Hide();
        }

        private void cadastrarToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Cadastrar_funcionario tela = new Cadastrar_funcionario();
            tela.Show();
            this.Hide();
        }

        private void consultarToolStripMenuItem1_Click_1(object sender, EventArgs e)
        {
            Consultar_Funcionario tela = new Consultar_Funcionario();
            tela.Show();
            this.Hide();
        }

        private void cadastrarToolStripMenuItem2_Click_1(object sender, EventArgs e)
        {
            Cadastrar_produto tela = new Cadastrar_produto();
            tela.Show();
            this.Hide();
        }

        private void consultarToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            Consultar_Produto tela = new Consultar_Produto();
            tela.Show();
            this.Hide();
        }

        private void cadastrarToolStripMenuItem3_Click_1(object sender, EventArgs e)
        {
            Cadastro_de_fornecedor tela = new Cadastro_de_fornecedor();
            tela.Show();
            this.Hide();
        }

        private void consultarToolStripMenuItem3_Click_1(object sender, EventArgs e)
        {
            Consultar_Fornecedor tela = new Consultar_Fornecedor();
            tela.Show();
            this.Hide();
        }

        private void cadastrarToolStripMenuItem4_Click_1(object sender, EventArgs e)
        {
            Cadastrar_Pedido tela = new Cadastrar_Pedido();
            tela.Show();
            this.Hide();
        }

        private void consultarToolStripMenuItem4_Click_1(object sender, EventArgs e)
        {

            Consultar_Pedido tela = new Consultar_Pedido();
            tela.Show();
            this.Hide();
        }

        private void cadastrarToolStripMenuItem5_Click_1(object sender, EventArgs e)
        {
            Tela.Cadastrar.CadastrarCategoria tela = new Tela.Cadastrar.CadastrarCategoria();
            tela.Show();
            this.Hide();
        }

        private void consultarToolStripMenuItem5_Click_1(object sender, EventArgs e)
        {
            Consultar_Cartegoria tela = new Consultar_Cartegoria();
            tela.Show();
            this.Hide();
        }

        private void cadastrarToolStripMenuItem6_Click_1(object sender, EventArgs e)
        {
            DB.Telas.Outros.frmFolhadePagamento tela = new DB.Telas.Outros.frmFolhadePagamento();
            tela.Show();
            this.Hide();
        }

        private void consultarToolStripMenuItem6_Click_1(object sender, EventArgs e)
        {
            DB.Telas.Consultar.ConsultarFolhadePagamento tela = new DB.Telas.Consultar.ConsultarFolhadePagamento();
            tela.Show();
            this.Hide();
        }

        private void cadastrarToolStripMenuItem7_Click(object sender, EventArgs e)
        {
            DB.Telas.Cadastrar.CadastrarCompra tela = new DB.Telas.Cadastrar.CadastrarCompra();
            tela.Show();
            this.Hide();
        }

        private void consultarToolStripMenuItem7_Click_1(object sender, EventArgs e)
        {
            DB.Telas.Consultar.ConsultarCompra tela = new DB.Telas.Consultar.ConsultarCompra();
            tela.Show();
        }

        private void consultarToolStripMenuItem8_Click_1(object sender, EventArgs e)
        {
            Consultar_Login tela = new Consultar_Login();
            tela.Show();
            this.Hide();
        }

        private void consultarToolStripMenuItem9_Click(object sender, EventArgs e)
        {
            todosdados tela = new todosdados();
            tela.Show();
            this.Hide();
        }

        private void consultarToolStripMenuItem10_Click(object sender, EventArgs e)
        {
            DB.Telas.Consultar.ConsultarFluxodeCaixa tela = new DB.Telas.Consultar.ConsultarFluxodeCaixa();
            tela.Show();
            this.Hide();
        }

        private void consultarToolStripMenuItem11_Click(object sender, EventArgs e)
        {
            Tela.Consultar.ConsultarEstoque tela = new Tela.Consultar.ConsultarEstoque();
            tela.Show();
            this.Hide();
        }

        private void cadastrarToolStripMenuItem11_Click(object sender, EventArgs e)
        {
            DB.Telas.Cadastrar.CadastrarGasto tela = new DB.Telas.Cadastrar.CadastrarGasto();
            tela.Show();
            this.Hide();
        }

        private void consultarToolStripMenuItem12_Click(object sender, EventArgs e)
        {
            Tela.Consultar.ConsultarGastos tela = new Tela.Consultar.ConsultarGastos();
            tela.Show();
            this.Hide();
        }

        private void enviarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Tela.Outros.EnviarEmail tela = new Tela.Outros.EnviarEmail();
            tela.Show();
            this.Hide();
        }
        IbmVoiceApi ibmApi = new IbmVoiceApi();
        private void imgFalar_Click(object sender, EventArgs e)
        {
            ibmApi.IniciarOuvir();
            imgRec.Visible = true;

        }

        private void imgParar_Click(object sender, EventArgs e)
        {
            try
            {
                lblProcess.Visible = true;
                imgRec.Visible = false;
                string texto = ibmApi.PararOuvir();
                ibmApi.Verificar(texto);
                lblProcess.Visible = false;
                this.Hide();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Lottus Store");
                lblProcess.Visible = false;
            }
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void twittarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            nsf._2018.diferenciais.frmTwitter tela = new nsf._2018.diferenciais.frmTwitter();
            tela.Show();
            this.Hide();
        }

        private void meusTwittsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Tela.Outros.frmMeusTwitts tela = new Tela.Outros.frmMeusTwitts();
            tela.Show();
            this.Hide();
        }

        private void twittsGeraisToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Tela.Outros.Carregar_Tweets tela = new Tela.Outros. Carregar_Tweets();
            tela.Show();
            this.Hide();
        }
    }
}
