﻿using Nsf._2018.Modulo2.DB.Filosofia.Plugin;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Loja_de_roupas.Tela.Outros
{
    public partial class EnviarEmail : Form
    {
        public EnviarEmail()
        {
            InitializeComponent();
        }
        Email email = new Email();

        private void btnEnviar_Click(object sender, EventArgs e)
        {
            try
            {
                
                email.Mensagem = txtMensagem.Text.Trim();
                email.Assunto = txtAssunto.Text.Trim();
                email.Para = txtDestinatario.Text.Trim();

                
                email.Enviar();
                
                MessageBox.Show("Mensagem enviada com sucesso", "Lottus Store");

            }
            catch (Exception Ex)
            {
                MessageBox.Show("Ocorreu o erro : " + Ex.Message);
            }
            
        }

        private void EnviarEmail_Load(object sender, EventArgs e)
        {

        }

        private void label9_Click(object sender, EventArgs e)
        {

        }

        private void txtMensagem_TextChanged(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void txtAssunto_TextChanged(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void txtDestinatario_TextChanged(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void lblAnexo_Click(object sender, EventArgs e)
        {
            OpenFileDialog janela = new OpenFileDialog();
            janela.ShowDialog();

            lblAnexo.Text = janela.FileName;

            email.AdicionarAnexo(janela.FileName);
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            frmMenu tela = new frmMenu();
            tela.Show();
            this.Hide();
            
        }

        private void txtMensagem_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((Keys)e.KeyChar == Keys.Enter)
            {
                btnEnviar_Click(null, null);
            }
        }
    }
}
