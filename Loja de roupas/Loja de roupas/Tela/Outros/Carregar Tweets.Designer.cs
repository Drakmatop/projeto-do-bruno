﻿namespace Loja_de_roupas.Tela.Outros
{
    partial class Carregar_Tweets
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Carregar_Tweets));
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.btnCarregar = new System.Windows.Forms.Button();
            this.lstTweets = new System.Windows.Forms.ListBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.Image = global::Loja_de_roupas.Properties.Resources.lottus_beatifull;
            this.pictureBox1.Location = new System.Drawing.Point(83, 47);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(5);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(61, 42);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 43;
            this.pictureBox1.TabStop = false;
            // 
            // btnCarregar
            // 
            this.btnCarregar.BackColor = System.Drawing.Color.Transparent;
            this.btnCarregar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCarregar.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnCarregar.Location = new System.Drawing.Point(137, 352);
            this.btnCarregar.Margin = new System.Windows.Forms.Padding(4);
            this.btnCarregar.Name = "btnCarregar";
            this.btnCarregar.Size = new System.Drawing.Size(583, 37);
            this.btnCarregar.TabIndex = 42;
            this.btnCarregar.Text = "Carregar Tweets";
            this.btnCarregar.UseVisualStyleBackColor = false;
            // 
            // lstTweets
            // 
            this.lstTweets.Font = new System.Drawing.Font("Arial", 11F);
            this.lstTweets.ForeColor = System.Drawing.Color.White;
            this.lstTweets.FormattingEnabled = true;
            this.lstTweets.HorizontalScrollbar = true;
            this.lstTweets.ItemHeight = 17;
            this.lstTweets.Location = new System.Drawing.Point(137, 187);
            this.lstTweets.Margin = new System.Windows.Forms.Padding(4);
            this.lstTweets.Name = "lstTweets";
            this.lstTweets.Size = new System.Drawing.Size(582, 157);
            this.lstTweets.TabIndex = 41;
            this.lstTweets.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.lstTweets_KeyPress);
            // 
            // Carregar_Tweets
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::Loja_de_roupas.Properties.Resources.Lotus_Store_TELA;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(854, 449);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.btnCarregar);
            this.Controls.Add(this.lstTweets);
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "Carregar_Tweets";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Carregar_Tweets";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button btnCarregar;
        private System.Windows.Forms.ListBox lstTweets;
    }
}