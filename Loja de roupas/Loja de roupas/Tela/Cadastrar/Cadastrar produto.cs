﻿using Loja_de_roupas.DB.Base.Categoria;
using Loja_de_roupas.DB.Estoque;
using Loja_de_roupas.DB.Fornecedor;
using Loja_de_roupas.DB.Produto;
using nsf._2018.diferenciais.Dispositivos;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Loja_de_roupas
{
    public partial class Cadastrar_produto : Form
    {
        BindingList<ProdutoDTO> produtosCarrinho = new BindingList<ProdutoDTO>();
        Validação v = new Validação();
        public Cadastrar_produto()
        {
            InitializeComponent();
            CarregarCombos();
            imgBarCode.Visible = false;
        }

        void CarregarCombos()
        {
            
            CategoriaBusiness catebus = new CategoriaBusiness();
            List<CategoriaDTO> cateList = catebus.Listar();

            cboCategoria.DisplayMember = nameof(CategoriaDTO.Nome);
            cboCategoria.ValueMember = nameof(CategoriaDTO.Id);
            cboCategoria.DataSource = cateList;


            string nome = "";
            FornecedorBusiness fornbus = new FornecedorBusiness();
            List<FornecedorDTO> fornList = fornbus.Consultar(nome);

            cboFornecedor.DisplayMember = nameof(FornecedorDTO.Nome);
            cboFornecedor.ValueMember = nameof(FornecedorDTO.Id);
            cboFornecedor.DataSource = fornList;

        }

        private void button1_Click(object sender, EventArgs e)
        {

        }

        private void Cadastrar_produto_Load(object sender, EventArgs e)
        {

        }


        private void bnt1_Click_2(object sender, EventArgs e)
        {
            try
            {
                if (imgBarCode.Visible == false)
                {
                    MessageBox.Show("Gere o código de barras antes de salvar o produto");
                }
                else
                {
                    CategoriaDTO cat = cboCategoria.SelectedItem as CategoriaDTO;
                    FornecedorDTO forn = cboFornecedor.SelectedItem as FornecedorDTO;
                    EstoqueDTO estoque = new EstoqueDTO();
                    EstoqueBusiness estoquebusiness = new EstoqueBusiness();


                    ProdutoDTO dto = new ProdutoDTO();
                    dto.Nome = txtProduto.Text.Trim();
                    dto.Cor = txtCor.Text.Trim();
                    dto.PrecoCompra = nudPrecoCompra.Value;
                    dto.PrecoVenda = nudPrecoVenda.Value;
                    dto.Tamanho = txtTamanho.Text.Trim();
                    dto.CategoriaId = cat.Id;
                    dto.FornecedorId = forn.Id;
                    dto.CodigodeBarras = txtGerar.Text;

                    ProdutoBusiness_ business = new ProdutoBusiness_();
                    int idproduto = business.Salvar(dto);
                    estoque.IdProduto = idproduto;
                    estoque.Quantidade = 0;
                    estoquebusiness.Salvar(estoque);

                    MessageBox.Show("Cadastro efetuado com sucesso", "Lottus Store");
                }
            }
            catch (Exception ex)
            {

                MessageBox.Show("Ocorreu o erro : " + ex.Message, "Lottus Store");
            }

        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            try
            {
            frmMenu tela = new frmMenu();
            tela.Show();
            this.Hide();
            }
            catch (Exception ex)
            {

                MessageBox.Show("Ocorreu o erro : " + ex.Message, "Lottus Store"); 
            }

        }

        private void txtProduto_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {
            v.soletras(e);
            }
            catch (Exception ex)
            {

                MessageBox.Show("Ocorreu o erro : " + ex.Message, "Lottus Store");
            }
 
        }

        private void cboFornecedor_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void label7_Click(object sender, EventArgs e)
        {

        }

        private void cboCategoria_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void label6_Click(object sender, EventArgs e)
        {

        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void nudPrecoVenda_ValueChanged(object sender, EventArgs e)
        {

        }

        private void nudPrecoCompra_ValueChanged(object sender, EventArgs e)
        {

        }

        private void txtCor_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtTamanho_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtProduto_TextChanged(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void txtCodBarras_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((Keys)e.KeyChar == Keys.Enter)
            {
                button1_Click(null, null);
            }


            v.sonumeros(e);
        }

        private void btnGerar_Click(object sender, EventArgs e)
        {
            if (txtGerar.Text == string.Empty)
            {
                MessageBox.Show("O código de barras é obrigatório");
            }
            else if (txtGerar.Text.Length < 12)
            {
                MessageBox.Show("O código de barras precisa ter 12 números");
            }
            else
            {
                SaveFileDialog janela = new SaveFileDialog();
                janela.Filter = "Imagens (*.png)|*.png";

                DialogResult result = janela.ShowDialog();

                if (result == DialogResult.OK)
                {
                    BarCodeService barcode = new BarCodeService();
                    Image imagem = barcode.SalvarCodigoBarras(txtGerar.Text, janela.FileName);

                    imgBarCode.Image = imagem;
                }
                imgBarCode.Visible = true;
            }
        }

        private void txtGerar_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
