﻿using Loja_de_roupas.DB.Compras;
using Loja_de_roupas.DB.Estoque;
using Loja_de_roupas.DB.Produto;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Loja_de_roupas.DB.Telas.Cadastrar
{
    public partial class CadastrarCompra : Form
    {
        BindingList<ProdutoDTO> produtosCarrinho = new BindingList<ProdutoDTO>();
        public CadastrarCompra()
        {
            InitializeComponent();
            CarregarCombo();
        }
        private void CarregarCombo()
        {
            ProdutoBusiness_ business = new ProdutoBusiness_();
            List<ProdutoDTO> lista = business.Consultar(string.Empty);
            cboProduto.ValueMember = nameof(ProdutoDTO.Id);
            cboProduto.DisplayMember = nameof(ProdutoDTO.Nome);
            cboProduto.DataSource = lista;
        }
        decimal valordacompra = 0;
        int quantidade = 0;

        private void button1_Click(object sender, EventArgs e)
        {
            CompraDTO dto = new CompraDTO();
            ProdutoDTO produto = cboProduto.SelectedItem as ProdutoDTO;
            CompraItemDTO compraitem = new CompraItemDTO();

            dto.DataCompra = DateTime.Now;
            dto.QtdItens = quantidade;
            dto.ValorCompra = Convert.ToDecimal(lblValordaCompra.Text);
            compraitem.IdProduto = produto.Id;

            CompraBusiness business = new CompraBusiness();
            int idcompra = business.Salvar(dto, produtosCarrinho.ToList());
            button3_Click(null, null);
            txtQuantidade.Clear();
            quantidade = 0;
            EstoqueBusiness businessestoque = new EstoqueBusiness();
            List<VwConsultarCompra> lista = business.ConsultarPorId(idcompra);
            List<EstoqueDTO> estoque = businessestoque.Listar();

            foreach (VwConsultarCompra item in lista)
            {
                foreach (EstoqueDTO item2 in estoque)
                {
                    if (item.IdProduto == item2.IdProduto)
                    {
                        item2.Quantidade = item2.Quantidade + item.QtdUnidades;
                    }
                }
            }


            foreach (EstoqueDTO item in estoque)
            {
                EstoqueDTO estoquedto = new EstoqueDTO();

                estoquedto.IdProduto = item.IdProduto;
                estoquedto.Quantidade = item.Quantidade;

                businessestoque.Alterar(estoquedto);
            }

            MessageBox.Show("Compra efetuada com sucesso", "Lottus Store");
        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                
                ProdutoDTO dto = cboProduto.SelectedItem as ProdutoDTO;
                int qtd = Convert.ToInt32(txtQuantidade.Text);

                for (int i = 0; i < qtd; i++)
                {
                    produtosCarrinho.Add(dto);
                    dataGridView1.AutoGenerateColumns = false;
                    dataGridView1.DataSource = produtosCarrinho;
                    valordacompra = valordacompra + dto.PrecoCompra;
                    quantidade = quantidade + 1;
                }
                lblValordaCompra.Text = valordacompra.ToString();
                MessageBox.Show("Item adicionado", "Lottus Store");
            }
            catch (Exception ex)
            {

                MessageBox.Show("Ocorreu o erro : " + ex.Message, "lottus store");
            }
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 1)
            {

            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            produtosCarrinho = new BindingList<ProdutoDTO>();
            dataGridView1.DataSource = produtosCarrinho;
            valordacompra = 0;
            lblValordaCompra.Text = valordacompra.ToString();
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            frmMenu tela = new frmMenu();
            tela.Show();
            this.Hide();
        }

        private void txtQuantidade_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((Keys)e.KeyChar == Keys.Enter)
            {
                button2_Click(null, null);
            }
        }
    }
}
