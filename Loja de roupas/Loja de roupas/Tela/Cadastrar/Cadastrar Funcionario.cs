﻿using Loja_de_roupas;
using Loja_de_roupas.DB.Funcionario;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Loja_de_roupas
{
    public partial class Cadastrar_funcionario : Form
    {
        Validação v = new Validação();

        public Cadastrar_funcionario()
        {
            InitializeComponent();
            rdnVendedor.Checked = true;
        }


        private void lbl2_Click(object sender, EventArgs e)
        {

        }

        private void Cadastrar_funcionario_Load(object sender, EventArgs e)
        {

        }

        private void bnt1_Click(object sender, EventArgs e)
        {
            try
            {
                bool a = rndADM.Checked;
                bool b = rdnVendedor.Checked;


                FuncionarioDTO dto = new FuncionarioDTO();
                dto.Nome = txtNome.Text.Trim();
                dto.Sobrenome = txtSobrenome.Text.Trim();
                dto.Telefone = txtTelefone.Text.Trim();
                dto.Cep = txtCep.Text.Trim();
                dto.DataNascimento = dtpData.Value;
                dto.Login = txtUsuario.Text.Trim();
                dto.Senha = txtSenha.Text.Trim();
                dto.PermissaoAdm = a;
                dto.PermissaoVendedor = b;
                dto.SalarioBruto = Convert.ToDecimal(txtSalarioB.Text);
                dto.Email = txtEmail.Text;
                FuncionarioBusiness business = new FuncionarioBusiness();
                business.Salvar(dto);

                MessageBox.Show("Cadastro efetuado com sucesso", "Lottus Store");
            }
            catch (Exception ex)
            {

                MessageBox.Show("Ocorreu o erro : " + ex.Message, "Lottus Store");
            }




        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            try
            {
                frmMenu tela = new frmMenu();
                tela.Show();
                this.Hide();
            }
            catch (Exception ex)
            {

                MessageBox.Show("Ocorreu o erro : " + ex.Message, "Lottus Store");
            }

        }

        private void txtNome_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {

            }
            catch (Exception ex)
            {

                MessageBox.Show("Ocorreu o erro : " + ex.Message, "Lottus Sotre");
            }

        }

        private void txtSobrenome_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtSobrenome_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {
                Validação v = new Validação();
                v.soletras(e);

                
            }
            catch (Exception ex)
            {

                MessageBox.Show("Ocorreu o erro : " + ex.Message, "Lottus Store");
            }

        }

        private void txtUsuario_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtUsuario_KeyPress(object sender, KeyPressEventArgs e)
        {

        }

        private void txtSenha_KeyPress(object sender, KeyPressEventArgs e)
        {

        }

        private void txtSalarioB_KeyPress(object sender, KeyPressEventArgs e)
        {
            Validação v = new Validação();
            v.sonumeros(e);
        }

        private void txtEmail_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((Keys)e.KeyChar == Keys.Enter)
            {
                bnt1_Click(null, null);
            }
        }
    }
}
