﻿using Loja_de_roupas.DB.Gastos;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Loja_de_roupas.DB.Telas.Cadastrar
{
    public partial class CadastrarGasto : Form
    {
        public CadastrarGasto()
        {
            InitializeComponent();
        }

        private void btnCadastrar_Click(object sender, EventArgs e)
        {
            try
            {
                GastoDTO dto = new GastoDTO();

                dto.Gasto = txtGasto.Text;
                dto.Tipo = cboTipo.SelectedItem.ToString();
                dto.Pagamento = dtpPagamento.Value;
                dto.Valor = nudValor.Value;

                GastoBusiness business = new GastoBusiness();
                business.Salvar(dto);
                MessageBox.Show("Gasto salvo com sucesso", "Lottus Store");
                txtGasto.Clear();
                nudValor.Value = 0.00m;
                dtpPagamento.Value = DateTime.Now;
            }
            catch (Exception ex)
            {

                MessageBox.Show("Ocorreu o erro : " + ex.Message, "Lottus Store"); 
            }
            

        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {

            frmMenu tela = new frmMenu();
            tela.Show();
            this.Hide();
        }

        private void cboTipo_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void nudValor_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((Keys)e.KeyChar == Keys.Enter)
            {
                btnCadastrar_Click(null, null);
            }
        }
    }
}
