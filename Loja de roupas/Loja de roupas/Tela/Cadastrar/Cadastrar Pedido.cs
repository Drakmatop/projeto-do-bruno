﻿using Loja_de_roupas.DB.Base.Clientes;
using Loja_de_roupas.DB.Estoque;
using Loja_de_roupas.DB.Funcionario;
using Loja_de_roupas.DB.Pedido;
using Loja_de_roupas.DB.Produto;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Loja_de_roupas
{
    public partial class Cadastrar_Pedido : Form
    {
        BindingList<ProdutoDTO> produtosCarrinho = new BindingList<ProdutoDTO>();
        Validação v = new Validação();

        public Cadastrar_Pedido()
        {
            InitializeComponent();
            CarregarCombos();
        }
        decimal valordavenda = 0;
        void CarregarCombos()
        {
            ProdutoBusiness_ business = new ProdutoBusiness_();
            List<ProdutoDTO> lista = business.Consultar(string.Empty);
            cboProduto.ValueMember = nameof(ProdutoDTO.Id);
            cboProduto.DisplayMember = nameof(ProdutoDTO.Nome);
            cboProduto.DataSource = lista;

            ClienteBusiness clientebus = new ClienteBusiness();
            List<ClienteDTO> clielista = clientebus.Listar();
            cboCliente.ValueMember = nameof(ClienteDTO.Id);
            cboCliente.DisplayMember = nameof(ClienteDTO.Nome);
            cboCliente.DataSource = clielista;


        }
        int quantidade = 0;
        int i = 10;
        private void btnAdicionar_Click(object sender, EventArgs e)
        {
            try
            {
                EstoqueBusiness estoquebussiness = new EstoqueBusiness();

                List<EstoqueDTO> estoquedto = estoquebussiness.Listar();


                ProdutoDTO dto = cboProduto.SelectedItem as ProdutoDTO;
                int qtd = Convert.ToInt32(nudQuantidade.Value);
                
                
                foreach (EstoqueDTO item in estoquedto)
                {
                    if (dto.Id == item.IdProduto)
                    {
                        if (i == 0)
                        {
                            if (quantidade < qtd)
                            {
                                MessageBox.Show("Quantidade de " + dto.Nome + " Não é suficiente para atender a esta venda, atualmente temos em estoque " + quantidade + " " + dto.Nome);
                            }
                            else
                            {

                                for (i = 0; i < qtd; i++)

                                {
                                    produtosCarrinho.Add(dto);
                                    valordavenda = valordavenda + dto.PrecoVenda;
                                    lblValordaVenda.Text = valordavenda.ToString();

                                }

                                quantidade = quantidade - qtd;

                                dgvProduto.AutoGenerateColumns = false;
                                dgvProduto.DataSource = produtosCarrinho;
                                i = 0;
                            }
                        }
                        else
                        {
                            if (item.Quantidade < qtd)
                            {
                                MessageBox.Show("Quantidade de " + dto.Nome + " Não é suficiente para atender a esta venda, atualmente temos em estoque " + item.Quantidade + " " + dto.Nome);
                            }
                            else
                            {

                                for (i = 0; i < qtd; i++)

                                {
                                    produtosCarrinho.Add(dto);
                                    valordavenda = valordavenda + dto.PrecoVenda;
                                    lblValordaVenda.Text = valordavenda.ToString();

                                }

                                quantidade = item.Quantidade - qtd;

                                dgvProduto.AutoGenerateColumns = false;
                                dgvProduto.DataSource = produtosCarrinho;
                                i = 0;
                            }
                        }
                       
                    }
                }
            }

            catch (Exception ex)
            {

                MessageBox.Show("Ocorreu o erro : " + ex.Message, "lottus store");
            }
         
        }

        private void btnCadastrar_Click(object sender, EventArgs e)
        {
            try
            {
                if (Convert.ToInt32(nudQuantidade.Value) == 0)
                {
                    MessageBox.Show("Quantidae é obrigatório");
                }
                else
                {

                    ClienteDTO clie = cboCliente.SelectedItem as ClienteDTO;
                    ProdutoDTO produto = cboProduto.SelectedItem as ProdutoDTO;

                    PedidoItemDTO pedidoitem = new PedidoItemDTO();
                    PedidoDTO dto = new PedidoDTO();
                    dto.FormaPagamento = cboFormaPagamento.Text;
                    dto.Data = DateTime.Now;
                    dto.ClienteId = clie.Id;
                    dto.FuncionarioId = UserSess.Usuariologado.Id;
                    pedidoitem.IdProduto = produto.Id;
                    PedidoBusiness business = new PedidoBusiness();
                    int idpedido = business.Salvar(dto, produtosCarrinho.ToList());
                    


                    EstoqueBusiness businessestoque = new EstoqueBusiness();
                    List<PedidoConsultarView> lista = business.ConsultarPorId(idpedido);
                    List<EstoqueDTO> estoque = businessestoque.Listar();

                    foreach (PedidoConsultarView item in lista)
                    {
                        foreach (EstoqueDTO item2 in estoque)
                        {
                            if (item.IdProduto == item2.IdProduto)
                            {
                                item2.Quantidade = item2.Quantidade - item.QtdItens;
                            }
                        }
                    }


                    foreach (EstoqueDTO item in estoque)
                    {
                        EstoqueDTO estoquedto = new EstoqueDTO();

                        estoquedto.IdProduto = item.IdProduto;
                        estoquedto.Quantidade = item.Quantidade;

                        businessestoque.Alterar(estoquedto);
                    }

                    MessageBox.Show("Pedido salvo com sucesso.", "Lottus Store", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    produtosCarrinho = new BindingList<ProdutoDTO>();
                    nudQuantidade.Value = 0;
                }

            }
            catch (Exception ex)
            {

                MessageBox.Show("Ocorreu o erro : " + ex.Message, "Lottus Store");
            }
            


        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            try
            {
            frmMenu tela = new frmMenu();
            tela.Show();
            this.Hide();
            }
            catch (Exception ex)
            {

                MessageBox.Show("Ocorreu o erro : " + ex.Message, "Lottus Sotre");
            }

        }

        private void textBox2_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {
            Validação v = new Validação();
            v.soletras(e); 
            }
            catch (Exception ex)
            {

                MessageBox.Show("Ocorreu o erro : " + ex.Message, "Lottus Store");
            }

        }

        private void cboFuncionario_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void nudQuantidade_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {
            Validação v = new Validação();
            v.sonumeros(e);
            }
            catch (Exception ex)
            {

                MessageBox.Show("Ocorreu o erro : " + ex.Message, "Lottus Store");
            }

        }

        private void cboCliente_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void nudQuantidade_ValueChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            produtosCarrinho = new BindingList<ProdutoDTO>();
            dgvProduto.AutoGenerateColumns = false;
            dgvProduto.DataSource = produtosCarrinho;
            valordavenda = 0;
            lblValordaVenda.Text = valordavenda.ToString();
            quantidade = 0;
            i = 10;
        }

        private void txtCategoria_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((Keys)e.KeyChar == Keys.Enter)
            {
                btnAdicionar_Click(null, null);
            }
        }
    }
}
