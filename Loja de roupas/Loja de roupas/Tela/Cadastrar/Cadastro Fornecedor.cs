﻿using Loja_de_roupas.DB.Fornecedor;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Loja_de_roupas
{
    public partial class Cadastro_de_fornecedor : Form
    {
        public Cadastro_de_fornecedor()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {

            try
            {
            FornecedorDTO dto = new FornecedorDTO();
            dto.Nome = txtNome.Text.Trim();
            dto.Telefone = txtTelef.Text.Trim();
            dto.Email = txtEmail.Text.Trim();
            dto.CEP = txtCep.Text.Trim();

            FornecedorBusiness business = new FornecedorBusiness();
            business.Salvar(dto);
                MessageBox.Show("Cadastro efetuado com sucesso", "Lottus Store");
            }
            catch (Exception ex)
            {

                MessageBox.Show("Ocorreu o erro : " + ex.Message, "Lottus Store");
            }
            

        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            try
            {
            frmMenu tela = new frmMenu();
            tela.Show();
            this.Hide();
            }
            catch (Exception ex)
            {

                MessageBox.Show("Ocorreu o erro : " + ex.Message, "Lottus Store");
            }

        }

        private void txtEmail_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtNome_KeyPress(object sender, KeyPressEventArgs e)
        {

            try
            {
            Validação v = new Validação();
            v.soletras(e);
            }
            catch (Exception ex)
            {

                MessageBox.Show("Ocorreu o erro : " + ex.Message, "Lottus Store");
            }

        }

        private void txtEmail_KeyPress(object sender, KeyPressEventArgs e)
        {
           
        }

        private void txtTelefone_MaskInputRejected(object sender, MaskInputRejectedEventArgs e)
        {

        }

        private void maskedTextBox1_MaskInputRejected(object sender, MaskInputRejectedEventArgs e)
        {

        }

        private void txtTelef_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((Keys)e.KeyChar == Keys.Enter)
            {
                button1_Click(null, null);
            }
        }
    }
}
