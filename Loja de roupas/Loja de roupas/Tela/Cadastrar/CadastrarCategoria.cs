﻿using Loja_de_roupas.DB.Base.Categoria;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Loja_de_roupas.Tela.Cadastrar
{
    public partial class CadastrarCategoria : Form
    {
        public CadastrarCategoria()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                CategoriaDTO dto = new CategoriaDTO();
                dto.Nome = txtCategoria.Text.Trim();
                CategoriaBusiness business = new CategoriaBusiness();
                business.Salvar(dto);
                MessageBox.Show("Cadastro efetuado com sucesso", "Lottus Store");
            }
            catch (Exception ex)
            {

                MessageBox.Show("Ocorreu um erro" + ex.Message, "Lottus Store");
            }

        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            frmMenu tela = new frmMenu();
            tela.Show();
            this.Hide();
        }

        private void txtCategoria_KeyPress(object sender, KeyPressEventArgs e)
        {
            Validação v = new Validação();
            v.soletras(e);
        }

        private void txtCategoria_KeyPress_1(object sender, KeyPressEventArgs e)
        {
            if ((Keys)e.KeyChar == Keys.Enter)
            {
                button1_Click(null, null);
            }
        }
    }
}
