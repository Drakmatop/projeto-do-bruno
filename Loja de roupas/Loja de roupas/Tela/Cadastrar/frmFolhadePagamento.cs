﻿using Loja_de_roupas.DB.Folha_de_Pagamento;
using Loja_de_roupas.DB.Funcionario;
using mecanica.DB.Programação.Folha_de_Pagamento;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Loja_de_roupas.DB.Telas.Outros
{
    public partial class frmFolhadePagamento : Form
    {
        public frmFolhadePagamento()
        {
            InitializeComponent();

            lblText.Visible = false;
            lblSalarioLiquido.Visible = false;

            lblPorcentagem.Visible = false;
            nudPorcentagem.Visible = false;
            lblHoraExtra.Visible = false;
            nudHorasExtras.Visible = false;

            lblConvenio.Visible = false;
            nudConvenio.Visible = false;
            CarregarCombo();
        }
       
        void CarregarCombo()
        {
            FuncionarioBusiness business = new FuncionarioBusiness();
            List<FuncionarioDTO> Funcionarios = business.Listar();

            cboFuncionario.ValueMember = nameof(FuncionarioDTO.Id);
            cboFuncionario.DisplayMember = nameof(FuncionarioDTO.Nome);
            cboFuncionario.DataSource = Funcionarios;

        }
        CalculosFolhadePagamento folha = new CalculosFolhadePagamento();
        decimal VT = 0;
        decimal SalarioBruto = 0;
        decimal DSR;
        decimal INSS;
        decimal IR = 0;
        decimal FGTS;
        decimal SalarioLiquido;
        decimal ValordeDesconto = 0;
        decimal valordeatrasos = 0;
        const int ht = 220;

        private void btnCalcular_Click(object sender, EventArgs e)
        {
             try
            {
                
                
                int he = Convert.ToInt32(nudHorasExtras.Value);
                decimal porcentagem = nudPorcentagem.Value;
                int diastrabalhados = folha.CalcularDiasTrabalhados(Convert.ToInt32(nudFaltasPriSemana.Value), Convert.ToInt32(nudFaltaSegSemana.Value), Convert.ToInt32(nudFaltaTerSemana.Value), Convert.ToInt32(nudFaltaQuarSemana.Value));
                int finaisdesemana = folha.CalcularFinaisdeSemanas(Convert.ToInt32(nudFaltasPriSemana.Value), Convert.ToInt32(nudFaltaSegSemana.Value), Convert.ToInt32(nudFaltaTerSemana.Value), Convert.ToInt32(nudFaltaQuarSemana.Value));
                int totaldefaltas = folha.CalcularTotalDeFaltas(Convert.ToInt32(nudFaltasPriSemana.Value), Convert.ToInt32(nudFaltaSegSemana.Value), Convert.ToInt32(nudFaltaTerSemana.Value), Convert.ToInt32(nudFaltaQuarSemana.Value));
                int horasatrasadas =  Convert.ToInt32(nudHorasAtradas.Value);

                FuncionarioDTO funcionario = cboFuncionario.SelectedItem as FuncionarioDTO;
                SalarioBruto = funcionario.SalarioBruto;

                if (nudHorasExtras.Value > 0 && chkVT.Checked == true)
                {
                    
                    decimal valorporhora = folha.CalcularValorPorHora(SalarioBruto, ht);
                    decimal totalhorasextras = folha.CalcularValorHoraExtra(valorporhora, porcentagem, he);
                    DSR = folha.CalcularDSR(totalhorasextras, diastrabalhados, finaisdesemana);
                    decimal TotalSalarioBruto = SalarioBruto + totalhorasextras + DSR;
                    valordeatrasos = folha.CalcularValorAtrasos(valorporhora, horasatrasadas);

                   
                    INSS = folha.CalcularINSS(SalarioBruto, TotalSalarioBruto);
                    ValordeDesconto = folha.ValordeDesconto(TotalSalarioBruto, INSS);
                    IR = folha.CalcularIR(TotalSalarioBruto, INSS);
                    FGTS = folha.CalcularFGTS(TotalSalarioBruto);
                    VT = SalarioBruto * 0.06m;

                    SalarioLiquido = SalarioBruto + totalhorasextras + DSR - INSS - ValordeDesconto - VT - valordeatrasos - nudVR.Value - nudRefeicao.Value - nudConvenio.Value;


                    lblText.Visible = true;
                    lblSalarioLiquido.Visible = true;
                    lblSalarioLiquido.Text = SalarioLiquido.ToString();
                    lblINSS.Text = Math.Round(INSS).ToString();
                    lblFGTS.Text =Math.Round(FGTS).ToString();
                    lblDSR.Text = Math.Round(DSR).ToString();
                    lblIR.Text = Math.Round(IR).ToString();
                    lblVT.Text = Math.Round(VT).ToString();
                    lblValordedesconto.Text =  Math.Round(ValordeDesconto).ToString();
                    lblValordeatraso.Text = Math.Round(valordeatrasos).ToString();

                }
                else if (nudHorasExtras.Value > 0 && chkVT.Checked == false)
                {
                    decimal valorporhora = folha.CalcularValorPorHora(SalarioBruto, ht);
                    decimal totalhorasextras = folha.CalcularValorHoraExtra(valorporhora, porcentagem, he);
                    DSR = folha.CalcularDSR(totalhorasextras, diastrabalhados, finaisdesemana);
                    decimal TotalSalarioBruto = SalarioBruto + totalhorasextras + DSR;
                    valordeatrasos = folha.CalcularValorAtrasos(valorporhora, horasatrasadas);



                    INSS = folha.CalcularINSS(SalarioBruto, TotalSalarioBruto);
                    ValordeDesconto = folha.ValordeDesconto(TotalSalarioBruto, INSS);

                    IR = folha.CalcularIR(TotalSalarioBruto, INSS);
                    FGTS = folha.CalcularFGTS(TotalSalarioBruto);


                    SalarioLiquido = SalarioBruto + totalhorasextras + DSR - INSS - ValordeDesconto - VT - valordeatrasos - nudVR.Value - nudRefeicao.Value - nudConvenio.Value;

                    lblText.Visible = true;
                    lblSalarioLiquido.Visible = true;
                    lblSalarioLiquido.Text = SalarioLiquido.ToString();
                    lblINSS.Text = Math.Round(INSS).ToString();
                    lblFGTS.Text = Math.Round(FGTS).ToString();
                    lblDSR.Text = Math.Round(DSR).ToString();
                    lblIR.Text = Math.Round(IR).ToString();
                    lblVT.Text = Math.Round(VT).ToString();
                    lblValordedesconto.Text = Math.Round(ValordeDesconto).ToString();
                    lblValordeatraso.Text = Math.Round(valordeatrasos).ToString();

                }
                else if (nudHorasExtras.Value == 0 && chkVT.Checked == true)
                {

                    decimal descontodosalariobruto = SalarioBruto / 30 * totaldefaltas;
                    SalarioBruto = SalarioBruto - descontodosalariobruto;

                    decimal valorporhora = folha.CalcularValorPorHora(SalarioBruto, ht);

                    INSS = folha.CalcularINSS(SalarioBruto, SalarioBruto);
                    ValordeDesconto = folha.ValordeDesconto(SalarioBruto, SalarioBruto);
                    valordeatrasos = folha.CalcularValorAtrasos(valorporhora, horasatrasadas);

                    IR = folha.CalcularIR(SalarioBruto, INSS);
                    FGTS = folha.CalcularFGTS(SalarioBruto);
                    VT = SalarioBruto * 0.06m;

                    SalarioLiquido = SalarioBruto - INSS - ValordeDesconto - VT - valordeatrasos - nudVR.Value - nudRefeicao.Value - nudConvenio.Value;

                    lblText.Visible = true;
                    lblSalarioLiquido.Visible = true;
                    lblSalarioLiquido.Text = SalarioLiquido.ToString();
                    lblINSS.Text = INSS.ToString();
                    lblFGTS.Text = FGTS.ToString();
                    lblDSR.Text = DSR.ToString();
                    lblIR.Text = IR.ToString();
                    lblVT.Text = VT.ToString();
                    lblValordedesconto.Text = ValordeDesconto.ToString();
                    lblValordeatraso.Text = valordeatrasos.ToString();
                }

                else if (nudHorasExtras.Value == 0 && chkVT.Checked == false)
                {
                    decimal descontodosalariobruto = SalarioBruto / 30 * totaldefaltas;
                    SalarioBruto = SalarioBruto - descontodosalariobruto;

                    decimal valorporhora = folha.CalcularValorPorHora(SalarioBruto, ht);
                    valordeatrasos = folha.CalcularValorAtrasos(valorporhora, horasatrasadas);

                    INSS = folha.CalcularINSS(SalarioBruto, SalarioBruto);
                    ValordeDesconto = folha.ValordeDesconto(SalarioBruto, SalarioBruto);

                    IR = folha.CalcularIR(SalarioBruto, INSS);
                    FGTS = folha.CalcularFGTS(SalarioBruto);

                    SalarioLiquido = SalarioBruto - INSS - ValordeDesconto - VT - valordeatrasos - nudVR.Value - nudRefeicao.Value - nudConvenio.Value;

                    lblText.Visible = true;
                    lblSalarioLiquido.Visible = true;
                    lblSalarioLiquido.Text = SalarioLiquido.ToString();
                    lblINSS.Text = INSS.ToString();
                    lblFGTS.Text = FGTS.ToString();
                    lblDSR.Text = DSR.ToString();
                    lblIR.Text = IR.ToString();
                    lblVT.Text = VT.ToString();
                    lblValordedesconto.Text = ValordeDesconto.ToString();
                    lblValordeatraso.Text = valordeatrasos.ToString();
                }
            }
            catch (Exception ex)
            {

                MessageBox.Show("Ocorreu o erro : " + ex.Message, "Lottus Store");
            }
        }
    


        private void chkHoraExtra_CheckedChanged(object sender, EventArgs e)
        {
            if (chkHoraExtra.Checked == true)
            {
                lblPorcentagem.Visible = true;
                nudPorcentagem.Visible = true;
                lblHoraExtra.Visible = true;
                nudHorasExtras.Visible = true;
            }

            if (chkHoraExtra.Checked == false)
            {
                lblPorcentagem.Visible = false;
                nudPorcentagem.Visible = false;
                lblHoraExtra.Visible = false;
                nudHorasExtras.Visible = false;
            }

        }


      

        private void btnSalvar_Click(object sender, EventArgs e)
        {
            try
            {
                FolhadePagamentoDTO dto = new FolhadePagamentoDTO();
                FuncionarioDTO funcionario = cboFuncionario.SelectedItem as FuncionarioDTO;

                if (lblSalarioLiquido.Text == "-")
                {
                    MessageBox.Show("Faça o calculo antes de salvar");
                }
                else
                {
                    dto.Bt_Valetransporte = chkVT.Checked;
                    dto.FGTS = FGTS;
                    dto.HorasExtras = Convert.ToInt32(nudHorasExtras.Value);
                    dto.INSS = INSS;
                    dto.IR = IR;
                    dto.SalarioLiquido = SalarioLiquido;
                    dto.vl_ValeTransporte = VT;
                    dto.IdFuncionario = funcionario.Id;
                    dto.Pagamento = DateTime.Now;

                    FolhadePagamentoBusiness business = new FolhadePagamentoBusiness();
                    business.Salvar(dto);
                    MessageBox.Show("Folha de pagamento salva com sucesso", "Lottus Store");

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocorreu o erro : " + ex.Message, "Lottus Store");
            }

        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            frmMenu tela = new frmMenu();
            tela.Show();
            this.Hide();
        }

        private void chkConvenio_CheckedChanged(object sender, EventArgs e)
        {
            if (chkConvenio.Checked == true)
            {
                lblConvenio.Visible = true;
                nudConvenio.Visible = true;
            }
            else
            {

                lblConvenio.Visible = false;
                nudConvenio.Visible = false;
            }
        }

        private void cboFuncionario_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}

