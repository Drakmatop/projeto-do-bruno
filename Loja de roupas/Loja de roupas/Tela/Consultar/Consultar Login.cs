﻿using Loja_de_roupas.DB.Funcionario;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Loja_de_roupas
{
    public partial class Consultar_Login : Form
    {
        public Consultar_Login()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                FuncionarioBusiness business = new FuncionarioBusiness();
                List<FuncionarioDTO> view = business.ConsultarLogin(txtFuncionario.Text);

                dataGridView1.AutoGenerateColumns = false;
            dataGridView1.DataSource = view;
            }
            catch (Exception ex)
            {

                MessageBox.Show("Ocorreu um erro" + ex.Message, "Lottus Store");
            }

           
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            try
            {
            frmMenu tela = new frmMenu();
            tela.Show();
            this.Hide();
            }
            catch (Exception ex)
            {

                MessageBox.Show("Ocorreu um erro" + ex.Message, "Lottus Store");
            }

        }

        private void txtFuncionario_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {
            Validação v = new Validação();
            v.soletras(e);
            }
            catch (Exception ex)
            {

                MessageBox.Show("Ocorreu um erro" + ex.Message, "Lottus Store");
            }

        }
    }
}
