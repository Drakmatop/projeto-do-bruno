﻿using Loja_de_roupas.DB.Base.Categoria;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Loja_de_roupas
{
    public partial class Consultar_Cartegoria : Form
    {
        public Consultar_Cartegoria()
        {
            InitializeComponent();
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            try
            {
            frmMenu tela = new frmMenu();
            tela.Show();
            this.Hide();
            }
            catch (Exception ex)
            {

                MessageBox.Show("Ocorreu um erro" + ex.Message, "Lottus Store");
            }


        }

        private void txt1_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {
            Validação v = new Validação();
            v.soletras(e);
            }
            catch (Exception ex)
            {

                MessageBox.Show("Ocorreu um erro" + ex.Message, "Lottus Store");
            }

        }

        private void button1_Click(object sender, EventArgs e)
        {
            CategoriaBusiness business = new CategoriaBusiness();
            List<CategoriaDTO> dto = new List<CategoriaDTO>();
            dto = business.Listar();
            dataGridView1.AutoGenerateColumns = false;
            dataGridView1.DataSource = dto;
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 2)
            {
                DialogResult r = MessageBox.Show("Deseja realmente excluir este item?", "Lottus Store", MessageBoxButtons.YesNo);
                if (r == DialogResult.Yes)
                {
                    CategoriaBusiness business = new CategoriaBusiness();
                    CategoriaDTO dto = dataGridView1.CurrentRow.DataBoundItem as CategoriaDTO;

                    business.Remover(dto.Id);
                    button1_Click(null, null);
                }
            }
           
        }
    }
}
