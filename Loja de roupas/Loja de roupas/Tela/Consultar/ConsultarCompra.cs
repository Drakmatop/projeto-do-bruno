﻿using Loja_de_roupas.DB.Compras;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Loja_de_roupas.DB.Telas.Consultar
{
    public partial class ConsultarCompra : Form
    {
        public ConsultarCompra()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            CompraBusiness business = new CompraBusiness();
            List<VwConsultarCompra> lista = business.Consultar(txtProduto.Text);
            dataGridView1.AutoGenerateColumns = false;
            dataGridView1.DataSource = lista;

        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 5)
            {
                CompraBusiness business = new CompraBusiness();
                VwConsultarCompra dto = dataGridView1.CurrentRow.DataBoundItem as VwConsultarCompra;

                business.Remover(dto.Id);
                button1_Click(null, null);
            }
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            frmMenu tela = new frmMenu();
            tela.Show();
            this.Hide();
        }
    }
}
