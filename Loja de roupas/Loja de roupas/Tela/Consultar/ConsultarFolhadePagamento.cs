﻿using mecanica.DB.Programação.Folha_de_Pagamento;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Loja_de_roupas.DB.Telas.Consultar
{
    public partial class ConsultarFolhadePagamento : Form
    {
        public ConsultarFolhadePagamento()
        {
            InitializeComponent();
        }

        private void ConsultarFolhadePagamento_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            FolhadePagamentoBusiness business = new FolhadePagamentoBusiness();
            List<FolhadePagamentoDTO> lista = business.Buscar(txtFuncionario.Text);

            dataGridView1.AutoGenerateColumns = false;
            dataGridView1.DataSource = lista;
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

            frmMenu tela = new frmMenu();
            tela.Show();
            this.Hide();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

            /*if (e.ColumnIndex == 7)
            {
                FolhadePagamentoDTO dto = dataGridView1.CurrentRow.DataBoundItem as FolhadePagamentoDTO;
                DB.Telas.Alterar.Alterar_Folha_de_Pagamento tela = new DB.Telas.Alterar.Alterar_Folha_de_Pagamento();
                tela.LoadScreen(dto);
                tela.Show();
                this.Hide();


            }*/

            if (e.ColumnIndex == 7)
            {
                DialogResult r = MessageBox.Show("Deseja realmente excluir este item?", "Lottus Store", MessageBoxButtons.YesNo);
                if (r == DialogResult.Yes)
                {
                    FolhadePagamentoBusiness business = new FolhadePagamentoBusiness();
                    FolhadePagamentoDTO dto = dataGridView1.CurrentRow.DataBoundItem as FolhadePagamentoDTO;
                    business.Remover(dto.Id);
                    button1_Click(null, null);
                }
            }


        }
    }
}
