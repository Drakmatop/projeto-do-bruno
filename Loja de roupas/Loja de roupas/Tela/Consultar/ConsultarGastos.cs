﻿using Loja_de_roupas.DB.Estoque;
using Loja_de_roupas.DB.Gastos;
using mecanica.DB.Programação.Estoque;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Loja_de_roupas.Tela.Consultar
{
    public partial class ConsultarGastos : Form
    {
        public ConsultarGastos()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            GastoBusiness business = new GastoBusiness();
            List<GastoDTO> lista = business.Consultar(txtFuncionario.Text);

            dgvConsultarFuncionario.AutoGenerateColumns = false;
            dgvConsultarFuncionario.DataSource = lista;

        }

       

        private void dgvConsultarFuncionario_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

            if (e.ColumnIndex == 4)
            {
                DialogResult r = MessageBox.Show("Deseja remover este item?", "Américas Mecânica", MessageBoxButtons.YesNo);
                if (r == DialogResult.Yes)
                {
                    GastoDTO vw = dgvConsultarFuncionario.CurrentRow.DataBoundItem as GastoDTO;
                    GastoBusiness business = new GastoBusiness();
                    business.Remover(vw.Id);
                }
            }
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

            frmMenu tela = new frmMenu();
            tela.Show();
            this.Hide();
        }

        private void txtFuncionario_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
