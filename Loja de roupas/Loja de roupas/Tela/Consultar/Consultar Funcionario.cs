﻿using Loja_de_roupas.DB.Funcionario;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Loja_de_roupas
{
    public partial class Consultar_Funcionario : Form
    {
        public Consultar_Funcionario()
        {
            InitializeComponent();
        }

        private void txt1_TextChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {

            try
            {
            FuncionarioBusiness business = new FuncionarioBusiness();
            List<FuncionarioDTO> dto = business.Consultar(txtFuncionario.Text);
            dataGridView1.AutoGenerateColumns = false;
            dataGridView1.DataSource = dto;
            }
            catch (Exception ex)
            {

                MessageBox.Show("Ocorreu um erro" + ex.Message, "Lottus Store");
            }
           
        }

        private void lbl1_Click(object sender, EventArgs e)
        {

        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

            


                if (e.ColumnIndex == 5)
                {
                    DialogResult r = MessageBox.Show("Deseja realmente excluir este item?", "Lottus Store", MessageBoxButtons.YesNo);
                    if (r == DialogResult.Yes)
                    {
                        FuncionarioBusiness business = new FuncionarioBusiness();
                        FuncionarioDTO a = dataGridView1.CurrentRow.DataBoundItem as FuncionarioDTO;
                        business.Remover(a.Id);
                        button1_Click(null, null);
                    }
                }
            
        }
        

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            try
            {
            frmMenu tela = new frmMenu();
            tela.Show();
            this.Hide();
            }
            catch (Exception ex)
            {

                MessageBox.Show("Ocorreu um erro" + ex.Message, "Lottus Store");
            }

        }

        private void txtFuncionario_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {
            Validação v = new Validação();
            v.soletras(e);
            }
            catch (Exception ex)
            {

                MessageBox.Show("Ocorreu um erro" + ex.Message, "Lottus Store");
            }

        }
    }
}
