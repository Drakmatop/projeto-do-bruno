﻿using Loja_de_roupas.DB.Funcionario;
using Loja_de_roupas.DB.Produto;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Loja_de_roupas
{
    public partial class Consultar_Produto : Form
    {
        public Consultar_Produto()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
            ProdutoBusiness_ business = new ProdutoBusiness_();
            List<ProdutoDTO> lista = business.Consultar(txtProduto.Text);
            dataGridView1.AutoGenerateColumns = false;
            dataGridView1.DataSource = lista;
            }
            catch (Exception ex)
            {

                MessageBox.Show("Ocorreu um erro" + ex.Message, "Lottus Store");
            }

        }

        private void txtProduto_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {
            Validação v = new Validação();
            v.soletras(e);
            }
            catch (Exception ex)
            {

                MessageBox.Show("Ocorreu um erro" + ex.Message, "Lottus Store");
            }

        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            try
            {
            frmMenu tela = new frmMenu();
            tela.Show();
            this.Hide();
            }
            catch (Exception ex)
            {

                MessageBox.Show("Ocorreu um erro" + ex.Message, "Lottus Store");
            }

        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

            if (e.ColumnIndex == 8)
            {
                DialogResult r = MessageBox.Show("Deseja realmente excluir este item?", "Lottus Store", MessageBoxButtons.YesNo);
                if (r == DialogResult.Yes)
                {
                    ProdutoBusiness_ business = new ProdutoBusiness_();
                    ProdutoDTO dto = dataGridView1.CurrentRow.DataBoundItem as ProdutoDTO;

                    business.Remover(dto.Id);
                    button1_Click(null, null);
                }
            }
        }
    }
}
