﻿using Loja_de_roupas.DB.Estoque;
using Loja_de_roupas.DB.Produto;
using mecanica.DB.Programação.Estoque;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Loja_de_roupas.Tela.Consultar
{
    public partial class ConsultarEstoque : Form
    {
        public ConsultarEstoque()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            EstoqueBusiness business = new EstoqueBusiness();
            List<vwEstoque> vw = business.Consultar(txtProduto.Text);

            dgvConsultarEstoque.AutoGenerateColumns = false;
            dgvConsultarEstoque.DataSource = vw;
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
           
        }

        private void dgvConsultarEstoque_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 3)
            {
                DialogResult r = MessageBox.Show("Deseja remover este item?", "Américas Mecânica", MessageBoxButtons.YesNo);
                if (r == DialogResult.Yes)
                {
                    ProdutoDTO dto = dgvConsultarEstoque.CurrentRow.DataBoundItem as ProdutoDTO;
                    ProdutoBusiness_ pro = new ProdutoBusiness_();
                    vwEstoque vw = dgvConsultarEstoque.CurrentRow.DataBoundItem as vwEstoque;
                    EstoqueBusiness business = new EstoqueBusiness();
                    business.Remover(vw.Id);
                    button1_Click(null, null);
                }
            }
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {

            frmMenu tela = new frmMenu();
            tela.Show();
            this.Hide();
        }
    }
}
