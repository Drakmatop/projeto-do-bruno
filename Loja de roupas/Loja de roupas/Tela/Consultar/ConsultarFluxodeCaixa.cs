﻿using Loja_de_roupas.DB.Fluxo_de_Caixa;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Loja_de_roupas.DB.Telas.Consultar
{
    public partial class ConsultarFluxodeCaixa : Form
    {
        public ConsultarFluxodeCaixa()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            FluxodeCaixaBusiness business = new FluxodeCaixaBusiness();
            List<VwConsultarFluxodeCaixa> lista = business.Consultar(dtpInicio.Value, dtpFim.Value);

            dgvFluxo.AutoGenerateColumns = false;
            dgvFluxo.DataSource = lista;
            decimal saldo = 0;
            foreach (VwConsultarFluxodeCaixa item in lista)
            {
                saldo = saldo + item.Lucro;
            }

            lblSaldo.Text = saldo.ToString();
            if (saldo < 0)
            {
                lblSaldo.ForeColor = Color.Red;
            }
            else
            {
                lblSaldo.ForeColor = Color.Green;
            }
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            frmMenu tela = new frmMenu();
            tela.Show();
            this.Hide();
        }

        private void dtpFim_ValueChanged(object sender, EventArgs e)
        {

        }
    }
}

