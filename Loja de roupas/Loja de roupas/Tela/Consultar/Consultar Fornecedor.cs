﻿using Loja_de_roupas.DB.Fornecedor;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Loja_de_roupas
{
    public partial class Consultar_Fornecedor : Form
    {
        public Consultar_Fornecedor()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
            FornecedorBusiness business = new FornecedorBusiness();
            List<FornecedorDTO> lista = business.Consultar(string.Empty);

            dataGridView1.AutoGenerateColumns = false;
            dataGridView1.DataSource = lista;
            }
            catch (Exception ex)
            {

                MessageBox.Show("Ocorreu um erro" + ex.Message, "Lottus Store");
            }

        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            try
            {
            frmMenu tela = new frmMenu();
            tela.Show();
            this.Hide();
            }
            catch (Exception ex)
            {

                MessageBox.Show("Ocorreu um erro" + ex.Message, "Lottus Store");
            }

        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

            if (e.ColumnIndex == 4)
            {
                DialogResult r = MessageBox.Show("Deseja realmente excluir este item?", "Lottus Store", MessageBoxButtons.YesNo);
                if (r == DialogResult.Yes)
                {
                    FornecedorBusiness busines = new FornecedorBusiness();
                    FornecedorDTO dto = dataGridView1.CurrentRow.DataBoundItem as FornecedorDTO;
                    busines.Remover(dto.Id);
                    button1_Click(null, null);
                }
            }

            }
    }
}
