﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Loja_de_roupas.DB.Base.Categoria
{
    class CategoriaDatabase
    {
        public int Salvar(CategoriaDTO dto)
        {
            string script = @"INSERT INTO tb_categoria (nm_categoria)
                            VALUES (@nm_categoria)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_categoria", dto.Nome));

            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parms);
        }
        public void Alterar(CategoriaDTO dto)
        {
            string script = @"UPDATE tb_categoria SET nm_categoria = @nm_categoria WHERE id_categoria = @id_categoria";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_categoria", dto.Nome));
            parms.Add(new MySqlParameter("id_categoria", dto.Id));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }
        public void Remover(int id)
        {
            string script = @"DELETE FROM tb_categoria WHERE id_categoria = @id_categoria";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_categoria", id));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }

        public List<CategoriaDTO> Listar()
        {
            string script = @"SELECT * FROM tb_categoria";

            List<MySqlParameter> parms = new List<MySqlParameter>();

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<CategoriaDTO> lista = new List<CategoriaDTO>();
            while (reader.Read())
            {
                CategoriaDTO dto = new CategoriaDTO();
                dto.Id = reader.GetInt32("id_categoria");
                dto.Nome = reader.GetString("nm_categoria");
               
                lista.Add(dto);
            }
            reader.Close();

            return lista;
        }

    }
}
