﻿using Loja_de_roupas.DB.Base;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Loja_de_roupas.DB.Produto
{
    class ProdutoDatabase
    {
        public int Salvar(ProdutoDTO dto)
        {
            string script = @"INSERT INTO tb_produto (nm_produto, vl_preco, ds_tamanho, ds_cor,id_categoria,id_fornecedor)
                            VALUES (@nm_produto, @vl_preco, @ds_tamanho, @ds_cor,@id_categoria,@id_fornecedor)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_produto", dto.Nome));
            parms.Add(new MySqlParameter("vl_preco", dto.Preco));
            parms.Add(new MySqlParameter("ds_tamanho", dto.Tamanho));
            parms.Add(new MySqlParameter("ds_cor", dto.Cor));
            parms.Add(new MySqlParameter("id_categoria", dto.IdCategoria));
            parms.Add(new MySqlParameter("id_fornecedor", dto.IdFornecedor));


            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parms);
        }

        public void Remover(int id)
        {
            string script = @"DELETE FROM tb_produto WHERE id_produto = @id_produto";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_produto", id));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }

        public List<ProdutoDTO> Consultar(string produto)
        {
            string script = @"SELECT * FROM tb_produto WHERE nm_produto like @nm_produto";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_produto","%" + produto + "%"));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<ProdutoDTO> lista = new List<ProdutoDTO>();
            while (reader.Read())
            {
                ProdutoDTO dto = new ProdutoDTO();
                dto.Id = reader.GetInt32("id_produto");
                dto.Nome = reader.GetString("nm_produto");
                dto.Preco = reader.GetDecimal("vl_preco");
                dto.Tamanho = reader.GetString("ds_tamanho");
                dto.Cor = reader.GetString("ds_cor");
                dto.IdCategoria = reader.GetInt32("id_categoria");
                dto.IdFornecedor = reader.GetInt32("id_fornecedor");

                lista.Add(dto);
            }
            reader.Close();

            return lista;
        }

        public List<ProdutoDTO> Listar()
        {
            string script = @"SELECT * FROM tb_produto";

            List<MySqlParameter> parms = new List<MySqlParameter>();

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<ProdutoDTO> lista = new List<ProdutoDTO>();
            while (reader.Read())
            {
                ProdutoDTO dto = new ProdutoDTO();
                dto.Id = reader.GetInt32("id_produto");
                dto.Nome = reader.GetString("nm_produto");
                dto.Preco = reader.GetDecimal("vl_preco");
                dto.Tamanho = reader.GetString("ds_tamanho");
                dto.Cor = reader.GetString("ds_cor");
                dto.IdCategoria = reader.GetInt32("id_categoria");
                dto.IdFornecedor = reader.GetInt32("id_fornecedor");

                lista.Add(dto);
            }
            reader.Close();

            return lista;
        }
    }
}
