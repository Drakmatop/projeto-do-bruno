﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Loja_de_roupas.DB.Produto
{
    class ProdutoDTO
    {
        public int Id{ get; set; }

        public string Nome { get; set; }

        public decimal Preco{ get; set; }

        public string Tamanho{ get; set; }

        public string Cor{ get; set; }

        public int IdFornecedor { get; set; }

        public int IdCategoria{ get; set; }
    }
}
