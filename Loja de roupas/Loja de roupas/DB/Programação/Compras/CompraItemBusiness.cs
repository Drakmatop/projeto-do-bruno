﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Loja_de_roupas.DB.Compras
{
    class CompraItemBusiness
    {
        public int Salvar(CompraItemDTO dto)
        {
            CompraItemDatabase db = new CompraItemDatabase();
            return db.Salvar(dto);
        }
        public void Remover(int id)
        {
            CompraItemDatabase db = new CompraItemDatabase();
            db.Remover(id);
        }

        public List<CompraItemDTO> ConsultarPorPedido(int idPedido)
        {
            CompraItemDatabase db = new CompraItemDatabase();
            return db.ConsultarPorCompra(idPedido);
        }

    }
}
