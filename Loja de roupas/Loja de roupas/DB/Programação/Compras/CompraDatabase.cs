﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Loja_de_roupas.DB.Compras
{
    class CompraDatabase
    {
        public int Salvar(CompraDTO dto)
        {
            string script = @"INSERT INTO tb_compra(qtd_unidade, vl_compra, dt_compra) VALUES
                           (@qtd_unidade, @vl_compra, @dt_compra) ";


            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("qtd_unidade", dto.QtdItens));
            parms.Add(new MySqlParameter("vl_compra", dto.ValorCompra));
            parms.Add(new MySqlParameter("dt_compra", dto.DataCompra));

            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parms);
        }

        public List<VwConsultarCompra> Consultar(string Produto)
        {
            string script = @"select * from vw_compra_consultar
            WHERE nm_produto like @nm_produto;";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_produto", "%" + Produto + "%"));

            Database db = new Database();

            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<VwConsultarCompra> lista = new List<VwConsultarCompra>();

            while (reader.Read())
            {
                VwConsultarCompra dto = new VwConsultarCompra();

                dto.NomeProduto = reader.GetString("nm_produto");
                dto.NomeFornecedor = reader.GetString("nm_fornecedor");
                dto.QtdUnidades = reader.GetInt32("qtd_itens");
                dto.ValorCompra = reader.GetDecimal("vl_total");
                dto.DataCompra = reader.GetDateTime("dt_compra");
                dto.Id = reader.GetInt32("id_compra");
                dto.IdProduto = reader.GetInt32("id_roupa");


                lista.Add(dto);
            }
            reader.Close();

            return lista;

        }
        public List<VwConsultarCompra> ConsultarPorId(int id)
        {
            string script = @"select * from vw_compra_consultar
            WHERE id_compra = @id_compra;";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_compra", id));

            Database db = new Database();

            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<VwConsultarCompra> lista = new List<VwConsultarCompra>();

            while (reader.Read())
            {
                VwConsultarCompra dto = new VwConsultarCompra();

                dto.NomeProduto = reader.GetString("nm_produto");
                dto.NomeFornecedor = reader.GetString("nm_fornecedor");
                dto.QtdUnidades = reader.GetInt32("qtd_itens");
                dto.ValorCompra = reader.GetDecimal("vl_total");
                dto.DataCompra = reader.GetDateTime("dt_compra");
                dto.Id = reader.GetInt32("id_compra");
                dto.IdProduto = reader.GetInt32("id_roupa");

                lista.Add(dto);
            }
            reader.Close();

            return lista;

        }
        public void Remover(int id)
        {
            string script = @"DELETE FROM tb_compra WHERE id_compra = @id_compra";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_compra", id));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }
    }
}
