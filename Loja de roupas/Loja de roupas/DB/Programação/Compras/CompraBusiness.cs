﻿using Loja_de_roupas.DB.Produto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Loja_de_roupas.DB.Compras
{
    class CompraBusiness
    {
        CompraDatabase db = new CompraDatabase();

        public int Salvar(CompraDTO compra, List<ProdutoDTO> produtos)
        {
            int idCompra = db.Salvar(compra);

            CompraItemBusiness compraBusiness = new CompraItemBusiness();
            foreach (ProdutoDTO item in produtos)
            {
                CompraItemDTO compraDTO = new CompraItemDTO();
                compraDTO.IdCompra = idCompra;
                compraDTO.IdProduto = item.Id;

                compraBusiness.Salvar(compraDTO);
            }
            return idCompra;
        }

        public List<VwConsultarCompra> Consultar(string produto)
        {
            return db.Consultar(produto);
        }

        public void Remover(int id)
        {
            CompraItemBusiness business = new CompraItemBusiness();
            business.Remover(id);
            db.Remover(id);
        }
        public List<VwConsultarCompra> ConsultarPorId(int id)
        {
            return db.ConsultarPorId(id);
        }
    }
}
