﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Loja_de_roupas.DB.Compras
{
    class CompraItemDatabase
    {
        public int Salvar(CompraItemDTO dto)
        {
            string script = @"INSERT INTO tb_compraitem (id_produto, id_compra) VALUES (@id_produto, @id_compra)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_compra", dto.IdCompra));
            parms.Add(new MySqlParameter("id_produto", dto.IdProduto));

            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parms);
        }
        public void Remover(int id)
        {
            string script = @"DELETE FROM tb_compraitem WHERE id_compra = @id_compra";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_compra", id));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }
        public List<CompraItemDTO> ConsultarPorCompra(int idcompra)
        {
            string script = @"SELECT * FROM tb_compraitem WHERE id_compra = @id_compra";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_compra", idcompra));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<CompraItemDTO> lista = new List<CompraItemDTO>();
            while (reader.Read())
            {
                CompraItemDTO dto = new CompraItemDTO();
                dto.Id = reader.GetInt32("id_compraitem");
                dto.IdCompra = reader.GetInt32("id_compra");
                dto.IdProduto = reader.GetInt32("id_roupa");

                lista.Add(dto);
            }
            reader.Close();

            return lista;
        }

    }
}
