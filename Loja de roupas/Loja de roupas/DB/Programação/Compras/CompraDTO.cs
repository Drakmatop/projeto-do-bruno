﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Loja_de_roupas.DB.Compras
{
    class CompraDTO
    {
        public int Id { get; set; }

        public int QtdItens { get; set; }

        public decimal ValorCompra { get; set; }

        public DateTime DataCompra { get; set; }



    }
}
