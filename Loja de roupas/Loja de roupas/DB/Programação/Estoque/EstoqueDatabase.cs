﻿using mecanica.DB.Programação.Estoque;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Loja_de_roupas.DB.Estoque
{
    class EstoqueDatabase
    {
        public int Salvar(EstoqueDTO dto)
        {
            string script =
                @"INSERT INTO tb_estoque
                (id_produto, ds_quantidade)
                VALUES
                (@id_produto, @ds_quantidade)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_produto", dto.IdProduto));
            parms.Add(new MySqlParameter("ds_quantidade", dto.Quantidade));

            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parms);
        }

        public void Alterar(EstoqueDTO dto)
        {
            string script = @"UPDATE tb_estoque SET 
                        ds_quantidade = @ds_quantidade 
                        WHERE id_produto = @id_produto";


            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("ds_quantidade", dto.Quantidade));
            parms.Add(new MySqlParameter("id_produto", dto.IdProduto));
            

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }

        public void Remover(int id)
        {
            string script = @"DELETE FROM tb_estoque WHERE id_produto = @id_produto";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_produto", id));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }

        public List<vwEstoque> Consultar(string nome)
        {
            string script = @"Select nm_produto, nm_fornecedor, ds_quantidade, id_produto FROM tb_estoque 
                                JOIN tb_produto 			p
                                ON p.id_roupa = tb_estoque.id_produto
                                JOIN tb_fornecedor 			f
                                ON f.id_fornecedor = p.id_fornecedor where nm_produto like @nm_produto; ";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_produto", nome + "%"));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<vwEstoque> lista = new List<vwEstoque>();
            while (reader.Read())
            {
                vwEstoque dto = new vwEstoque();
                dto.Id = reader.GetInt32("id_produto");
                dto.Produto = reader.GetString("nm_produto");
                dto.Quantidade = reader.GetInt32("ds_quantidade");
                dto.Fornecedor = reader.GetString("nm_fornecedor");


                lista.Add(dto);
            }
            reader.Close();

            return lista;
        }

        public List<EstoqueDTO> Listar()
        {
            string script = @"Select * FROM tb_estoque";

            List<MySqlParameter> parms = new List<MySqlParameter>();

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<EstoqueDTO> lista = new List<EstoqueDTO>();
            while (reader.Read())
            {
                EstoqueDTO dto = new EstoqueDTO();
                dto.Id = reader.GetInt32("id_estoque");
                dto.IdProduto = reader.GetInt32("id_produto");
                dto.Quantidade = reader.GetInt32("ds_quantidade");

                lista.Add(dto);
            }
            reader.Close();

            return lista;
        }
    }
}
