﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Loja_de_roupas.DB.Funcionario
{
    class FuncionarioBusiness
    {
        public int Salvar(FuncionarioDTO dto)
        {
            if (dto.Nome == string.Empty)
            {
                throw new ArgumentException("Nome da roupa é obrigatório", "Lottus Store");
            }

            if (dto.Cep == string.Empty)
            {
                throw new ArgumentException("CPF é obrigatório", "Lottus Store");
            }
            if (dto.Sobrenome == string.Empty)
            {
                throw new ArgumentException("Sobrenome é obrigatório", "Lottus Store");
            }
            if (dto.Telefone == string.Empty)
            {
                throw new ArgumentException("Telefone é obrigatório", "Lottus Store");
            }
            if (dto.DataNascimento > DateTime.Now)
            {
                throw new ArgumentException("Data é maior do que agora", "Lottus Store");
            }
            if (dto.SalarioBruto == 0)
            {
                throw new ArgumentException("Salário é obrigatório", "Lottus Store");
            }
            if (dto.Email == string.Empty)
            {
                throw new ArgumentException("E-mail é obrigatório", "Lottus Store");
            }
            if (dto.Login == string.Empty)
            {
                throw new ArgumentException("Login é obrigatório", "Lottus Store");
            }
            if (dto.Nome.Length > 25)
            {
                throw new ArgumentException("Muitos caracteres", "Lottus Store");
            }
            if (dto.Telefone.Length > 15)
            {
                throw new ArgumentException("Muitos caracteres", "Lottus Store");
            }
            if (dto.Cep.Length > 15)
            {
                throw new ArgumentException("Muitos caracteres", "Lottus Store");
            }
            if (dto.Email.Length > 50)
            {
                throw new ArgumentException("Muitos caracteres", "Lottus Store");
            }
            if (dto.Sobrenome.Length > 35)
            {
                throw new ArgumentException("Muitos caracteres", "Lottus Store");
            }
            if (dto.Login.Length > 20)
            {
                throw new ArgumentException("Muitos caracteres", "Lottus Store");
            }
            if (dto.Senha.Length > 20)
            {
                throw new ArgumentException("Muitos caracteres", "Lottus Store");
            }
            if (dto.SalarioBruto > 30000)
            {
                throw new ArgumentException("Muitos caracteres", "Lottus Store");
            }
            if (dto.DataNascimento.Year < 1999)
            {
                throw new ArgumentException("Funcionário não tem idade para trabalhar", "Lottus Store");
            }


            FuncionarioDatabase db = new FuncionarioDatabase();
            return db.Salvar(dto);
        }
        public FuncionarioDTO ConsultarLoginPeloUsuario(string usuario)
        {
            FuncionarioDatabase db = new FuncionarioDatabase();
            return db.ConsultarLoginPeloUsuario(usuario);
        }


        public void Remover(int id)
        {
            FuncionarioDatabase db = new FuncionarioDatabase();
            db.Remover(id);
        }


        public List<FuncionarioDTO> Consultar(string nome)
        {
            FuncionarioDatabase pedidoDatabase = new FuncionarioDatabase();
            return pedidoDatabase.Consultar(nome);
        }
        public FuncionarioDTO Logar(string Login, string Senha)
        {
            if (Login == string.Empty)
            {
                throw new ArgumentException("Usuário é obrigatório");
            }
            if (Senha == string.Empty)
            {
                throw new ArgumentException("Senha é obrigatória");
            }
            FuncionarioDatabase db = new FuncionarioDatabase();

            return db.Logar(Login, Senha);

        }

        public List<FuncionarioDTO> ConsultarLogin(string nome)
        {
            FuncionarioDatabase db = new FuncionarioDatabase();
            return db.ConsultarLogin(nome);
        }

        public List<FuncionarioDTO> Listar()
        {
            FuncionarioDatabase db = new FuncionarioDatabase();
            return db.Listar();
        }
        public void Alterar(FuncionarioDTO dto)
        {
            FuncionarioDatabase db = new FuncionarioDatabase();
            db.Alterar(dto);
        }
    }
}
