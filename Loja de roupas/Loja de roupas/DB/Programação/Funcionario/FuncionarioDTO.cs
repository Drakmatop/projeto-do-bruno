﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Loja_de_roupas.DB.Funcionario
{
    public class FuncionarioDTO
    {
        public int Id{ get; set; }

        public string Nome{ get; set; }

        public string Sobrenome { get; set; }

        public string Cep{ get; set; }

        public string Telefone { get; set; }

        public DateTime DataNascimento { get; set; }

        public string Login{ get; set; }

        public string Senha{ get; set; }

        public decimal SalarioBruto { get; set; }

        public string Email { get; set; }

        public bool PermissaoAdm{ get; set; }

        public bool PermissaoVendedor { get; set; }



    }
}
