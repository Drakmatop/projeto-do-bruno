﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Loja_de_roupas.DB.Funcionario
{
    class FuncionarioDatabase
    {
        public int Salvar(FuncionarioDTO dto)
        {
            string script =
                @"INSERT INTO tb_funcionario
                (nm_funcionario, nm_sobrenome, ds_cep, ds_telefone,dt_nascimento,ds_login,ds_senha,bt_permissaoadm,bt_permissaovendedor, nr_salariobruto, ds_email)
                VALUES
                (@nm_funcionario, @nm_sobrenome, @ds_cep, @ds_telefone,@dt_nascimento,@ds_login,@ds_senha,@bt_permissaoadm,@bt_permissaovendedor, @nr_salariobruto, @ds_email)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_funcionario", dto.Nome));
            parms.Add(new MySqlParameter("nm_sobrenome", dto.Sobrenome));
            parms.Add(new MySqlParameter("ds_cep", dto.Cep));
            parms.Add(new MySqlParameter("ds_telefone", dto.Telefone));
            parms.Add(new MySqlParameter("dt_nascimento", dto.DataNascimento));
            parms.Add(new MySqlParameter("ds_login", dto.Login));
            parms.Add(new MySqlParameter("ds_senha", dto.Senha));
            parms.Add(new MySqlParameter("bt_permissaoadm", dto.PermissaoAdm));
            parms.Add(new MySqlParameter("bt_permissaovendedor", dto.PermissaoVendedor));
            parms.Add(new MySqlParameter("nr_salariobruto", dto.SalarioBruto));
            parms.Add(new MySqlParameter("ds_email", dto.Email));



            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parms);
        }

        public List<FuncionarioDTO> Consultar(string Nome)
        {
            string script = @"Select * FROM tb_funcionario WHERE nm_funcionario like @nm_funcionario";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_funcionario", Nome + "%"));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<FuncionarioDTO> lista = new List<FuncionarioDTO>();
            while (reader.Read())
            {
                FuncionarioDTO dto = new FuncionarioDTO();
                dto.Id = reader.GetInt32("id_funcionario");
                dto.Nome = reader.GetString("nm_funcionario");
                dto.Sobrenome = reader.GetString("nm_sobrenome");
                dto.Cep = reader.GetString("ds_cep");
                dto.Telefone = reader.GetString("ds_telefone");
                dto.DataNascimento = reader.GetDateTime("dt_nascimento");
                dto.Login = reader.GetString("ds_login");
                dto.Senha = reader.GetString("ds_senha");
                dto.PermissaoAdm = reader.GetBoolean("bt_permissaoadm");
                dto.PermissaoVendedor = reader.GetBoolean("bt_permissaoVendedor");
                dto.SalarioBruto = reader.GetDecimal("nr_salariobruto");
                dto.Email = reader.GetString("ds_email");


                lista.Add(dto);
            }
            reader.Close();

            return lista;
        }

        public List<FuncionarioDTO> Listar()
        {
            string script = @"Select * FROM tb_funcionario";

            List<MySqlParameter> parms = new List<MySqlParameter>();

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<FuncionarioDTO> lista = new List<FuncionarioDTO>();
            while (reader.Read())
            {
                FuncionarioDTO dto = new FuncionarioDTO();
                dto.Id = reader.GetInt32("id_funcionario");
                dto.Nome = reader.GetString("nm_funcionario");
                dto.Sobrenome = reader.GetString("nm_sobrenome");
                dto.Cep = reader.GetString("ds_cep");
                dto.Telefone = reader.GetString("ds_telefone");
                dto.DataNascimento = reader.GetDateTime("dt_nascimento");
                dto.Login = reader.GetString("ds_login");
                dto.Senha = reader.GetString("ds_senha");
                dto.PermissaoAdm = reader.GetBoolean("bt_permissaoadm");
                dto.PermissaoVendedor = reader.GetBoolean("bt_permissaoVendedor");
                dto.SalarioBruto = reader.GetDecimal("nr_salariobruto");
                dto.Email = reader.GetString("ds_email");

                lista.Add(dto);
            }
            reader.Close();

            return lista;
        }


        public void Remover(int id)
        {
            string script = @"DELETE FROM tb_funcionario WHERE id_funcionario = @id_funcionario";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_funcionario", id));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }
 
 
        public FuncionarioDTO Logar(string Usuário, string Senha)
        {
            string script = @"Select * FROM tb_funcionario WHERE ds_login = @ds_login
                        AND ds_senha = @ds_senha";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("ds_login", Usuário));
            parms.Add(new MySqlParameter("ds_senha", Senha));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            FuncionarioDTO funcionario = null;
            if(reader.Read())
            {
                funcionario = new FuncionarioDTO();
                funcionario.Id = reader.GetInt32("id_funcionario");
                funcionario.Nome = reader.GetString("nm_funcionario");
                funcionario.Sobrenome = reader.GetString("nm_sobrenome");
                funcionario.Cep = reader.GetString("ds_cep");
                funcionario.Telefone = reader.GetString("ds_telefone");
                funcionario.DataNascimento = reader.GetDateTime("dt_nascimento");
                funcionario.Login = reader.GetString("ds_login");
                funcionario.Senha = reader.GetString("ds_senha");
                funcionario.PermissaoAdm = reader.GetBoolean("bt_permissaoadm");
                funcionario.PermissaoVendedor = reader.GetBoolean("bt_permissaoVendedor");
                funcionario.SalarioBruto = reader.GetDecimal("nr_salariobruto");
                funcionario.Email = reader.GetString("ds_email");

            }
            reader.Close();

            return funcionario;
        }
        public List<FuncionarioDTO> ConsultarLogin(string Nome)
        {
            string script = @" SELECT nm_funcionario,ds_login, ds_senha,
            bt_permissaoadm,bt_permissaovendedor
            FROM tb_funcionario where
              nm_funcionario like @nm_funcionario ";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_funcionario", Nome + "%"));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);
            List<FuncionarioDTO> func = new List<FuncionarioDTO>(); ;

            while(reader.Read())
            {
                FuncionarioDTO funcionario = new FuncionarioDTO();
                funcionario.Nome = reader.GetString("nm_funcionario");
                funcionario.Login = reader.GetString("ds_login");
                funcionario.Senha = reader.GetString("ds_senha");
                funcionario.PermissaoAdm = reader.GetBoolean("bt_permissaoadm");
                funcionario.PermissaoVendedor = reader.GetBoolean("bt_permissaoVendedor");

                func.Add(funcionario);
            }
            reader.Close();

            return func;


        }
        public FuncionarioDTO ConsultarLoginPeloUsuario(string usuario)
        {
            string script = @" SELECT nm_funcionario,ds_login, ds_senha,
            bt_permissaoadm,bt_permissaovendedor, ds_email
            FROM tb_funcionario where
              ds_login = @ds_login";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("ds_login", usuario));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);
            FuncionarioDTO funcionario = null;
            if(reader.Read())
            {
                funcionario = new FuncionarioDTO();
                funcionario.Nome = reader.GetString("nm_funcionario");
                funcionario.Login = reader.GetString("ds_login");
                funcionario.Senha = reader.GetString("ds_senha");
                funcionario.PermissaoAdm = reader.GetBoolean("bt_permissaoadm");
                funcionario.PermissaoVendedor = reader.GetBoolean("bt_permissaoVendedor");
                funcionario.Email = reader.GetString("ds_email");
            }
            reader.Close();

            return funcionario;

        }
        public void Alterar(FuncionarioDTO dto)
        {
            string script =
                @"UPDATE tb_funcionario set
                nm_funcionario = @nm_funcionario, nm_sobrenome = @nm_sobrenome, ds_cep = @ds_cep, ds_telefone = @ds_telefone, dt_nascimento = @dt_nascimento,
                ds_login = @ds_login,ds_senha = @ds_senha,bt_permissaoadm = @bt_permissaoadm, bt_permissaovendedor = @bt_permissaovendedor, nr_salariobruto = @nr_salariobruto, ds_email = @ds_email
                WHERE id_funcionario = @id_funcionario";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_funcionario", dto.Nome));
            parms.Add(new MySqlParameter("nm_sobrenome", dto.Sobrenome));
            parms.Add(new MySqlParameter("ds_cep", dto.Cep));
            parms.Add(new MySqlParameter("ds_telefone", dto.Telefone));
            parms.Add(new MySqlParameter("dt_nascimento", dto.DataNascimento));
            parms.Add(new MySqlParameter("ds_login", dto.Login));
            parms.Add(new MySqlParameter("ds_senha", dto.Senha));
            parms.Add(new MySqlParameter("bt_permissaoadm", dto.PermissaoAdm));
            parms.Add(new MySqlParameter("bt_permissaovendedor", dto.PermissaoVendedor));
            parms.Add(new MySqlParameter("nr_salariobruto", dto.SalarioBruto));
            parms.Add(new MySqlParameter("ds_email", dto.Email));
            parms.Add(new MySqlParameter("id_funcionario", dto.Id));



            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }




    }
}
