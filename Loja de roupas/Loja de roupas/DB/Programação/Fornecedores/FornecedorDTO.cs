﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Loja_de_roupas.DB.Fornecedor
{
    public class FornecedorDTO
    {
        public int Id{ get; set; }

        public string Nome{ get; set; }

        public string Telefone { get; set; }

        public string Email{ get; set; }

        public string CEP { get; set; }

    }
}
