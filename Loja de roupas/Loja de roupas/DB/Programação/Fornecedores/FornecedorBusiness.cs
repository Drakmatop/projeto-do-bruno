﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Loja_de_roupas.DB.Fornecedor
{
    class FornecedorBusiness
    {
        FornecedorDatabase db = new FornecedorDatabase();
        public int Salvar(FornecedorDTO dto)
        {
            if (dto.Nome == string.Empty)
            {
                throw new ArgumentException("Nome é obrigatório", "Lottus Store");
            }
            if (dto.Telefone == string.Empty)
            {
                throw new ArgumentException("Telefone é obrigatório", "Lottus Store");
            }
            if (dto.Email == string.Empty)
            {
                throw new ArgumentException("E-mail é obrigatório", "Lottus Store");
            }
            if (dto.CEP == string.Empty)
            {
                throw new ArgumentException("Cep é obrigatório", "Lottus Store");
            }
            if (dto.Nome.Length > 30)
            {
                throw new ArgumentException("Muitos Caracteres", "Lottus Store");
            }
            if (dto.Telefone.Length > 15)
            {
                throw new ArgumentException("Muitos Caracteres", "Lottus Store");
            }
            if (dto.Email.Length == 50)
            {
                throw new ArgumentException("Muitos Caracteres", "Lottus Store");
            }
            if (dto.CEP.Length > 15)
            {
                throw new ArgumentException("Muitos Caracteres", "Lottus Store");
            }

            return db.Salvar(dto);
        }

        public void Remover(int id)
        {
            db.Remover(id);
        }

        public List<FornecedorDTO> Consultar(string Nome)
        {
            if (Nome == string.Empty)
            {
                Nome = "";
            }

            return db.Consultar(Nome);
        }
        public void Alterar(FornecedorDTO dto)
        {
            db.Alterar(dto);
        }
    }
}
