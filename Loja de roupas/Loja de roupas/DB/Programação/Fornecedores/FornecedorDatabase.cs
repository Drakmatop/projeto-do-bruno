﻿using Loja_de_roupas.DB.Base;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Loja_de_roupas.DB.Fornecedor
{
    class FornecedorDatabase
    {
        public int Salvar(FornecedorDTO dto)
        {
            string script =
                @"INSERT INTO tb_fornecedor
                (nm_fornecedor, ds_telefone, ds_email, ds_cep)
                VALUES
                (@nm_fornecedor, @ds_telefone, @ds_email, @ds_cep)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_fornecedor", dto.Nome));
            parms.Add(new MySqlParameter("ds_telefone", dto.Telefone));
            parms.Add(new MySqlParameter("ds_email", dto.Email));
            parms.Add(new MySqlParameter("ds_cep", dto.CEP));

            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parms);
        }
        public void Alterar(FornecedorDTO dto)
        {
            string script = @"UPDATE tb_fornecedor SET 
                        nm_fornecedor = @nm_fornecedor, 
                        ds_telefone = @ds_telefone, 
                        ds_email = @ds_email, 
                        ds_cep = @ds_cep
                        WHERE id_fornecedor = @id_fornecedor";


            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_fornecedor", dto.Id));
            parms.Add(new MySqlParameter("nm_fornecedor", dto.Nome));
            parms.Add(new MySqlParameter("ds_telefone", dto.Telefone));
            parms.Add(new MySqlParameter("ds_email", dto.Email));
            parms.Add(new MySqlParameter("ds_cep", dto.CEP));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }

        public void Remover(int id)
        {
            string script = @"DELETE FROM tb_fornecedor WHERE id_fornecedor = @id_fornecedor";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_fornecedor", id));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }

        public List<FornecedorDTO> Consultar(string Nome)
        {
            string script = @"SELECT * FROM tb_fornecedor WHERE nm_fornecedor like @nm_fornecedor";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_fornecedor", "%" + Nome + "%"));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<FornecedorDTO> lista = new List<FornecedorDTO>();
            while (reader.Read())
            {
                FornecedorDTO dto = new FornecedorDTO();
                dto.Id = reader.GetInt32("id_fornecedor");
                dto.Nome = reader.GetString("nm_fornecedor");
                dto.Telefone = reader.GetString("ds_telefone");
                dto.Email = reader.GetString("ds_email");
                dto.CEP = reader.GetString("ds_cep");


                lista.Add(dto);
            }
            reader.Close();

            return lista;

        }
    }
}
