﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Loja_de_roupas.DB
{
    class VwConsultarFluxodeCaixa
    {
        public DateTime Data { get; set; }

        public decimal Despesas { get; set; }

        public decimal Ganhos { get; set; }

        public decimal Lucro { get; set; }
    }
}
