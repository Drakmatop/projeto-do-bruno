﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Loja_de_roupas.DB.Fluxo_de_Caixa
{
    class FluxodeCaixaDatabase
    {
        public List<VwConsultarFluxodeCaixa> Consultar(DateTime Inicio, DateTime Fim)
        {
            string script = @"select * from vw_consultar_fluxodecaixa
            WHERE dt_referencia >= @dt_inicio
                AND dt_referencia <= @dt_fim;";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("dt_inicio", Inicio));
            parms.Add(new MySqlParameter("dt_fim", Fim));


            Database db = new Database();

            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<VwConsultarFluxodeCaixa> lista = new List<VwConsultarFluxodeCaixa>();

            while (reader.Read())
            {
                VwConsultarFluxodeCaixa dto = new VwConsultarFluxodeCaixa();

                dto.Data = reader.GetDateTime("dt_referencia");
                dto.Despesas = reader.GetDecimal("vl_total_despesas");
                dto.Ganhos = reader.GetDecimal("vl_total_ganhos");
                dto.Lucro = reader.GetDecimal("vl_saldo");

                lista.Add(dto);
            }
            reader.Close();

            return lista;

        }
    }
}
