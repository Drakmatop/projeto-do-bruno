﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Loja_de_roupas.DB.Produto
{
    public class ProdutoDTO
    {
        public int Id{ get; set; }

        public string Nome { get; set; }

        public decimal PrecoVenda{ get; set; }

        public string Tamanho{ get; set; }

        public string Cor{ get; set; }

        public decimal PrecoCompra { get; set; }

        public string CodigodeBarras { get; set; }

        public int CategoriaId { get; set; }
        public int FornecedorId { get; set; }
        


    }
}
