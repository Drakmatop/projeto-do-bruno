﻿using Loja_de_roupas.DB.Base.Categoria;
using Loja_de_roupas.DB.Fornecedor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Loja_de_roupas.DB.Produto
{
    class ProdutoBusiness_
    {
        public int Salvar(ProdutoDTO item)
        {

            if (item.Nome == string.Empty)
            {
                throw new ArgumentException("Produto é obrigatório", "Lottus Store");
            }
            if (item.PrecoCompra == 0)
            {
                throw new ArgumentException("Preço de compra é obrigatório", "Lottus Store");
            }
            if (item.PrecoVenda == 0)
            {
                throw new ArgumentException("Preço de venda é obrigatório", "Lottus Store");
            }
            if (item.Tamanho == string.Empty)
            {
                throw new ArgumentException("Tamanho é obrigatório", "Lottus Store");
            }
            if (item.Cor == string.Empty)
            {
                throw new ArgumentException("Cor é obrigatório", "Lottus Store");
            }
            if (item.FornecedorId == 0)
            {
                throw new ArgumentException("É obrigatório escolher o fornecedor", "Lottus Store");
            }
            if (item.CategoriaId == 0)
            {
                throw new ArgumentException("É obrigatório escolher o categoria", "Lottus Store");
            }
            if (item.CodigodeBarras == string.Empty)
            {
                throw new ArgumentException("Código de barras é obrigatório", "Lottus Store");
            }


            if (item.Nome.Length > 15)
            {
                throw new ArgumentException("Nome do produto tem muitos caracteres", "Lottus Store");
            }
            if (item.PrecoCompra > 100000)
            {
                throw new ArgumentException("Preço de compra é muito alto", "Lottus Store");
            }
            if (item.PrecoVenda == 300000)
            {
                throw new ArgumentException("Preço de venda é muito alto", "Lottus Store");
            }
            if (item.Tamanho.Length > 15)
            {
                throw new ArgumentException("Tamanho tem muitos caracteres", "Lottus Store");
            }
            if (item.Cor.Length > 15)
            {
                throw new ArgumentException("Cor tem muitos caracteres", "Lottus Store");
            }
            if (item.CodigodeBarras.Length < 12)
            {
                throw new ArgumentException("Código de barras precisa ter 12 números", "Lottus Store");
            }
            ProdutoDatabase produtoDatabase = new ProdutoDatabase();
            int idProduto = produtoDatabase.Salvar(item);
            return idProduto;


        }

        public void Remover(int id)
        {
            ProdutoDatabase db = new ProdutoDatabase();
            db.Remover(id);
        }

        public List<ProdutoDTO> Consultar(string produto)
        {
            if (produto == string.Empty)
            {
                produto = string.Empty;
            }
            ProdutoDatabase db = new ProdutoDatabase();
            return db.Consultar(produto);
        }

        public List<ProdutoDTO> Listar()
        {
            ProdutoDatabase db = new ProdutoDatabase();
            return db.Listar();
        }

        public void Alterar(ProdutoDTO dto)
        {
            ProdutoDatabase db = new ProdutoDatabase();
            db.Alterar(dto);
        }
    }
}
