﻿using Loja_de_roupas.DB.Base;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Loja_de_roupas.DB.Produto
{
    class ProdutoDatabase
    {
        public int Salvar(ProdutoDTO dto)
        {
            string script = @"INSERT INTO tb_produto (nm_produto, vl_precovenda, vl_precocompra, ds_tamanho, ds_cor,id_categoria,id_fornecedor, ds_codigodebarras)
                            VALUES (@nm_produto, @vl_precovenda, @vl_precocompra, @ds_tamanho, @ds_cor,@id_categoria,@id_fornecedor, @ds_codigodebarras)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_produto", dto.Nome));
            parms.Add(new MySqlParameter("vl_precovenda", dto.PrecoVenda));
            parms.Add(new MySqlParameter("ds_tamanho", dto.Tamanho));
            parms.Add(new MySqlParameter("ds_cor", dto.Cor));
            parms.Add(new MySqlParameter("vl_precocompra", dto.PrecoCompra));
            parms.Add(new MySqlParameter("id_fornecedor", dto.FornecedorId));
            parms.Add(new MySqlParameter("id_categoria", dto.CategoriaId));
            parms.Add(new MySqlParameter("ds_codigodebarras", dto.CodigodeBarras));

            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parms);
        }

        public void Remover(int id)
        {
            string script = @"DELETE FROM tb_produto WHERE id_roupa = @id_roupa";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_roupa", id));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }

        public List<ProdutoDTO> Consultar(string produto)
        {
            string script = @"SELECT * FROM tb_produto WHERE nm_produto like @nm_produto";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_produto","%" + produto + "%"));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<ProdutoDTO> lista = new List<ProdutoDTO>();
            while (reader.Read())
            {
                ProdutoDTO dto = new ProdutoDTO();
                dto.Id = reader.GetInt32("id_roupa");
                dto.Nome = reader.GetString("nm_produto");
                dto.PrecoVenda = reader.GetDecimal("vl_precovenda");
                dto.PrecoCompra = reader.GetDecimal("vl_precocompra");
                dto.Tamanho = reader.GetString("ds_tamanho");
                dto.Cor = reader.GetString("ds_cor");
                dto.CategoriaId = reader.GetInt32("id_categoria");
                dto.FornecedorId = reader.GetInt32("id_fornecedor");
                dto.CodigodeBarras = reader.GetString("ds_codigodebarras");

                lista.Add(dto);
            }
            reader.Close();

            return lista;
        }
        public void Alterar(ProdutoDTO dto)
        {
            string script = @"UPDATE tb_produto SET 
                        nm_produto = @nm_produto, 
                        vl_precovenda = @vl_precovenda, 
                        vl_precocompra = @vl_precocompra, 
                        ds_tamanho = @ds_tamanho,
                        ds_cor = @ds_cor, 
                        id_categoria = @id_categoria, 
                        id_fornecedor = @id_fornecedor,
                        ds_codigodebarras = ds_codigodebarras
                        WHERE id_roupa = @id_roupa";


            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_roupa", dto.Id));
            parms.Add(new MySqlParameter("nm_produto", dto.Nome));
            parms.Add(new MySqlParameter("vl_precovenda", dto.PrecoVenda));
            parms.Add(new MySqlParameter("ds_tamanho", dto.Tamanho));
            parms.Add(new MySqlParameter("ds_cor", dto.Cor));
            parms.Add(new MySqlParameter("vl_precocompra", dto.PrecoCompra));
            parms.Add(new MySqlParameter("id_fornecedor", dto.FornecedorId));
            parms.Add(new MySqlParameter("id_categoria", dto.CategoriaId));
            parms.Add(new MySqlParameter("ds_codigodebarras", dto.CodigodeBarras));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }
        public List<ProdutoDTO> Listar()
        {
            string script = @"SELECT * FROM tb_produto";

            List<MySqlParameter> parms = new List<MySqlParameter>();

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<ProdutoDTO> lista = new List<ProdutoDTO>();
            while (reader.Read())
            {
                ProdutoDTO dto = new ProdutoDTO();
                dto.Id = reader.GetInt32("id_roupa");
                dto.Nome = reader.GetString("nm_produto");
                dto.PrecoVenda = reader.GetDecimal("vl_precovenda");
                dto.PrecoCompra = reader.GetDecimal("vl_precocompra");
                dto.Tamanho = reader.GetString("ds_tamanho");
                dto.Cor = reader.GetString("ds_cor");
                dto.CategoriaId = reader.GetInt32("id_categoria");
                dto.FornecedorId = reader.GetInt32("id_fornecedor");
                dto.CodigodeBarras = reader.GetString("ds_codigodebarras");

                lista.Add(dto);
            }
            reader.Close();

            return lista;
        }
    }
}
