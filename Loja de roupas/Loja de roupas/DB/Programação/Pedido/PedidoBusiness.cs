﻿using Loja_de_roupas.DB.Produto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Loja_de_roupas.DB.Pedido
{
    class PedidoBusiness
    {
        public int Salvar(PedidoDTO pedido, List<ProdutoDTO> produtos)
        {
            if (pedido.FormaPagamento == string.Empty)
            {
                throw new ArgumentException("FormaPagamento é obrigatório", "Lottus Store");
            }
            if (pedido.Data > DateTime.Now)
            {
                throw new ArgumentException("Data é Maior do que hoje", "Lottus Store");
            }
            if (pedido.FuncionarioId == 0)
            {
                throw new ArgumentException(" Funcionario é obrigatório", "Lottus Store");
            }
            if (pedido.ClienteId == 0)
            {
                throw new ArgumentException("Cliente é obrigatório", "Lottus Store");
            }


            if (pedido.FormaPagamento.Length > 25)
            {
                throw new ArgumentException("Muitos caracteres", "Lottus Store");
            }
            foreach (ProdutoDTO item in produtos)
            {
                if (item.Nome == string.Empty)
                {
                    throw new ArgumentException("Produto é obrigatório", "Lottus Store");
                }
                if (item.PrecoCompra == 0)
                {
                    throw new ArgumentException("Preço de compra é obrigatório", "Lottus Store");
                }
                if (item.PrecoVenda == 0)
                {
                    throw new ArgumentException("Preço de venda é obrigatório", "Lottus Store");
                }
                if (item.Tamanho == string.Empty)
                {
                    throw new ArgumentException("Tamanho é obrigatório", "Lottus Store");
                }
                if (item.Cor == string.Empty)
                {
                    throw new ArgumentException("Cor é obrigatório", "Lottus Store");
                }
                if (item.FornecedorId == 0)
                {
                    throw new ArgumentException("É obrigatório escolher o fornecedor", "Lottus Store");
                }
                if (item.CategoriaId == 0)
                {
                    throw new ArgumentException("É obrigatório escolher o categoria", "Lottus Store");
                }



                if (item.Nome.Length > 15)
                {
                    throw new ArgumentException("Nome do produto tem muitos caracteres", "Lottus Store");
                }
                if (item.PrecoCompra > 100000)
                {
                    throw new ArgumentException("Preço de compra é muito alto", "Lottus Store");
                }
                if (item.PrecoVenda == 300000)
                {
                    throw new ArgumentException("Preço de venda é muito alto", "Lottus Store");
                }
                if (item.Tamanho.Length > 15)
                {
                    throw new ArgumentException("Tamanho tem muitos caracteres", "Lottus Store");
                }
                if (item.Cor.Length > 15)
                {
                    throw new ArgumentException("Cor tem muitos caracteres", "Lottus Store");
                }
            }

            PedidoDatabase pedidoDatabase = new PedidoDatabase();
            int idPedido = pedidoDatabase.Salvar(pedido);

            PedidoItemBusiness itemBusiness = new PedidoItemBusiness();
            foreach (ProdutoDTO item in produtos)
            {
                PedidoItemDTO itemDto = new PedidoItemDTO();
                itemDto.IdPedido = idPedido;
                itemDto.IdProduto = item.Id;

                itemBusiness.Salvar(itemDto);
            }

            return idPedido;
        }

        public List<PedidoConsultarView> Consultar(string cliente)
        {
            PedidoDatabase pedidoDatabase = new PedidoDatabase();
            return pedidoDatabase.Consultar(cliente);
        }

        public void Remover(int id)
        {
            PedidoDatabase db = new PedidoDatabase();
            db.Remover(id);
        }

        public List<PedidoConsultarView> ConsultarPorId(int id)
        {
            PedidoDatabase db = new PedidoDatabase();
            return db.ConsultarPorId(id);
        }

        }
}
