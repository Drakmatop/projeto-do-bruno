﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Loja_de_roupas.DB.Pedido
{
    class PedidoItemBusiness
    {
        public int Salvar(PedidoItemDTO dto)
        {
            if (dto.IdPedido == 0)
            {
                throw new ArgumentException("Pedido não foi reconhecido", "Lottus Store");
            }
            if (dto.IdProduto == 0)
            {
                throw new ArgumentException("Produto não foi reconhecido", "Lottus Store");
            }
            PedidoItemDatabase db = new PedidoItemDatabase();
            return db.Salvar(dto);
        }


        public List<PedidoItemDTO> ConsultarPorPedido(int idPedido)
        {
            PedidoItemDatabase db = new PedidoItemDatabase();
            return db.ConsultarPorPedido(idPedido);
        }

        public void Remover(int id)
        {
            PedidoItemDatabase db = new PedidoItemDatabase();
            db.Remover(id);
        }
    }
}
