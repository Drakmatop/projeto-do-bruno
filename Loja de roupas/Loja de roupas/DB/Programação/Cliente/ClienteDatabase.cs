﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Loja_de_roupas.DB.Base.Clientes
{
    class ClienteDatabase
    {
        public int Salvar(ClienteDTO dto)
        {
            string script = @"INSERT INTO tb_cliente (nm_cliente, nm_Sobrenome, ds_CEP, ds_CPF,ds_Telefone,nr_numero, ds_Complemento)
                            VALUES (@nm_cliente, @nm_Sobrenome, @ds_CEP, @ds_CPF,@ds_Telefone,@nr_numero,@ds_Complemento)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_cliente", dto.Nome));
            parms.Add(new MySqlParameter("nm_Sobrenome", dto.Sobrenome));
            parms.Add(new MySqlParameter("ds_CEP", dto.CEP));
            parms.Add(new MySqlParameter("ds_CPF", dto.CPF));
            parms.Add(new MySqlParameter("ds_Telefone", dto.Telefone));
            parms.Add(new MySqlParameter("nr_Numero", dto.Numero));
            parms.Add(new MySqlParameter("ds_Complemento", dto.Complemento));

            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parms);
        }
        public void Alterar(ClienteDTO dto)
        {
            string script = @"UPDATE tb_gasto SET 
                        nm_cliente = @nm_cliente, 
                        nm_Sobrenome = @nm_Sobrenome, 
                        ds_CEP = @ds_CEP, 
                        ds_CPF = @ds_CPF,                    
                        ds_Telefone = @ds_Telefone, 
                        nr_Numero = @nr_Numero, 
                        ds_Complemento = @ds_Complemento,
                        WHERE id_cliente = @id_cliente";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_cliente", dto.Id));
            parms.Add(new MySqlParameter("nm_cliente", dto.Nome));
            parms.Add(new MySqlParameter("nm_Sobrenome", dto.Sobrenome));
            parms.Add(new MySqlParameter("ds_CEP", dto.CEP));
            parms.Add(new MySqlParameter("ds_CPF", dto.CPF));
            parms.Add(new MySqlParameter("ds_Telefone", dto.Telefone));
            parms.Add(new MySqlParameter("nr_Numero", dto.Numero));
            parms.Add(new MySqlParameter("ds_Complemento", dto.Complemento));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }
        public void Remover(int id)
        {
            string script = @"DELETE FROM tb_cliente WHERE id_cliente = @id_cliente";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_cliente", id));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }

        public List<ClienteDTO> Consultar(string Nome)
        {
            string script = @"SELECT * FROM tb_cliente WHERE nm_cliente like @nm_cliente";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_cliente", "%" + Nome + "%"));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<ClienteDTO> lista = new List<ClienteDTO>();
            while (reader.Read())
            {
                ClienteDTO dto = new ClienteDTO();
                dto.Id = reader.GetInt32("id_cliente");
                dto.Nome = reader.GetString("nm_cliente");
                dto.Sobrenome = reader.GetString("nm_Sobrenome");
                dto.CEP = reader.GetString("ds_CEP");
                dto.CPF = reader.GetString("ds_CPF");
                dto.Telefone = reader.GetString("ds_Telefone");
                dto.Numero = reader.GetInt32("nr_numero");
                dto.Complemento = reader.GetString("ds_Complemento");

                lista.Add(dto);
            }
            reader.Close();

            return lista;
        }

        public List<ClienteDTO> Listar()
        {
            string script = @"SELECT * FROM tb_cliente";

            List<MySqlParameter> parms = new List<MySqlParameter>();

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<ClienteDTO> lista = new List<ClienteDTO>();
            while (reader.Read())
            {
                ClienteDTO dto = new ClienteDTO();
                dto.Id = reader.GetInt32("id_cliente");
                dto.Nome = reader.GetString("nm_cliente");
                dto.Sobrenome = reader.GetString("nm_Sobrenome");
                dto.CEP = reader.GetString("ds_CEP");
                dto.CPF = reader.GetString("ds_CPF");
                dto.Telefone = reader.GetString("ds_Telefone");
                dto.Numero = reader.GetInt32("nr_numero");
                dto.Complemento = reader.GetString("ds_Complemento");

                lista.Add(dto);
            }
            reader.Close();

            return lista;
        }
    }
}
