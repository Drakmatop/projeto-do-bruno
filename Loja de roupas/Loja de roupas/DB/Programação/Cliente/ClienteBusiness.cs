﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Loja_de_roupas.DB.Base.Clientes
{
    class ClienteBusiness
    {
        public int Salvar(ClienteDTO dto)
        {
            if (dto.Nome == string.Empty)
            {
                throw new ArgumentException("Nome da roupa é obrigatório", "Lottus Store");
            }
            if (dto.Complemento == string.Empty)
            {
                throw new ArgumentException("Complemento é obrigatório", "Lottus Store");
            }
            if (dto.CPF == string.Empty)
            {
                throw new ArgumentException("CPF é obrigatório", "Lottus Store");
            }
            if (dto.Sobrenome == string.Empty)
            {
                throw new ArgumentException("Sobrenome é obrigatório", "Lottus Store");
            }
            if (dto.Telefone == string.Empty)
            {
                throw new ArgumentException("Telefone é obrigatório", "Lottus Store");
            }
            if (dto.Numero == 0)
            {
                throw new ArgumentException("Número é obrigatório", "Lottus Store");
            }
            if (dto.CEP == string.Empty)
            {
                throw new ArgumentException("CEP é obrigatório", "Lottus Store");
            }



            if (dto.Nome.Length > 25)
            {
                throw new ArgumentException("Nome da roupa tem muitos caracteres", "Lottus Store");
            }
            if (dto.Complemento.Length > 30)
            {
                throw new ArgumentException("Complemento tem muitos caracteres", "Lottus Store");
            }
            if (dto.CPF.Length > 20)
            {
                throw new ArgumentException("CPF tem muitos caracteres", "Lottus Store");
            }
            if (dto.Sobrenome.Length > 25)
            {
                throw new ArgumentException("Sobrenome tem muitos caracteres", "Lottus Store");
            }
            if (dto.Telefone.Length > 20)
            {
                throw new ArgumentException("Telefone tem muitos caracteres", "Lottus Store");
            }
            if (dto.CEP.Length > 20)
            {
                throw new ArgumentException("CEP tem muitos caracteres", "Lottus Store");
            }



            ClienteDatabase db = new ClienteDatabase();
            return db.Salvar(dto);
        }
        public void Alterar(ClienteDTO dto)
        {
            ClienteDatabase db = new ClienteDatabase();
            db.Alterar(dto);
        }

        public void Remover(int id)
        {
            ClienteDatabase db = new ClienteDatabase();
            db.Remover(id);
        }

        public List<ClienteDTO> Consultar(string Cliente)
        {
            if (Cliente == string.Empty)
            {
                Cliente = "";
            }
            ClienteDatabase db = new ClienteDatabase();
            return db.Consultar(Cliente);
        }

        public List<ClienteDTO> Listar()
        {
            ClienteDatabase db = new ClienteDatabase();
            return db.Listar();
        }
    }
}
