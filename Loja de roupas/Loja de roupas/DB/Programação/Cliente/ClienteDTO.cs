﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Loja_de_roupas.DB.Base.Clientes
{
    public class ClienteDTO
    {
        public int Id{ get; set; }

        public string Nome { get; set; }

        public string Sobrenome { get; set; }

        public string  CPF { get; set; }

        public string CEP { get; set; }

        public string Telefone { get; set; }

        public int Numero { get; set; }

        public string Complemento { get; set; }
    }

}
