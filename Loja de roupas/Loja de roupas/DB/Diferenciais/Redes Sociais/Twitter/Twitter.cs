﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tweetinvi;
using Tweetinvi.Models;

namespace nsf._2018.diferenciais.RedesSociais
{
    class Twitter
    {
        string CONSUMER_KEY    = "OU6ymM5DSgH9RLRuAzjjfLzDs";
        string CONSUMER_SECRET = "Z8zjWYlfsaaDR4YUhW5N52jkEu18lvWa3OLpCrFOShQIPmHTBL";
        string ACCESS_TOKEN    = "1056313660304486401-1IcAweysfFXM3NlOL3Cr7dKyHPkGuJ";
        string ACCESS_SECRET   = "DlC81NWtcssvV8Hmt6Xg7lkDOqp8bYbIVVyKCYUEhSp49";
        IAuthenticatedUser user;
        public Twitter()
        {
            // Atribui as credenciais. Para conseguir as credenciais, cadastra-se em https://apps.twitter.com
            Auth.SetUserCredentials(CONSUMER_KEY, CONSUMER_SECRET, ACCESS_TOKEN, ACCESS_SECRET);
             user = User.GetAuthenticatedUser();
        }

        public void Enviar(string mensagem)
        {
            Tweet.PublishTweet(mensagem);
        }

        public List<TweetResponse> Meustwitts()
        {
            List<ITweet> tweets = Timeline.GetUserTimeline(user).ToList();

            List<TweetResponse> tweetResponse = new List<TweetResponse>();
            foreach (ITweet item in tweets)
            {
                TweetResponse tweet = new TweetResponse();
                tweet.Id = item.Id;
                tweet.Usuario = item.CreatedBy.Name;
                tweet.Mensagem = item.FullText;
                tweet.Criacao = item.CreatedAt;

                tweetResponse.Add(tweet);
            }

            return tweetResponse;
        }

        public List<TweetResponse> TwittsGerais()
        {
            List<ITweet> tweets = Timeline.GetHomeTimeline().ToList();

            List<TweetResponse> tweetResponse = new List<TweetResponse>();
            foreach (ITweet item in tweets)
            {
                TweetResponse tweet = new TweetResponse();
                tweet.Id = item.Id;
                tweet.Usuario = item.CreatedBy.Name;
                tweet.Mensagem = item.FullText;
                tweet.Criacao = item.CreatedAt;

                tweetResponse.Add(tweet);
            }

            return tweetResponse;
        }
    }
}
