﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Loja_de_roupas.DB.Base.Categoria
{
    class CategoriaBusiness
    {
        public int Salvar(CategoriaDTO dto)
        {
            if (dto.Nome == string.Empty)
            {
                throw new ArgumentException("Nome é obrigatório", "Lottus Store");
            }


            CategoriaDatabase db = new CategoriaDatabase();
            return db.Salvar(dto);


        }

        public void Remover(int id)
        {
            CategoriaDatabase db = new CategoriaDatabase();
            db.Remover(id);
        }

        public List<CategoriaDTO> Listar()
        {
            CategoriaDatabase db = new CategoriaDatabase();
            return db.Listar();

        }
    }
}
