﻿using Loja_de_roupas.DB.Funcionario;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Loja_de_roupas
{
    public partial class Login : Form
    {
        public Login()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
            FuncionarioBusiness business = new FuncionarioBusiness();
            FuncionarioDTO Funcionario = business.Logar(txtUsuario.Text, txtSenha.Text);
            if (Funcionario != null)
            {
                UserSess.Usuariologado = Funcionario;
                Menu frm = new Menu();
                frm.Show();
                Hide();
            }
            else
            {
                MessageBox.Show("Credenciais inválidas.");
            }
            }
            catch (Exception ex)
            {

                MessageBox.Show("Ocorreu um erro" + ex.Message, "Lottus Store");
            }

        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            try
            {
            Menu tela = new Menu();
            tela.Show();
            this.Hide();
            }
            catch (Exception ex)
            {

                MessageBox.Show("Ocorreu um erro" + ex.Message, "Lottus Store");
            }

        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
            if (txtPermissao.Text == "adm")
            {
                Cadastrar_funcionario tela = new Cadastrar_funcionario();
                tela.Show();
            }
            }
            catch (Exception ex)
            {

                MessageBox.Show("Ocorreu um erro" + ex.Message, "Lottus Store");
            }

        }
    }
}
