﻿using Loja_de_roupas.DB.Funcionario;
using Loja_de_roupas.Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Loja_de_roupas
{
    public partial class Menu : Form
    {
        public Menu()
        {
            InitializeComponent();
            ValidarLogin();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            try
            {
            Consultar_Produto tela = new Consultar_Produto();
            tela.Show();
            this.Hide();
            }
            catch (Exception ex)
            {

                MessageBox.Show("Ocorreu um erro" + ex.Message, "Lottus Store");
            }

           
        }

        private void Menu_Load(object sender, EventArgs e)
        {

        }

        private void button13_Click(object sender, EventArgs e)
        {
            try
            {
            todosdados tela = new todosdados();
            tela.Show();
            this.Hide();
            }
            catch (Exception ex)
            {

                MessageBox.Show("Ocorreu um erro" + ex.Message, "Lottus Store");
            }

        }

        private void button17_Click(object sender, EventArgs e)
        {
            
        }

        private void button9_Click(object sender, EventArgs e)
        {
            try
            {
            Cadastro_de_fornecedor tela = new Cadastro_de_fornecedor();
            tela.Show();
            this.Hide();
            }
            catch (Exception ex)
            {

                MessageBox.Show("Ocorreu um erro" + ex.Message, "Lottus Store");
            }


        }

        private void button15_Click(object sender, EventArgs e)
        {
            try
            {
            Cartegoria tela = new Cartegoria();
            tela.Show();
            this.Hide();
            }
            catch (Exception ex)
            {

                MessageBox.Show("Ocorreu um erro" + ex.Message, "Lottus Store");
            }


        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
            Cadastrar_produto tela = new Cadastrar_produto();
            tela.Show();
            this.Hide();
            }
            catch (Exception ex)
            {

                MessageBox.Show("Ocorreu um erro" + ex.Message, "Lottus Store");
            }
            

            
           


        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
            Cadastrar_funcionario tela = new Cadastrar_funcionario();
            tela.Show();
            this.Hide();
            }
            catch (Exception ex)
            {

                MessageBox.Show("Ocorreu um erro" + ex.Message, "Lottus Store");
            }



        }

        private void button5_Click(object sender, EventArgs e)
        {

            try
            {
            Cadastro_de_cliente tela = new Cadastro_de_cliente();
            tela.Show();
            this.Hide();
            }
            catch (Exception ex)
            {

                MessageBox.Show("Ocorreu um erro" + ex.Message, "Lottus Store");
            }

        }

        private void button10_Click(object sender, EventArgs e)
        {
            try
            {
            Consultar_Funcionario tela = new Consultar_Funcionario();
            tela.Show();
            this.Hide();
            }
            catch (Exception ex)
            {

                MessageBox.Show("Ocorreu um erro" + ex.Message, "Lottus Store");
            }


        }

        private void button3_Click(object sender, EventArgs e)
        {
            try
            {
            Consultar_Cliente tela = new Consultar_Cliente();
            tela.Show();
            this.Hide();
            }
            catch (Exception ex)
            {

                MessageBox.Show("Ocorreu um erro" + ex.Message, "Lottus Store");
            }


        }

        private void button12_Click(object sender, EventArgs e)
        {
            try
            {
            Controle_de_estoque tela = new Controle_de_estoque();
            tela.Show();
            this.Hide();
            }
            catch (Exception ex)
            {

                MessageBox.Show("Ocorreu um erro" + ex.Message, "Lottus Store");
            }


        }

        private void button7_Click(object sender, EventArgs e)
        {
            try
            {
            Consultar_Fornecedor tela = new Consultar_Fornecedor();
            tela.Show();
            this.Hide();
            }
            catch (Exception ex)
            {

                MessageBox.Show("Ocorreu um erro" + ex.Message, "Lottus Store");
            }

        }

        private void button4_Click(object sender, EventArgs e)
        {
            try
            {
            Cadastrar_Pedido tela = new Cadastrar_Pedido();
            tela.Show();
            this.Hide();
            }
            catch (Exception ex)
            {

                MessageBox.Show("Ocorreu um erro" + ex.Message, "Lottus Store");
            }


        }

        private void button8_Click(object sender, EventArgs e)
        {
            try
            {
            Consultar_Pedido tela = new Consultar_Pedido();
            tela.Show();
            this.Hide();
            }
            catch (Exception ex)
            {

                MessageBox.Show("Ocorreu um erro" + ex.Message, "Lottus Store");
            }


        }

    
        public void ValidarLogin()
        {
            try
            {
            if (UserSess.Usuariologado.PermissaoAdm == false)
            {
                button15.Enabled = false;
                button9.Enabled = false;
                button1.Enabled = false;
                button2.Enabled = false;
            }

            }
            catch (Exception ex)
            {

                MessageBox.Show("Ocorreu um erro, Logue novamente" , "Lottus Store");
                Application.Exit();
            }
        }

        private void button14_Click(object sender, EventArgs e)
        {
            try
            {
            Consultar_Login tela = new Consultar_Login();
            tela.Show();
            this.Hide();
            }
            catch (Exception ex)
            {

                MessageBox.Show("Ocorreu um erro" + ex.Message, "Lottus Store");
            }

        }

        private void button11_Click(object sender, EventArgs e)
        {
            try
            {
            Consultar_Estoque tela = new Consultar_Estoque();
            tela.Show();
            this.Hide();
            }
            catch (Exception ex)
            {

                MessageBox.Show("Ocorreu um erro" + ex.Message, "Lottus Store");
            }

        }

        private void Menu_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }
    }
}
