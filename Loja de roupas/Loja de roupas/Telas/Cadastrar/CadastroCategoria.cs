﻿using Loja_de_roupas.DB.Base.Categoria;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Loja_de_roupas.Resources
{
    public partial class Cartegoria : Form
    {
        public Cartegoria()
        {
            InitializeComponent();
        }

        private void Cartegoria_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {

            try
            {
            CategoriaDTO dto = new CategoriaDTO();
            dto.Nome = txtCategoria.Text.Trim();
            CategoriaBusiness business = new CategoriaBusiness();
            business.Salvar(dto);
                MessageBox.Show("Cadastro efetuado com sucesso", "Lottus Store");
            }
            catch (Exception ex)
            {

                MessageBox.Show("Ocorreu um erro" + ex.Message, "Lottus Store");
            }
            
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            try
            {
            Menu tela = new Menu();
            tela.Show();
            this.Hide();
            }
            catch (Exception ex)
            {

                MessageBox.Show("Ocorreu um erro" + ex.Message, "Lottus Store");
            }


        }

        private void txtCategoria_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {
            Validação v = new Validação();
            v.soletras(e);
            }
            catch (Exception ex)
            {

                MessageBox.Show("Ocorreu um erro" + ex.Message, "Lottus Store");
            }

        }
    }
}
