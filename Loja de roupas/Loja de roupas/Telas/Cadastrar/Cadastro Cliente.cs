﻿using Loja_de_roupas.DB.Base.Clientes;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Loja_de_roupas
{
    public partial class Cadastro_de_cliente : Form
    {
        public Cadastro_de_cliente()
        {
            InitializeComponent();
        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void label7_Click(object sender, EventArgs e)
        {

        }

        private void btnCadastrar_Click(object sender, EventArgs e)
        {
            try
            {
            ClienteDTO dto = new ClienteDTO();
            dto.Nome = txtNome.Text.Trim();
            dto.Sobrenome = txtSobrenome.Text.Trim();
            dto.Telefone = txtTelefone.Text.Trim();
            dto.CEP = txtCep.Text.Trim();
            dto.Complemento = txtComplemento.Text.Trim();
            dto.Numero = Convert.ToInt32(nudNcasa.Value);
            dto.CPF = txtCpf.Text.Trim();

            ClienteBusiness business = new ClienteBusiness();
            business.Salvar(dto);


            MessageBox.Show("Cadastro efetuado com sucesso", "Lottus Store");
            }
            catch (Exception ex)
            {

                MessageBox.Show("Ocorreu um erro" + ex.Message, "Lottus Store");
            }

        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            try
            {
            Menu tela = new Menu();
            tela.Show();
            this.Hide();
            }
            catch (Exception ex)
            {

                MessageBox.Show("Ocorreu um erro" + ex.Message, "Lottus Store");
            }

        }

        private void txtSobrenome_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtNome_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {
            Validação v = new Validação();
            v.soletras(e);
            }
            catch (Exception ex)
            {

                MessageBox.Show("Ocorreu um erro" + ex.Message, "Lottus Store");
            }

        }

        private void txtSobrenome_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {
            Validação v = new Validação();
            v.soletras(e);
            }
            catch (Exception ex)
            {

                MessageBox.Show("Ocorreu um erro" + ex.Message, "Lottus Store");
            }

        }

        private void txtComplemento_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtComplemento_KeyPress(object sender, KeyPressEventArgs e)
        {
        }

        private void maskedTextBox1_MaskInputRejected(object sender, MaskInputRejectedEventArgs e)
        {

        }
    }
}
