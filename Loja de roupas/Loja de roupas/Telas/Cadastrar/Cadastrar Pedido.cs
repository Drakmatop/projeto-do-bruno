﻿using Loja_de_roupas.DB.Base.Clientes;
using Loja_de_roupas.DB.Funcionario;
using Loja_de_roupas.DB.Pedido;
using Loja_de_roupas.DB.Produto;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Loja_de_roupas
{
    public partial class Cadastrar_Pedido : Form
    {
        BindingList<ProdutoDTO> produtosCarrinho = new BindingList<ProdutoDTO>();
        Validação v = new Validação();

        public Cadastrar_Pedido()
        {
            InitializeComponent();
            CarregarCombos();
        }
        void CarregarCombos()
        {
            ProdutoBusiness_ business = new ProdutoBusiness_();
            List<ProdutoDTO> lista = business.Consultar(string.Empty);
            cboProduto.ValueMember = nameof(ProdutoDTO.Id);
            cboProduto.DisplayMember = nameof(ProdutoDTO.Nome);
            cboProduto.DataSource = lista;

            ClienteBusiness clientebus = new ClienteBusiness();
            List<ClienteDTO> clielista = clientebus.Listar();
            cboCliente.ValueMember = nameof(ClienteDTO.Id);
            cboCliente.DisplayMember = nameof(ClienteDTO.Nome);
            cboCliente.DataSource = clielista;


        }

        private void btnAdicionar_Click(object sender, EventArgs e)
        {
            try
            {
            ProdutoDTO dto = cboProduto.SelectedItem as ProdutoDTO;
            int qtd = Convert.ToInt32(nudQuantidade.Value);

            for (int i = 0; i < qtd; i++)

            {
                produtosCarrinho.Add(dto);
            }
                MessageBox.Show("Item adicionado", "Lottus Store");
            }
            catch (Exception ex)
            {

                MessageBox.Show("ocorreu o erro" + ex.Message, "lottus store");
            }
          
        }

        private void btnCadastrar_Click(object sender, EventArgs e)
        {
            try
            {
                if (Convert.ToInt32(nudQuantidade.Value) == 0)
                {
                    MessageBox.Show("Quantidae é obrigatório");
                }
                else if (txtCpf.Text == string.Empty)
                {
                    MessageBox.Show("Cpf é obrigatório");

                }
                else
                {

                    ClienteDTO clie = cboCliente.SelectedItem as ClienteDTO;
                    ProdutoDTO produto = cboProduto.SelectedItem as ProdutoDTO;

                    PedidoItemDTO pedidoitem = new PedidoItemDTO();
                    PedidoDTO dto = new PedidoDTO();
                    dto.FormaPagamento = cboFormaPagamento.Text;
                    dto.Data = DateTime.Now;
                    dto.ClienteId = clie.Id;
                    dto.FuncionarioId = UserSess.Usuariologado.Id;
                    pedidoitem.IdProduto = produto.Id;

                    PedidoBusiness business = new PedidoBusiness();

                    business.Salvar(dto, produtosCarrinho.ToList());


                    MessageBox.Show("Pedido salvo com sucesso.", "Lottus Store", MessageBoxButtons.OK, MessageBoxIcon.Information);

                }

            }
            catch (Exception ex)
            {

                MessageBox.Show("Ocorreu um erro" + ex.Message, "Lottus Store");
            }
            


        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            try
            {
            Menu tela = new Menu();
            tela.Show();
            this.Hide();
            }
            catch (Exception ex)
            {

                MessageBox.Show("Ocorreu um erro" + ex.Message, "Lottus Sotre");
            }

        }

        private void textBox2_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {
            Validação v = new Validação();
            v.soletras(e); 
            }
            catch (Exception ex)
            {

                MessageBox.Show("Ocorreu um erro" + ex.Message, "Lottus Store");
            }

        }

        private void cboFuncionario_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void nudQuantidade_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {
            Validação v = new Validação();
            v.sonumeros(e);
            }
            catch (Exception ex)
            {

                MessageBox.Show("Ocorreu um erro" + ex.Message, "Lottus Store");
            }

        }
    }
}
