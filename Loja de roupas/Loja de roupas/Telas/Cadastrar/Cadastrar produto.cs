﻿using Loja_de_roupas.DB.Base.Categoria;
using Loja_de_roupas.DB.Fornecedor;
using Loja_de_roupas.DB.Produto;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Loja_de_roupas
{
    public partial class Cadastrar_produto : Form
    {
        BindingList<ProdutoDTO> produtosCarrinho = new BindingList<ProdutoDTO>();
        Validação v = new Validação();
        public Cadastrar_produto()
        {
            InitializeComponent();
            CarregarCombos();
        }

        void CarregarCombos()
        {
            
            CategoriaBusiness catebus = new CategoriaBusiness();
            List<CategoriaDTO> cateList = catebus.Listar();

            cboCategoria.DisplayMember = nameof(CategoriaDTO.Nome);
            cboCategoria.ValueMember = nameof(CategoriaDTO.Id);
            cboCategoria.DataSource = cateList;


            string nome = "";
            FornecedorBusiness fornbus = new FornecedorBusiness();
            List<FornecedorDTO> fornList = fornbus.Consultar(nome);

            cboFornecedor.DisplayMember = nameof(FornecedorDTO.Nome);
            cboFornecedor.ValueMember = nameof(FornecedorDTO.Id);
            cboFornecedor.DataSource = fornList;

        }

        private void button1_Click(object sender, EventArgs e)
        {

        }

        private void Cadastrar_produto_Load(object sender, EventArgs e)
        {

        }


        private void bnt1_Click_2(object sender, EventArgs e)
        {
            try
            {
            CategoriaDTO cat = cboCategoria.SelectedItem as CategoriaDTO;
            FornecedorDTO forn = cboFornecedor.SelectedItem as FornecedorDTO;

            ProdutoDTO dto = new ProdutoDTO();
            dto.Nome = txtProduto.Text.Trim();
            dto.Cor = txtCor.Text.Trim();
            dto.Preco = nudPreco.Value;
            dto.Tamanho = txtTamanho.Text.Trim();
            dto.CategoriaId = cat.Id;
            dto.FornecedorId = forn.Id;
            

            ProdutoBusiness_ business = new ProdutoBusiness_();
            business.Salvar(dto);

            MessageBox.Show("Cadastro efetuado com sucesso", "Lottus Store");
            }
            catch (Exception ex)
            {

                MessageBox.Show("Ocorreu um erro" + ex.Message, "Lottus Store");
            }   

        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            try
            {
            Menu tela = new Menu();
            tela.Show();
            this.Hide();
            }
            catch (Exception ex)
            {

                MessageBox.Show("Ocorreu um erro" + ex.Message, "Lottus Store"); 
            }

        }

        private void txtProduto_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {
            Validação v = new Validação();
            v.soletras(e);
            }
            catch (Exception ex)
            {

                MessageBox.Show("Ocorreu um erro" + ex.Message, "Lottus Store");
            }
 
        }

    }
}
