﻿using Loja_de_roupas.DB.Base.Clientes;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Loja_de_roupas
{
    public partial class Consultar_Cliente : Form
    {
        public Consultar_Cliente()
        {
            InitializeComponent();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void txt1_TextChanged(object sender, EventArgs e)
        {

        }

        private void lbl1_Click(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
            List<ClienteDTO> dto = new List<ClienteDTO>();

            ClienteBusiness business = new ClienteBusiness();
            dto = business.Consultar(txtCliente.Text);
            dataGridView1.AutoGenerateColumns = false;
            dataGridView1.DataSource = dto;
            }
            catch (Exception ex)
            {

                MessageBox.Show("Ocorreu um erro" + ex.Message, "Lottus Store");
            }

           
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            try
            {
            Menu tela = new Menu();
            tela.Show();
            this.Hide();
            }
            catch (Exception ex)
            {

                MessageBox.Show("Ocorreu um erro" + ex.Message, "Lottus Store");
            }

        }

        private void txtCliente_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {
            Validação v = new Validação();
            v.soletras(e);
            }
            catch (Exception ex)
            {

                MessageBox.Show("Ocorreu um erro" + ex.Message, "Lottus Store");
            }

        }
    }
}
