﻿using Loja_de_roupas.DB.Estoque;
using Loja_de_roupas.DB.Produto;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Loja_de_roupas
{
    public partial class Controle_de_estoque : Form
    {
        public Controle_de_estoque()
        {
            InitializeComponent();
            carregarcombo();
        }

        public void carregarcombo()
        {
            ProdutoBusiness_ catebus = new ProdutoBusiness_();
            List<ProdutoDTO> cateList = catebus.Listar();

            cboProduto.DisplayMember = nameof(ProdutoDTO.Nome);
            cboProduto.ValueMember = nameof(ProdutoDTO.Id);
            cboProduto.DataSource = cateList;


        }
        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
            ProdutoDTO a = cboProduto.SelectedItem as ProdutoDTO;
            EstoqueDTO dto = new EstoqueDTO();
            dto.IdProduto = a.Id;
            int quantidade =Convert.ToInt32(nudEntrada.Value) - Convert.ToInt32(nudSaida.Value);
            dto.Quantidade = quantidade;
         

            EstoqueBusiness business = new EstoqueBusiness();
            business.Salvar(dto);
                MessageBox.Show("Cadastro efetuado com sucesso", "Lottus Store");
            }
            catch (Exception ex)
            {

                MessageBox.Show("Ocorreu um erro" + ex.Message, "Lottus Store");
            }

           

        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {

        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            try
            {
            Menu tela = new Menu();
            tela.Show();
            this.Hide();
            }
            catch (Exception ex)
            {

                MessageBox.Show("Ocorreu um erro" + ex.Message, "Lottus Store");
            }

        }
    }
}
