﻿using Loja_de_roupas.DB.Funcionario;
using Loja_de_roupas.DB.Pedido;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Loja_de_roupas
{
    public partial class Consultar_Pedido : Form
    {
        public Consultar_Pedido()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
            PedidoBusiness pedido = new PedidoBusiness();
            List<PedidoConsultarView> view = pedido.Consultar(txtCliente.Text);
            FuncionarioBusiness business = new FuncionarioBusiness();
            dataGridView1.AutoGenerateColumns = false;
            dataGridView1.DataSource = view;
            }
            catch (Exception ex)
            {

                MessageBox.Show("Ocorreu um erro" + ex.Message, "Lottus Store");

            }


        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            try
            {
            Menu tela = new Menu();
            tela.Show();
            this.Hide();
            }
            catch (Exception ex)
            {

                MessageBox.Show("Ocorreu um erro" + ex.Message, "Lottus Store");
            }

        }

        private void txtCliente_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {
            Validação v = new Validação();
            v.soletras(e);
            }
            catch (Exception ex)
            {

                MessageBox.Show("Ocorreu um erro" + ex.Message, "Lottus Store");
            }

        }
    }
}
